package interfaz;

import dominio.*;
import java.awt.Color;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JFileChooser;

public class VentanaPrincipal extends javax.swing.JFrame {

    WorldTrip wt = WorldTrip.getInstance();
    private String cedulaUsuario;

    public VentanaPrincipal() {
        initComponents();
        jLayeredPane = this.getLayeredPane();
        jLayeredPane.add(panelCreacionUsuario);
        jLayeredPane.add(panelIngreso);
        jLayeredPane.add(panelMenuFuncionario);
        jLayeredPane.add(panelMenuPasajero);
        jLayeredPane.moveToFront(panelIngreso);
        cedulaUsuario = "";
        this.setLocationRelativeTo(null);
        this.setResizable(false);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLayeredPane = new javax.swing.JLayeredPane();
        panelIngreso = new javax.swing.JPanel();
        crearNuevaCuenta = new javax.swing.JButton();
        ingresoSesion = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        txtUsuario = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        lblMensajeLogin = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        panelCreacionUsuario = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        txtCrear = new javax.swing.JTextField();
        crearCuenta = new javax.swing.JButton();
        lblMensajeCreacion = new javax.swing.JLabel();
        cancelarCreacion = new javax.swing.JButton();
        jLabel13 = new javax.swing.JLabel();
        jLabel54 = new javax.swing.JLabel();
        txtNomCrear = new javax.swing.JTextField();
        panelMenuPasajero = new javax.swing.JPanel();
        panelInicialP = new javax.swing.JPanel();
        informacionPersonal1 = new javax.swing.JButton();
        consultarServiciosP1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        panelModificarP = new javax.swing.JPanel();
        informacionPersonal5 = new javax.swing.JButton();
        consultarServiciosP5 = new javax.swing.JButton();
        jScrollPane7 = new javax.swing.JScrollPane();
        mostrarHistorialP = new javax.swing.JList();
        jLabel43 = new javax.swing.JLabel();
        jLabel44 = new javax.swing.JLabel();
        jLabel46 = new javax.swing.JLabel();
        jLabel47 = new javax.swing.JLabel();
        nuevaCedula = new javax.swing.JTextField();
        nuevoNombre = new javax.swing.JTextField();
        confirmarNuevaCedula = new javax.swing.JRadioButton();
        confirmarNuevoNombre = new javax.swing.JRadioButton();
        jLabel48 = new javax.swing.JLabel();
        nuevat1 = new javax.swing.JTextField();
        confirmarNuevaTarjeta = new javax.swing.JRadioButton();
        guardarPasActualizado = new javax.swing.JButton();
        errorCedulaNueva = new javax.swing.JLabel();
        errorNombreNuevo = new javax.swing.JLabel();
        errorTarjetaNueva = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        nuevat2 = new javax.swing.JTextField();
        nuevat3 = new javax.swing.JTextField();
        nuevat4 = new javax.swing.JTextField();
        panelCompraViajeP = new javax.swing.JPanel();
        informacionPersonal7 = new javax.swing.JButton();
        consultarServiciosP7 = new javax.swing.JButton();
        jLabel36 = new javax.swing.JLabel();
        jLabel38 = new javax.swing.JLabel();
        jLabel39 = new javax.swing.JLabel();
        comboBoxDias = new javax.swing.JComboBox();
        jLabel40 = new javax.swing.JLabel();
        cantidadAsientosP = new javax.swing.JTextField();
        numeroTarj = new javax.swing.JTextField();
        efectuarCompra = new javax.swing.JButton();
        jScrollPane6 = new javax.swing.JScrollPane();
        servicioSeleccionadoP = new javax.swing.JList();
        lblAvisoCompraP = new javax.swing.JLabel();
        mensajeServicioP = new javax.swing.JLabel();
        jLabel42 = new javax.swing.JLabel();
        jComboBoxPagos = new javax.swing.JComboBox();
        numeroTarj1 = new javax.swing.JTextField();
        numeroTarj2 = new javax.swing.JTextField();
        numeroTarj3 = new javax.swing.JTextField();
        cerrarSesion10 = new javax.swing.JButton();
        jLabel55 = new javax.swing.JLabel();
        panelConsultarServiciosP = new javax.swing.JPanel();
        informacionPersonal6 = new javax.swing.JButton();
        consultarServiciosP6 = new javax.swing.JButton();
        comprarPasajesP6 = new javax.swing.JButton();
        jScrollPane4 = new javax.swing.JScrollPane();
        listaServiciosP = new javax.swing.JList();
        jLabel33 = new javax.swing.JLabel();
        jLabel34 = new javax.swing.JLabel();
        origenServicioP = new javax.swing.JTextField();
        destinoServicioP = new javax.swing.JTextField();
        buscarServiciosP = new javax.swing.JButton();
        jTextField6 = new javax.swing.JTextField();
        ordPorPrecioP = new javax.swing.JButton();
        ordPorTransporte = new javax.swing.JButton();
        ordPorHoraLlegadaP = new javax.swing.JButton();
        ordPorHoraSalidaP = new javax.swing.JButton();
        jLabel35 = new javax.swing.JLabel();
        lblMensajeServiciosP = new javax.swing.JLabel();
        jLabel37 = new javax.swing.JLabel();
        lblMensajeCompraP = new javax.swing.JLabel();
        cerrarSeson9 = new javax.swing.JButton();
        panelMenuFuncionario = new javax.swing.JPanel();
        panelInicial = new javax.swing.JPanel();
        procesarArchivos3 = new javax.swing.JButton();
        crearFuncionario3 = new javax.swing.JButton();
        registrarServicio3 = new javax.swing.JButton();
        modificarServicio4 = new javax.swing.JButton();
        cambiarDatosPasajero4 = new javax.swing.JButton();
        consultarServicio4 = new javax.swing.JButton();
        comprarPasaje4 = new javax.swing.JButton();
        cerrarSesion4 = new javax.swing.JButton();
        jButton11 = new javax.swing.JButton();
        panelRegistroServicio = new javax.swing.JPanel();
        rLunes = new javax.swing.JRadioButton();
        rMartes = new javax.swing.JRadioButton();
        rMiercoles = new javax.swing.JRadioButton();
        rJueves = new javax.swing.JRadioButton();
        rViernes = new javax.swing.JRadioButton();
        rSabado = new javax.swing.JRadioButton();
        rDomingo = new javax.swing.JRadioButton();
        jLabel6 = new javax.swing.JLabel();
        tiposTransporte = new javax.swing.JComboBox();
        jLabel7 = new javax.swing.JLabel();
        txtOrigen = new javax.swing.JTextField();
        txtDestino = new javax.swing.JTextField();
        txtDuracion = new javax.swing.JTextField();
        txtIdentificador = new javax.swing.JTextField();
        txtHora = new javax.swing.JTextField();
        txtAsientos = new javax.swing.JTextField();
        txtPrecio = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        lblMensajeServicio = new javax.swing.JLabel();
        registroServicio = new javax.swing.JButton();
        procesarArchivos = new javax.swing.JButton();
        crearFuncionario = new javax.swing.JButton();
        registrarServicio = new javax.swing.JButton();
        modificarServicio = new javax.swing.JButton();
        cambiarDatosPasajero = new javax.swing.JButton();
        consultarServicio = new javax.swing.JButton();
        comprarPasaje = new javax.swing.JButton();
        cerrarSesion7 = new javax.swing.JButton();
        jLabel50 = new javax.swing.JLabel();
        jButton10 = new javax.swing.JButton();
        panelRegistrarFuncionario = new javax.swing.JPanel();
        registrarNuevoFunc = new javax.swing.JButton();
        txtNombreFunc = new javax.swing.JTextField();
        txtCedulaFunc = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        lblMensajeCreacionFunc = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        procesarArchivos1 = new javax.swing.JButton();
        crearFuncionario1 = new javax.swing.JButton();
        registrarServicio1 = new javax.swing.JButton();
        modificarServicio1 = new javax.swing.JButton();
        cambiarDatosPasajero1 = new javax.swing.JButton();
        consultarServicio1 = new javax.swing.JButton();
        comprarPasaje1 = new javax.swing.JButton();
        cerrarSesion6 = new javax.swing.JButton();
        jButton9 = new javax.swing.JButton();
        panelProcesarArchivos = new javax.swing.JPanel();
        procesarArchivos2 = new javax.swing.JButton();
        crearFuncionario2 = new javax.swing.JButton();
        registrarServicio2 = new javax.swing.JButton();
        modificarServicio2 = new javax.swing.JButton();
        cambiarDatosPasajero2 = new javax.swing.JButton();
        consultarServicio2 = new javax.swing.JButton();
        comprarPasaje2 = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        txtInfoArchivos = new javax.swing.JTextArea();
        jLabel29 = new javax.swing.JLabel();
        cerrarSesion5 = new javax.swing.JButton();
        jButton8 = new javax.swing.JButton();
        panelModificarServicio = new javax.swing.JPanel();
        resultadoModificarServicio = new javax.swing.JLabel();
        nuevaHoraFin = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        nuevaHoraInicio = new javax.swing.JTextField();
        guardarTransporte = new javax.swing.JButton();
        jLabel18 = new javax.swing.JLabel();
        procesarArchivos4 = new javax.swing.JButton();
        registrarServicio4 = new javax.swing.JButton();
        crearFuncionario4 = new javax.swing.JButton();
        modificarServicio3 = new javax.swing.JButton();
        cambiarDatosPasajero3 = new javax.swing.JButton();
        consultarServicio3 = new javax.swing.JButton();
        comprarPasaje3 = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        jListaServicios = new javax.swing.JList();
        cerrarSesion3 = new javax.swing.JButton();
        jLabel51 = new javax.swing.JLabel();
        jButton7 = new javax.swing.JButton();
        panelConsultarServicios = new javax.swing.JPanel();
        procesarArchivos5 = new javax.swing.JButton();
        registrarServicio5 = new javax.swing.JButton();
        crearFuncionario5 = new javax.swing.JButton();
        cambiarDatosPasajero5 = new javax.swing.JButton();
        modificarServicio5 = new javax.swing.JButton();
        consultarServicio5 = new javax.swing.JButton();
        comprarPasaje5 = new javax.swing.JButton();
        jLabel19 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        ciudadOrigen = new javax.swing.JTextField();
        ciudadDestino = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        jListaServiciosEncontrados = new javax.swing.JList();
        jLabel23 = new javax.swing.JLabel();
        ordenarPrecio = new javax.swing.JButton();
        horaSalida = new javax.swing.JButton();
        horaLlegada = new javax.swing.JButton();
        transporte = new javax.swing.JButton();
        cargarServicios = new javax.swing.JButton();
        resultadoCargarServicios = new javax.swing.JLabel();
        jTextField3 = new javax.swing.JTextField();
        cerrarSesion2 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        panelModificarUsuario = new javax.swing.JPanel();
        procesarArchivos6 = new javax.swing.JButton();
        registrarServicio6 = new javax.swing.JButton();
        crearFuncionario6 = new javax.swing.JButton();
        modificarServicio6 = new javax.swing.JButton();
        cambiarDatosPasajero6 = new javax.swing.JButton();
        consultarServicio6 = new javax.swing.JButton();
        comprarPasaje6 = new javax.swing.JButton();
        jLabel24 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        nombreNuevo = new javax.swing.JTextField();
        cedulaNueva = new javax.swing.JTextField();
        guardarPasajero = new javax.swing.JButton();
        resultadoUsuario = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        cedulaActual = new javax.swing.JTextField();
        jLabel27 = new javax.swing.JLabel();
        confirmarBaja = new javax.swing.JRadioButton();
        confirmarCedula = new javax.swing.JRadioButton();
        confirmarNombre = new javax.swing.JRadioButton();
        jLabel28 = new javax.swing.JLabel();
        errorPasajeroNoEsta = new javax.swing.JLabel();
        pasajeroBorrado = new javax.swing.JLabel();
        cerrarSesion = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        panelCompraPasajeF = new javax.swing.JPanel();
        procesarArchivos7 = new javax.swing.JButton();
        registrarServicio7 = new javax.swing.JButton();
        crearFuncionario7 = new javax.swing.JButton();
        modificarServicio7 = new javax.swing.JButton();
        cambiarDatosPasajero7 = new javax.swing.JButton();
        consultarServicio7 = new javax.swing.JButton();
        comprarPasaje7 = new javax.swing.JButton();
        cerrarSesion1 = new javax.swing.JButton();
        informacionPasajero7 = new javax.swing.JButton();
        jLabel30 = new javax.swing.JLabel();
        cedulaPasajero = new javax.swing.JTextField();
        jLabel31 = new javax.swing.JLabel();
        comprarPasajeAPasajero = new javax.swing.JButton();
        mensajeServicio = new javax.swing.JLabel();
        mensajeCedula = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jLabel32 = new javax.swing.JLabel();
        cantidadAsientos = new javax.swing.JTextField();
        jLabel41 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox();
        jScrollPane10 = new javax.swing.JScrollPane();
        jListaServiciosCompra = new javax.swing.JList();
        panelConsultarInfoPasajeros = new javax.swing.JPanel();
        jLabel49 = new javax.swing.JLabel();
        jLabel45 = new javax.swing.JLabel();
        jScrollPane8 = new javax.swing.JScrollPane();
        lstMostrarHistorialP = new javax.swing.JList();
        jLabel52 = new javax.swing.JLabel();
        procesarArchivos8 = new javax.swing.JButton();
        registrarServicio8 = new javax.swing.JButton();
        crearFuncionario8 = new javax.swing.JButton();
        modificarServicio8 = new javax.swing.JButton();
        cambiarDatosPasajero8 = new javax.swing.JButton();
        consultarServicio8 = new javax.swing.JButton();
        comprarPasaje8 = new javax.swing.JButton();
        cerrarSesion8 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jScrollPane9 = new javax.swing.JScrollPane();
        lstMostrarPasajerosP = new javax.swing.JList();
        jLabel53 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLayeredPane.setPreferredSize(new java.awt.Dimension(590, 540));

        panelIngreso.setPreferredSize(new java.awt.Dimension(690, 521));

        crearNuevaCuenta.setText("Crear cuenta");
        crearNuevaCuenta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                crearNuevaCuentaActionPerformed(evt);
            }
        });

        ingresoSesion.setText("Login");
        ingresoSesion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ingresoSesionActionPerformed(evt);
            }
        });

        jLabel1.setText("Cedula:");

        txtUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtUsuarioActionPerformed(evt);
            }
        });

        jLabel2.setText("No tiene cuenta creada?");

        jLabel16.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        jLabel16.setText("Login de usuario:");

        javax.swing.GroupLayout panelIngresoLayout = new javax.swing.GroupLayout(panelIngreso);
        panelIngreso.setLayout(panelIngresoLayout);
        panelIngresoLayout.setHorizontalGroup(
            panelIngresoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelIngresoLayout.createSequentialGroup()
                .addGroup(panelIngresoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelIngresoLayout.createSequentialGroup()
                        .addGap(247, 247, 247)
                        .addComponent(jLabel16))
                    .addGroup(panelIngresoLayout.createSequentialGroup()
                        .addGap(268, 268, 268)
                        .addComponent(ingresoSesion, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelIngresoLayout.createSequentialGroup()
                        .addGap(146, 146, 146)
                        .addGroup(panelIngresoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblMensajeLogin, javax.swing.GroupLayout.PREFERRED_SIZE, 348, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(panelIngresoLayout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 252, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(panelIngresoLayout.createSequentialGroup()
                        .addGap(234, 234, 234)
                        .addGroup(panelIngresoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(crearNuevaCuenta, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2))))
                .addContainerGap(105, Short.MAX_VALUE))
        );
        panelIngresoLayout.setVerticalGroup(
            panelIngresoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelIngresoLayout.createSequentialGroup()
                .addGap(42, 42, 42)
                .addComponent(jLabel16)
                .addGap(119, 119, 119)
                .addGroup(panelIngresoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addGap(32, 32, 32)
                .addComponent(ingresoSesion, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lblMensajeLogin, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(27, 27, 27)
                .addComponent(jLabel2)
                .addGap(18, 18, 18)
                .addComponent(crearNuevaCuenta, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        panelCreacionUsuario.setPreferredSize(new java.awt.Dimension(590, 540));

        jLabel3.setText("Cedula:");

        crearCuenta.setText("Crear Cuenta");
        crearCuenta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                crearCuentaActionPerformed(evt);
            }
        });

        cancelarCreacion.setText("Cancelar");
        cancelarCreacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelarCreacionActionPerformed(evt);
            }
        });

        jLabel13.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        jLabel13.setText("Nueva cuenta de pasajero:");

        jLabel54.setText("Nombre: ");

        javax.swing.GroupLayout panelCreacionUsuarioLayout = new javax.swing.GroupLayout(panelCreacionUsuario);
        panelCreacionUsuario.setLayout(panelCreacionUsuarioLayout);
        panelCreacionUsuarioLayout.setHorizontalGroup(
            panelCreacionUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelCreacionUsuarioLayout.createSequentialGroup()
                .addGap(114, 114, 114)
                .addGroup(panelCreacionUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(panelCreacionUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(lblMensajeCreacion, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 319, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(panelCreacionUsuarioLayout.createSequentialGroup()
                            .addGroup(panelCreacionUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel3)
                                .addComponent(jLabel54))
                            .addGap(25, 25, 25)
                            .addGroup(panelCreacionUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(txtCrear, javax.swing.GroupLayout.DEFAULT_SIZE, 236, Short.MAX_VALUE)
                                .addComponent(txtNomCrear))))
                    .addGroup(panelCreacionUsuarioLayout.createSequentialGroup()
                        .addComponent(crearCuenta, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(84, 84, 84)
                        .addComponent(cancelarCreacion, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(136, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelCreacionUsuarioLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel13)
                .addGap(164, 164, 164))
        );
        panelCreacionUsuarioLayout.setVerticalGroup(
            panelCreacionUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelCreacionUsuarioLayout.createSequentialGroup()
                .addGap(45, 45, 45)
                .addComponent(jLabel13)
                .addGap(61, 61, 61)
                .addGroup(panelCreacionUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtCrear, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addGroup(panelCreacionUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel54)
                    .addComponent(txtNomCrear, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(46, 46, 46)
                .addComponent(lblMensajeCreacion, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(48, 48, 48)
                .addGroup(panelCreacionUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(crearCuenta, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cancelarCreacion, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(171, Short.MAX_VALUE))
        );

        panelMenuPasajero.setPreferredSize(new java.awt.Dimension(690, 521));

        informacionPersonal1.setText("Informacion Personal");
        informacionPersonal1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                informacionPersonal1ActionPerformed(evt);
            }
        });

        consultarServiciosP1.setText("Consultar Servicios");
        consultarServiciosP1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                consultarServiciosP1ActionPerformed(evt);
            }
        });

        jButton2.setText("Cerrar Sesion");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelInicialPLayout = new javax.swing.GroupLayout(panelInicialP);
        panelInicialP.setLayout(panelInicialPLayout);
        panelInicialPLayout.setHorizontalGroup(
            panelInicialPLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelInicialPLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelInicialPLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jButton2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(informacionPersonal1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(consultarServiciosP1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(416, Short.MAX_VALUE))
        );
        panelInicialPLayout.setVerticalGroup(
            panelInicialPLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelInicialPLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(informacionPersonal1, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(consultarServiciosP1, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(405, Short.MAX_VALUE))
        );

        panelModificarP.setPreferredSize(new java.awt.Dimension(690, 521));

        informacionPersonal5.setText("Informacion Personal");
        informacionPersonal5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                informacionPersonal5ActionPerformed(evt);
            }
        });

        consultarServiciosP5.setText("Consultar Servicios");
        consultarServiciosP5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                consultarServiciosP5ActionPerformed(evt);
            }
        });

        jScrollPane7.setViewportView(mostrarHistorialP);

        jLabel43.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        jLabel43.setText("Historial de viajes:");

        jLabel44.setBackground(javax.swing.UIManager.getDefaults().getColor("Button.light"));
        jLabel44.setFont(new java.awt.Font("Lucida Grande", 0, 11)); // NOI18N
        jLabel44.setText("Origen|Destino|Hora Salida|Hora Llegada|Dia|Transporte|Precio|Tipo Pago");

        jLabel46.setText("Nueva Cedula:");

        jLabel47.setText("Nuevo Nombre:");

        confirmarNuevaCedula.setText("Confirmar");

        confirmarNuevoNombre.setText("Confirmar");

        jLabel48.setText("Nueva Tarjeta");

        confirmarNuevaTarjeta.setText("Confirmar");

        guardarPasActualizado.setText("Guardar");
        guardarPasActualizado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                guardarPasActualizadoActionPerformed(evt);
            }
        });

        jButton1.setText("Cerrar Sesion");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelModificarPLayout = new javax.swing.GroupLayout(panelModificarP);
        panelModificarP.setLayout(panelModificarPLayout);
        panelModificarPLayout.setHorizontalGroup(
            panelModificarPLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelModificarPLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelModificarPLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(panelModificarPLayout.createSequentialGroup()
                        .addGroup(panelModificarPLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(informacionPersonal5, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                            .addComponent(consultarServiciosP5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(panelModificarPLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelModificarPLayout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(panelModificarPLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel44, javax.swing.GroupLayout.PREFERRED_SIZE, 403, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 388, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(panelModificarPLayout.createSequentialGroup()
                                .addGap(129, 129, 129)
                                .addComponent(jLabel43))
                            .addGroup(panelModificarPLayout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(panelModificarPLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jLabel48, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel46)
                                    .addComponent(jLabel47))
                                .addGap(21, 21, 21)
                                .addGroup(panelModificarPLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(panelModificarPLayout.createSequentialGroup()
                                        .addGroup(panelModificarPLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(nuevaCedula)
                                            .addComponent(nuevoNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(27, 27, 27)
                                        .addGroup(panelModificarPLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(confirmarNuevaCedula)
                                            .addComponent(confirmarNuevoNombre)))
                                    .addGroup(panelModificarPLayout.createSequentialGroup()
                                        .addComponent(nuevat1, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(nuevat2, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(nuevat3, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(nuevat4, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(confirmarNuevaTarjeta))))))
                    .addGroup(panelModificarPLayout.createSequentialGroup()
                        .addGroup(panelModificarPLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(errorCedulaNueva, javax.swing.GroupLayout.DEFAULT_SIZE, 357, Short.MAX_VALUE)
                            .addComponent(errorNombreNuevo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(errorTarjetaNueva, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addComponent(guardarPasActualizado, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(32, 32, 32)))
                .addContainerGap(95, Short.MAX_VALUE))
        );
        panelModificarPLayout.setVerticalGroup(
            panelModificarPLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelModificarPLayout.createSequentialGroup()
                .addGroup(panelModificarPLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelModificarPLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(informacionPersonal5, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(consultarServiciosP5, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelModificarPLayout.createSequentialGroup()
                        .addGap(14, 14, 14)
                        .addComponent(jLabel43)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel44)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(panelModificarPLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(panelModificarPLayout.createSequentialGroup()
                        .addGroup(panelModificarPLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel46, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(nuevaCedula, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panelModificarPLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel47, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(nuevoNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(panelModificarPLayout.createSequentialGroup()
                        .addComponent(confirmarNuevaCedula)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(confirmarNuevoNombre)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelModificarPLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel48, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(nuevat1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(nuevat2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(nuevat3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(confirmarNuevaTarjeta)
                    .addComponent(nuevat4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(panelModificarPLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(guardarPasActualizado, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(panelModificarPLayout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(errorCedulaNueva, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(errorNombreNuevo, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(errorTarjetaNueva, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(132, 132, 132))
        );

        panelCompraViajeP.setPreferredSize(new java.awt.Dimension(690, 521));

        informacionPersonal7.setText("Informacion Personal");
        informacionPersonal7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                informacionPersonal7ActionPerformed(evt);
            }
        });

        consultarServiciosP7.setText("Consultar Servicios");
        consultarServiciosP7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                consultarServiciosP7ActionPerformed(evt);
            }
        });

        jLabel36.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        jLabel36.setText("Compra de pasaje:");

        jLabel38.setText("Dia:");

        jLabel39.setText("Asientos:");

        comboBoxDias.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboBoxDiasActionPerformed(evt);
            }
        });

        jLabel40.setText("Metodo de Pago:");

        cantidadAsientosP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cantidadAsientosPActionPerformed(evt);
            }
        });

        efectuarCompra.setText("Efecutar compra");
        efectuarCompra.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                efectuarCompraActionPerformed(evt);
            }
        });

        jScrollPane6.setViewportView(servicioSeleccionadoP);

        jLabel42.setText("Tarjeta:");

        jComboBoxPagos.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Visa", "Master", "Oca", "American Express" }));

        cerrarSesion10.setText("Cerrar Sesion");
        cerrarSesion10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cerrarSesion10ActionPerformed(evt);
            }
        });

        jLabel55.setFont(new java.awt.Font("Lucida Grande", 0, 10)); // NOI18N
        jLabel55.setText("Origen|Destino|Salida|Llegada|Frecuencia|Precio|Transporte|Disponibles|Pasajes");

        javax.swing.GroupLayout panelCompraViajePLayout = new javax.swing.GroupLayout(panelCompraViajeP);
        panelCompraViajeP.setLayout(panelCompraViajePLayout);
        panelCompraViajePLayout.setHorizontalGroup(
            panelCompraViajePLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelCompraViajePLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(informacionPersonal7, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(124, 124, 124)
                .addComponent(jLabel36, javax.swing.GroupLayout.PREFERRED_SIZE, 211, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(panelCompraViajePLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(consultarServiciosP7)
                .addGap(12, 12, 12)
                .addComponent(jLabel55, javax.swing.GroupLayout.PREFERRED_SIZE, 378, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(panelCompraViajePLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(cerrarSesion10, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 384, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(panelCompraViajePLayout.createSequentialGroup()
                .addGap(202, 202, 202)
                .addComponent(jLabel38)
                .addGap(46, 46, 46)
                .addComponent(comboBoxDias, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(panelCompraViajePLayout.createSequentialGroup()
                .addGap(202, 202, 202)
                .addComponent(jLabel39)
                .addGap(12, 12, 12)
                .addComponent(cantidadAsientosP, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(panelCompraViajePLayout.createSequentialGroup()
                .addGap(202, 202, 202)
                .addComponent(jLabel40)
                .addGap(12, 12, 12)
                .addComponent(jComboBoxPagos, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(panelCompraViajePLayout.createSequentialGroup()
                .addGap(202, 202, 202)
                .addComponent(jLabel42)
                .addGap(12, 12, 12)
                .addComponent(numeroTarj, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(numeroTarj1, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(numeroTarj2, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(numeroTarj3, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(panelCompraViajePLayout.createSequentialGroup()
                .addGap(70, 70, 70)
                .addComponent(efectuarCompra)
                .addGap(14, 14, 14)
                .addComponent(lblAvisoCompraP, javax.swing.GroupLayout.PREFERRED_SIZE, 282, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(panelCompraViajePLayout.createSequentialGroup()
                .addGap(230, 230, 230)
                .addComponent(mensajeServicioP, javax.swing.GroupLayout.PREFERRED_SIZE, 261, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        panelCompraViajePLayout.setVerticalGroup(
            panelCompraViajePLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelCompraViajePLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addGroup(panelCompraViajePLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(informacionPersonal7, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(panelCompraViajePLayout.createSequentialGroup()
                        .addGap(7, 7, 7)
                        .addComponent(jLabel36)))
                .addGap(6, 6, 6)
                .addGroup(panelCompraViajePLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(consultarServiciosP7, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(panelCompraViajePLayout.createSequentialGroup()
                        .addGap(31, 31, 31)
                        .addComponent(jLabel55)))
                .addGap(6, 6, 6)
                .addGroup(panelCompraViajePLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cerrarSesion10, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(12, 12, 12)
                .addGroup(panelCompraViajePLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelCompraViajePLayout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addComponent(jLabel38))
                    .addComponent(comboBoxDias, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(panelCompraViajePLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelCompraViajePLayout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(jLabel39))
                    .addComponent(cantidadAsientosP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(panelCompraViajePLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelCompraViajePLayout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addComponent(jLabel40))
                    .addComponent(jComboBoxPagos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(panelCompraViajePLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelCompraViajePLayout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(jLabel42))
                    .addComponent(numeroTarj, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(numeroTarj1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(numeroTarj2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(numeroTarj3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(22, 22, 22)
                .addGroup(panelCompraViajePLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(efectuarCompra, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblAvisoCompraP, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addComponent(mensajeServicioP, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        panelConsultarServiciosP.setPreferredSize(new java.awt.Dimension(588, 649));

        informacionPersonal6.setText("Informacion Personal");
        informacionPersonal6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                informacionPersonal6ActionPerformed(evt);
            }
        });

        consultarServiciosP6.setText("Consultar Servicios");
        consultarServiciosP6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                consultarServiciosP6ActionPerformed(evt);
            }
        });

        comprarPasajesP6.setText("Comprar Pasaje");
        comprarPasajesP6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comprarPasajesP6ActionPerformed(evt);
            }
        });

        jScrollPane4.setViewportView(listaServiciosP);

        jLabel33.setText("Origen:");

        jLabel34.setText("Destino:");

        origenServicioP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                origenServicioPActionPerformed(evt);
            }
        });

        buscarServiciosP.setText("Buscar Servicios");
        buscarServiciosP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buscarServiciosPActionPerformed(evt);
            }
        });

        jTextField6.setFont(new java.awt.Font("Lucida Grande", 0, 10)); // NOI18N
        jTextField6.setText("Origen|Destino|Salida|Llegada|Frecuencia|Precio|Transporte|Disponibles");
        jTextField6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField6ActionPerformed(evt);
            }
        });

        ordPorPrecioP.setText("Precio");
        ordPorPrecioP.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        ordPorPrecioP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ordPorPrecioPActionPerformed(evt);
            }
        });

        ordPorTransporte.setText("Transporte");
        ordPorTransporte.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        ordPorTransporte.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ordPorTransporteActionPerformed(evt);
            }
        });

        ordPorHoraLlegadaP.setText("Hora Llegada");
        ordPorHoraLlegadaP.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        ordPorHoraLlegadaP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ordPorHoraLlegadaPActionPerformed(evt);
            }
        });

        ordPorHoraSalidaP.setText("Hora Salida");
        ordPorHoraSalidaP.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        ordPorHoraSalidaP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ordPorHoraSalidaPActionPerformed(evt);
            }
        });

        jLabel35.setText("Ordenar por:");

        jLabel37.setFont(new java.awt.Font("Lucida Grande", 0, 14)); // NOI18N
        jLabel37.setText("Servicios encontrados:");

        cerrarSeson9.setText("Cerrar Sesion");
        cerrarSeson9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cerrarSeson9ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelConsultarServiciosPLayout = new javax.swing.GroupLayout(panelConsultarServiciosP);
        panelConsultarServiciosP.setLayout(panelConsultarServiciosPLayout);
        panelConsultarServiciosPLayout.setHorizontalGroup(
            panelConsultarServiciosPLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelConsultarServiciosPLayout.createSequentialGroup()
                .addGroup(panelConsultarServiciosPLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(panelConsultarServiciosPLayout.createSequentialGroup()
                        .addGap(38, 38, 38)
                        .addGroup(panelConsultarServiciosPLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane4)
                            .addGroup(panelConsultarServiciosPLayout.createSequentialGroup()
                                .addComponent(lblMensajeCompraP, javax.swing.GroupLayout.PREFERRED_SIZE, 361, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(comprarPasajesP6))
                            .addGroup(panelConsultarServiciosPLayout.createSequentialGroup()
                                .addGroup(panelConsultarServiciosPLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel37)
                                    .addComponent(jTextField6, javax.swing.GroupLayout.PREFERRED_SIZE, 381, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(panelConsultarServiciosPLayout.createSequentialGroup()
                                        .addComponent(jLabel35)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(ordPorPrecioP, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(ordPorHoraSalidaP, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(ordPorHoraLlegadaP, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(ordPorTransporte, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addGap(15, 15, 15))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panelConsultarServiciosPLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(panelConsultarServiciosPLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelConsultarServiciosPLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(informacionPersonal6, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(consultarServiciosP6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addComponent(cerrarSeson9, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(panelConsultarServiciosPLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelConsultarServiciosPLayout.createSequentialGroup()
                                .addGroup(panelConsultarServiciosPLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel33)
                                    .addComponent(jLabel34))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 29, Short.MAX_VALUE)
                                .addGroup(panelConsultarServiciosPLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(origenServicioP, javax.swing.GroupLayout.DEFAULT_SIZE, 174, Short.MAX_VALUE)
                                    .addComponent(destinoServicioP))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(buscarServiciosP, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(panelConsultarServiciosPLayout.createSequentialGroup()
                                .addComponent(lblMensajeServiciosP, javax.swing.GroupLayout.PREFERRED_SIZE, 297, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE)))))
                .addGap(17, 17, 17))
        );
        panelConsultarServiciosPLayout.setVerticalGroup(
            panelConsultarServiciosPLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelConsultarServiciosPLayout.createSequentialGroup()
                .addGroup(panelConsultarServiciosPLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelConsultarServiciosPLayout.createSequentialGroup()
                        .addGroup(panelConsultarServiciosPLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelConsultarServiciosPLayout.createSequentialGroup()
                                .addGap(12, 12, 12)
                                .addGroup(panelConsultarServiciosPLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel33)
                                    .addComponent(origenServicioP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(panelConsultarServiciosPLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel34)
                                    .addComponent(destinoServicioP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(panelConsultarServiciosPLayout.createSequentialGroup()
                                .addGap(27, 27, 27)
                                .addComponent(buscarServiciosP, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lblMensajeServiciosP, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelConsultarServiciosPLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(informacionPersonal6, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(consultarServiciosP6, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cerrarSeson9, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(22, 22, 22)
                .addComponent(jLabel37, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTextField6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 178, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelConsultarServiciosPLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblMensajeCompraP, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelConsultarServiciosPLayout.createSequentialGroup()
                        .addGroup(panelConsultarServiciosPLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(ordPorTransporte, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(ordPorHoraLlegadaP, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(ordPorHoraSalidaP, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(ordPorPrecioP, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel35))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(comprarPasajesP6, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(152, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout panelMenuPasajeroLayout = new javax.swing.GroupLayout(panelMenuPasajero);
        panelMenuPasajero.setLayout(panelMenuPasajeroLayout);
        panelMenuPasajeroLayout.setHorizontalGroup(
            panelMenuPasajeroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelMenuPasajeroLayout.createSequentialGroup()
                .addComponent(panelInicialP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 103, Short.MAX_VALUE))
            .addGroup(panelMenuPasajeroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(panelMenuPasajeroLayout.createSequentialGroup()
                    .addComponent(panelModificarP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
            .addGroup(panelMenuPasajeroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(panelMenuPasajeroLayout.createSequentialGroup()
                    .addComponent(panelConsultarServiciosP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 102, Short.MAX_VALUE)))
            .addGroup(panelMenuPasajeroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(panelMenuPasajeroLayout.createSequentialGroup()
                    .addComponent(panelCompraViajeP, javax.swing.GroupLayout.PREFERRED_SIZE, 591, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 99, Short.MAX_VALUE)))
        );
        panelMenuPasajeroLayout.setVerticalGroup(
            panelMenuPasajeroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelInicialP, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(panelMenuPasajeroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(panelModificarP, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 645, Short.MAX_VALUE))
            .addGroup(panelMenuPasajeroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(panelConsultarServiciosP, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 645, Short.MAX_VALUE))
            .addGroup(panelMenuPasajeroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(panelCompraViajeP, javax.swing.GroupLayout.DEFAULT_SIZE, 645, Short.MAX_VALUE))
        );

        panelInicial.setPreferredSize(new java.awt.Dimension(690, 521));

        procesarArchivos3.setText("Procesar Archivos");
        procesarArchivos3.setMaximumSize(new java.awt.Dimension(135, 29));
        procesarArchivos3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                procesarArchivos3ActionPerformed(evt);
            }
        });

        crearFuncionario3.setText("Crear Funcionario");
        crearFuncionario3.setMaximumSize(new java.awt.Dimension(135, 29));
        crearFuncionario3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                crearFuncionario3ActionPerformed(evt);
            }
        });

        registrarServicio3.setText("Registrar Servicio");
        registrarServicio3.setMaximumSize(new java.awt.Dimension(135, 29));
        registrarServicio3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                registrarServicio3ActionPerformed(evt);
            }
        });

        modificarServicio4.setText("Modificar Servicio");
        modificarServicio4.setMaximumSize(new java.awt.Dimension(135, 29));
        modificarServicio4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modificarServicio4ActionPerformed(evt);
            }
        });

        cambiarDatosPasajero4.setText("Cambiar Datos Pasajero");
        cambiarDatosPasajero4.setMaximumSize(new java.awt.Dimension(135, 29));
        cambiarDatosPasajero4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cambiarDatosPasajero4ActionPerformed(evt);
            }
        });

        consultarServicio4.setText("Consultar Servicios");
        consultarServicio4.setMaximumSize(new java.awt.Dimension(135, 29));
        consultarServicio4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                consultarServicio4ActionPerformed(evt);
            }
        });

        comprarPasaje4.setText("Comprar Pasaje");
        comprarPasaje4.setMaximumSize(new java.awt.Dimension(135, 29));
        comprarPasaje4.setPreferredSize(new java.awt.Dimension(159, 25));
        comprarPasaje4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comprarPasaje4ActionPerformed(evt);
            }
        });

        cerrarSesion4.setText("Cerrar Sesion");
        cerrarSesion4.setMaximumSize(new java.awt.Dimension(135, 29));
        cerrarSesion4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cerrarSesion4ActionPerformed(evt);
            }
        });

        jButton11.setText("Informacion Pasajeros");
        jButton11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton11ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelInicialLayout = new javax.swing.GroupLayout(panelInicial);
        panelInicial.setLayout(panelInicialLayout);
        panelInicialLayout.setHorizontalGroup(
            panelInicialLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelInicialLayout.createSequentialGroup()
                .addGroup(panelInicialLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelInicialLayout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addGroup(panelInicialLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(procesarArchivos3, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(registrarServicio3, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(crearFuncionario3, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(modificarServicio4, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cambiarDatosPasajero4, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(consultarServicio4, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(comprarPasaje4, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton11, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(panelInicialLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(cerrarSesion4, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        panelInicialLayout.setVerticalGroup(
            panelInicialLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelInicialLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(procesarArchivos3, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(registrarServicio3, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(crearFuncionario3, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(modificarServicio4, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(cambiarDatosPasajero4, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(consultarServicio4, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(comprarPasaje4, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton11, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cerrarSesion4, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(157, Short.MAX_VALUE))
        );

        panelRegistroServicio.setPreferredSize(new java.awt.Dimension(690, 521));

        rLunes.setText("Lunes");
        rLunes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rLunesActionPerformed(evt);
            }
        });

        rMartes.setText("Martes");

        rMiercoles.setText("Miercoles");

        rJueves.setText("Jueves");

        rViernes.setText("Viernes");

        rSabado.setText("Sabado");

        rDomingo.setText("Domingo");

        jLabel6.setText("Dias disponibles:");

        tiposTransporte.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Avion", "Barco", "Omnibus", "Remisse" }));
        tiposTransporte.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tiposTransporteActionPerformed(evt);
            }
        });

        jLabel7.setText("Tipo de Transporte:");

        txtDestino.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDestinoActionPerformed(evt);
            }
        });

        txtHora.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtHoraActionPerformed(evt);
            }
        });

        jLabel8.setText("Identificador:");

        jLabel9.setText("Origen:");

        jLabel10.setText("Destino:");

        jLabel11.setText("Salida(hh:mm):");

        jLabel12.setText("Llegada(hh:mm):");

        jLabel14.setText("Asientos:");

        jLabel15.setText("Precio:");

        registroServicio.setText("Registrar");
        registroServicio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                registroServicioActionPerformed(evt);
            }
        });

        procesarArchivos.setText("Procesar Archivos");
        procesarArchivos.setMaximumSize(new java.awt.Dimension(135, 29));
        procesarArchivos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                procesarArchivosActionPerformed(evt);
            }
        });

        crearFuncionario.setText("Crear Funcionario");
        crearFuncionario.setMaximumSize(new java.awt.Dimension(135, 29));
        crearFuncionario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                crearFuncionarioActionPerformed(evt);
            }
        });

        registrarServicio.setText("Registrar Servicio");
        registrarServicio.setMaximumSize(new java.awt.Dimension(135, 29));
        registrarServicio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                registrarServicioActionPerformed(evt);
            }
        });

        modificarServicio.setText("Modificar Servicio");
        modificarServicio.setMaximumSize(new java.awt.Dimension(135, 29));
        modificarServicio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modificarServicioActionPerformed(evt);
            }
        });

        cambiarDatosPasajero.setText("Cambiar Datos Pasajero");
        cambiarDatosPasajero.setMaximumSize(new java.awt.Dimension(135, 29));
        cambiarDatosPasajero.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cambiarDatosPasajeroActionPerformed(evt);
            }
        });

        consultarServicio.setText("Consultar Servicios");
        consultarServicio.setMaximumSize(new java.awt.Dimension(135, 29));
        consultarServicio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                consultarServicioActionPerformed(evt);
            }
        });

        comprarPasaje.setText("Comprar Pasaje");
        comprarPasaje.setMaximumSize(new java.awt.Dimension(135, 29));
        comprarPasaje.setPreferredSize(new java.awt.Dimension(159, 25));
        comprarPasaje.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comprarPasajeActionPerformed(evt);
            }
        });

        cerrarSesion7.setText("Cerrar Sesion");
        cerrarSesion7.setMaximumSize(new java.awt.Dimension(135, 29));
        cerrarSesion7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cerrarSesion7ActionPerformed(evt);
            }
        });

        jLabel50.setFont(new java.awt.Font("Lucida Grande", 0, 14)); // NOI18N
        jLabel50.setText("Registro nuevo servicio:");

        jButton10.setText("Informacion Pasajeros");
        jButton10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton10ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelRegistroServicioLayout = new javax.swing.GroupLayout(panelRegistroServicio);
        panelRegistroServicio.setLayout(panelRegistroServicioLayout);
        panelRegistroServicioLayout.setHorizontalGroup(
            panelRegistroServicioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelRegistroServicioLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addGroup(panelRegistroServicioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelRegistroServicioLayout.createSequentialGroup()
                        .addComponent(procesarArchivos, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(128, 128, 128)
                        .addComponent(jLabel50)
                        .addContainerGap())
                    .addGroup(panelRegistroServicioLayout.createSequentialGroup()
                        .addGroup(panelRegistroServicioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(registrarServicio, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(crearFuncionario, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(modificarServicio, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cambiarDatosPasajero, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(consultarServicio, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(comprarPasaje, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton10, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cerrarSesion7, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(panelRegistroServicioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelRegistroServicioLayout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(panelRegistroServicioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(panelRegistroServicioLayout.createSequentialGroup()
                                        .addGap(37, 37, 37)
                                        .addComponent(jLabel7)
                                        .addGap(28, 28, 28)
                                        .addComponent(tiposTransporte, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(panelRegistroServicioLayout.createSequentialGroup()
                                        .addGroup(panelRegistroServicioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel12)
                                            .addComponent(jLabel14)
                                            .addComponent(jLabel15)
                                            .addGroup(panelRegistroServicioLayout.createSequentialGroup()
                                                .addGap(2, 2, 2)
                                                .addGroup(panelRegistroServicioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addComponent(jLabel8)
                                                    .addComponent(jLabel9)
                                                    .addComponent(jLabel10)
                                                    .addComponent(jLabel11))))
                                        .addGap(12, 12, 12)
                                        .addGroup(panelRegistroServicioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(txtIdentificador, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(txtOrigen, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(txtDestino, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(txtHora, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(txtDuracion, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(txtAsientos, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(txtPrecio, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(6, 6, 6)
                                        .addGroup(panelRegistroServicioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel6)
                                            .addGroup(panelRegistroServicioLayout.createSequentialGroup()
                                                .addGap(7, 7, 7)
                                                .addGroup(panelRegistroServicioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addComponent(rLunes)
                                                    .addComponent(rMartes)
                                                    .addComponent(rMiercoles)
                                                    .addComponent(rJueves)
                                                    .addComponent(rViernes)
                                                    .addComponent(rSabado)
                                                    .addComponent(rDomingo))))))
                                .addContainerGap(129, Short.MAX_VALUE))
                            .addGroup(panelRegistroServicioLayout.createSequentialGroup()
                                .addGap(119, 119, 119)
                                .addComponent(registroServicio, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelRegistroServicioLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblMensajeServicio, javax.swing.GroupLayout.PREFERRED_SIZE, 520, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(62, 62, 62))
        );
        panelRegistroServicioLayout.setVerticalGroup(
            panelRegistroServicioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelRegistroServicioLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addGroup(panelRegistroServicioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel50)
                    .addComponent(procesarArchivos, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(panelRegistroServicioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelRegistroServicioLayout.createSequentialGroup()
                        .addGroup(panelRegistroServicioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelRegistroServicioLayout.createSequentialGroup()
                                .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(20, 20, 20)
                                .addComponent(jLabel9)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel10)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel11)
                                .addGap(21, 21, 21)
                                .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(22, 22, 22)
                                .addComponent(jLabel14)
                                .addGap(24, 24, 24)
                                .addComponent(jLabel15))
                            .addGroup(panelRegistroServicioLayout.createSequentialGroup()
                                .addComponent(txtIdentificador, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(6, 6, 6)
                                .addComponent(txtOrigen, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(6, 6, 6)
                                .addComponent(txtDestino, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(6, 6, 6)
                                .addComponent(txtHora, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(11, 11, 11)
                                .addComponent(txtDuracion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(12, 12, 12)
                                .addComponent(txtAsientos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(12, 12, 12)
                                .addComponent(txtPrecio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(panelRegistroServicioLayout.createSequentialGroup()
                                .addComponent(jLabel6)
                                .addGap(12, 12, 12)
                                .addComponent(rLunes)
                                .addGap(6, 6, 6)
                                .addComponent(rMartes)
                                .addGap(12, 12, 12)
                                .addComponent(rMiercoles)
                                .addGap(6, 6, 6)
                                .addComponent(rJueves)
                                .addGap(12, 12, 12)
                                .addComponent(rViernes)
                                .addGap(6, 6, 6)
                                .addComponent(rSabado)
                                .addGap(12, 12, 12)
                                .addComponent(rDomingo)))
                        .addGap(6, 6, 6)
                        .addGroup(panelRegistroServicioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelRegistroServicioLayout.createSequentialGroup()
                                .addGap(19, 19, 19)
                                .addComponent(jLabel7))
                            .addGroup(panelRegistroServicioLayout.createSequentialGroup()
                                .addGap(15, 15, 15)
                                .addComponent(tiposTransporte, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(registroServicio, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelRegistroServicioLayout.createSequentialGroup()
                        .addComponent(registrarServicio, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(crearFuncionario, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(modificarServicio, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(cambiarDatosPasajero, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(consultarServicio, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(comprarPasaje, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton10, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cerrarSesion7, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(26, 26, 26)
                .addComponent(lblMensajeServicio, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(42, 42, 42))
        );

        panelRegistrarFuncionario.setPreferredSize(new java.awt.Dimension(690, 521));

        registrarNuevoFunc.setText("Registrar");
        registrarNuevoFunc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                registrarNuevoFuncActionPerformed(evt);
            }
        });

        txtNombreFunc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNombreFuncActionPerformed(evt);
            }
        });

        jLabel4.setText("Nombre: ");

        jLabel5.setText("Cedula:");

        jLabel17.setFont(new java.awt.Font("Lucida Grande", 0, 14)); // NOI18N
        jLabel17.setText("Nuevo Funcionario:");

        procesarArchivos1.setText("Procesar Archivos");
        procesarArchivos1.setMaximumSize(new java.awt.Dimension(135, 0));
        procesarArchivos1.setPreferredSize(new java.awt.Dimension(171, 25));
        procesarArchivos1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                procesarArchivos1ActionPerformed(evt);
            }
        });

        crearFuncionario1.setText("Crear Funcionario");
        crearFuncionario1.setMaximumSize(new java.awt.Dimension(135, 0));
        crearFuncionario1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                crearFuncionario1ActionPerformed(evt);
            }
        });

        registrarServicio1.setText("Registrar Servicio");
        registrarServicio1.setMaximumSize(new java.awt.Dimension(135, 0));
        registrarServicio1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                registrarServicio1ActionPerformed(evt);
            }
        });

        modificarServicio1.setText("Modificar Servicio");
        modificarServicio1.setMaximumSize(new java.awt.Dimension(135, 0));
        modificarServicio1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modificarServicio1ActionPerformed(evt);
            }
        });

        cambiarDatosPasajero1.setText("Cambiar Datos Pasajero");
        cambiarDatosPasajero1.setMaximumSize(new java.awt.Dimension(135, 0));
        cambiarDatosPasajero1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cambiarDatosPasajero1ActionPerformed(evt);
            }
        });

        consultarServicio1.setText("Consultar Servicios");
        consultarServicio1.setMaximumSize(new java.awt.Dimension(135, 0));
        consultarServicio1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                consultarServicio1ActionPerformed(evt);
            }
        });

        comprarPasaje1.setText("Comprar Pasaje");
        comprarPasaje1.setMaximumSize(new java.awt.Dimension(135, 0));
        comprarPasaje1.setPreferredSize(new java.awt.Dimension(159, 25));
        comprarPasaje1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comprarPasaje1ActionPerformed(evt);
            }
        });

        cerrarSesion6.setText("Cerrar Sesion");
        cerrarSesion6.setMaximumSize(new java.awt.Dimension(135, 0));
        cerrarSesion6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cerrarSesion6ActionPerformed(evt);
            }
        });

        jButton9.setText("Informacion Pasajeros");
        jButton9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton9ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelRegistrarFuncionarioLayout = new javax.swing.GroupLayout(panelRegistrarFuncionario);
        panelRegistrarFuncionario.setLayout(panelRegistrarFuncionarioLayout);
        panelRegistrarFuncionarioLayout.setHorizontalGroup(
            panelRegistrarFuncionarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelRegistrarFuncionarioLayout.createSequentialGroup()
                .addGroup(panelRegistrarFuncionarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelRegistrarFuncionarioLayout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addGroup(panelRegistrarFuncionarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelRegistrarFuncionarioLayout.createSequentialGroup()
                                .addComponent(procesarArchivos1, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(147, 147, 147)
                                .addComponent(jLabel17))
                            .addComponent(registrarServicio1, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(panelRegistrarFuncionarioLayout.createSequentialGroup()
                                .addGroup(panelRegistrarFuncionarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(crearFuncionario1, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(modificarServicio1, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(37, 37, 37)
                                .addGroup(panelRegistrarFuncionarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel4)
                                    .addComponent(jLabel5))
                                .addGap(18, 18, 18)
                                .addGroup(panelRegistrarFuncionarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txtNombreFunc, javax.swing.GroupLayout.DEFAULT_SIZE, 210, Short.MAX_VALUE)
                                    .addComponent(txtCedulaFunc)))
                            .addGroup(panelRegistrarFuncionarioLayout.createSequentialGroup()
                                .addGroup(panelRegistrarFuncionarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(consultarServicio1, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(comprarPasaje1, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(123, 123, 123)
                                .addComponent(registrarNuevoFunc, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(panelRegistrarFuncionarioLayout.createSequentialGroup()
                                .addComponent(cambiarDatosPasajero1, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(81, 81, 81)
                                .addComponent(lblMensajeCreacionFunc, javax.swing.GroupLayout.PREFERRED_SIZE, 349, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(panelRegistrarFuncionarioLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(panelRegistrarFuncionarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cerrarSesion6, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton9, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(19, Short.MAX_VALUE))
        );
        panelRegistrarFuncionarioLayout.setVerticalGroup(
            panelRegistrarFuncionarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelRegistrarFuncionarioLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addGroup(panelRegistrarFuncionarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(procesarArchivos1, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel17))
                .addGap(6, 6, 6)
                .addComponent(registrarServicio1, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addGroup(panelRegistrarFuncionarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(panelRegistrarFuncionarioLayout.createSequentialGroup()
                        .addGroup(panelRegistrarFuncionarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelRegistrarFuncionarioLayout.createSequentialGroup()
                                .addGroup(panelRegistrarFuncionarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel4)
                                    .addComponent(txtNombreFunc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(12, 12, 12)
                                .addGroup(panelRegistrarFuncionarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel5)
                                    .addComponent(txtCedulaFunc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(panelRegistrarFuncionarioLayout.createSequentialGroup()
                                .addComponent(crearFuncionario1, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(6, 6, 6)
                                .addComponent(modificarServicio1, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(6, 6, 6)
                        .addComponent(cambiarDatosPasajero1, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(lblMensajeCreacionFunc, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(panelRegistrarFuncionarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelRegistrarFuncionarioLayout.createSequentialGroup()
                        .addComponent(consultarServicio1, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(comprarPasaje1, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(registrarNuevoFunc, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton9, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cerrarSesion6, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(166, Short.MAX_VALUE))
        );

        panelProcesarArchivos.setPreferredSize(new java.awt.Dimension(690, 521));

        procesarArchivos2.setText("Procesar Archivos");
        procesarArchivos2.setMaximumSize(new java.awt.Dimension(135, 29));
        procesarArchivos2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                procesarArchivos2ActionPerformed(evt);
            }
        });

        crearFuncionario2.setText("Crear Funcionario");
        crearFuncionario2.setMaximumSize(new java.awt.Dimension(135, 29));
        crearFuncionario2.setMinimumSize(new java.awt.Dimension(135, 29));
        crearFuncionario2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                crearFuncionario2ActionPerformed(evt);
            }
        });

        registrarServicio2.setText("Registrar Servicio");
        registrarServicio2.setMaximumSize(new java.awt.Dimension(135, 29));
        registrarServicio2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                registrarServicio2ActionPerformed(evt);
            }
        });

        modificarServicio2.setText("Modificar Servicio");
        modificarServicio2.setMaximumSize(new java.awt.Dimension(135, 29));
        modificarServicio2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modificarServicio2ActionPerformed(evt);
            }
        });

        cambiarDatosPasajero2.setText("Cambiar Datos Pasajero");
        cambiarDatosPasajero2.setMaximumSize(new java.awt.Dimension(135, 29));
        cambiarDatosPasajero2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cambiarDatosPasajero2ActionPerformed(evt);
            }
        });

        consultarServicio2.setText("Consultar Servicios");
        consultarServicio2.setMaximumSize(new java.awt.Dimension(135, 29));
        consultarServicio2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                consultarServicio2ActionPerformed(evt);
            }
        });

        comprarPasaje2.setText("Comprar Pasaje");
        comprarPasaje2.setMaximumSize(new java.awt.Dimension(135, 29));
        comprarPasaje2.setPreferredSize(new java.awt.Dimension(159, 25));
        comprarPasaje2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comprarPasaje2ActionPerformed(evt);
            }
        });

        txtInfoArchivos.setColumns(20);
        txtInfoArchivos.setFont(new java.awt.Font("Lucida Grande", 0, 14)); // NOI18N
        txtInfoArchivos.setRows(5);
        jScrollPane3.setViewportView(txtInfoArchivos);

        jLabel29.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        jLabel29.setText("Procesamiento de Archivos:");

        cerrarSesion5.setText("Cerrar Sesion");
        cerrarSesion5.setMaximumSize(new java.awt.Dimension(135, 29));
        cerrarSesion5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cerrarSesion5ActionPerformed(evt);
            }
        });

        jButton8.setText("Informacion Pasajeros");
        jButton8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton8ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelProcesarArchivosLayout = new javax.swing.GroupLayout(panelProcesarArchivos);
        panelProcesarArchivos.setLayout(panelProcesarArchivosLayout);
        panelProcesarArchivosLayout.setHorizontalGroup(
            panelProcesarArchivosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelProcesarArchivosLayout.createSequentialGroup()
                .addGroup(panelProcesarArchivosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelProcesarArchivosLayout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addGroup(panelProcesarArchivosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelProcesarArchivosLayout.createSequentialGroup()
                                .addComponent(procesarArchivos2, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(107, 107, 107)
                                .addComponent(jLabel29))
                            .addComponent(comprarPasaje2, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(panelProcesarArchivosLayout.createSequentialGroup()
                                .addGroup(panelProcesarArchivosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(registrarServicio2, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(crearFuncionario2, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(modificarServicio2, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(cambiarDatosPasajero2, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(consultarServicio2, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(41, 41, 41)
                                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 366, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(panelProcesarArchivosLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(panelProcesarArchivosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cerrarSesion5, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton8, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(107, Short.MAX_VALUE))
        );
        panelProcesarArchivosLayout.setVerticalGroup(
            panelProcesarArchivosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelProcesarArchivosLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addGroup(panelProcesarArchivosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(procesarArchivos2, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(panelProcesarArchivosLayout.createSequentialGroup()
                        .addGap(7, 7, 7)
                        .addComponent(jLabel29)))
                .addGap(6, 6, 6)
                .addGroup(panelProcesarArchivosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelProcesarArchivosLayout.createSequentialGroup()
                        .addComponent(registrarServicio2, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(crearFuncionario2, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(modificarServicio2, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(cambiarDatosPasajero2, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(consultarServicio2, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addComponent(comprarPasaje2, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton8, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cerrarSesion5, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(92, Short.MAX_VALUE))
        );

        panelModificarServicio.setPreferredSize(new java.awt.Dimension(690, 521));

        nuevaHoraFin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nuevaHoraFinActionPerformed(evt);
            }
        });

        jLabel20.setText("Hora Fin (hh:mm):");

        jLabel22.setText("Hora Inicio (hh:mm):");

        guardarTransporte.setText("Guardar");
        guardarTransporte.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                guardarTransporteActionPerformed(evt);
            }
        });

        jLabel18.setFont(new java.awt.Font("Comic Sans MS", 0, 16)); // NOI18N
        jLabel18.setText("Seleccione el servicio que desea modificar:");

        procesarArchivos4.setText("Procesar Archivos");
        procesarArchivos4.setMaximumSize(new java.awt.Dimension(135, 29));
        procesarArchivos4.setPreferredSize(new java.awt.Dimension(171, 25));
        procesarArchivos4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                procesarArchivos4ActionPerformed(evt);
            }
        });

        registrarServicio4.setText("Registrar Servicio");
        registrarServicio4.setMaximumSize(new java.awt.Dimension(135, 29));
        registrarServicio4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                registrarServicio4ActionPerformed(evt);
            }
        });

        crearFuncionario4.setText("Crear Funcionario");
        crearFuncionario4.setMaximumSize(new java.awt.Dimension(135, 29));
        crearFuncionario4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                crearFuncionario4ActionPerformed(evt);
            }
        });

        modificarServicio3.setText("Modificar Servicio");
        modificarServicio3.setMaximumSize(new java.awt.Dimension(135, 29));
        modificarServicio3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modificarServicio3ActionPerformed(evt);
            }
        });

        cambiarDatosPasajero3.setText("Cambiar Datos Pasajero");
        cambiarDatosPasajero3.setMaximumSize(new java.awt.Dimension(135, 29));
        cambiarDatosPasajero3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cambiarDatosPasajero3ActionPerformed(evt);
            }
        });

        consultarServicio3.setText("Consultar Servicios");
        consultarServicio3.setMaximumSize(new java.awt.Dimension(135, 29));
        consultarServicio3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                consultarServicio3ActionPerformed(evt);
            }
        });

        comprarPasaje3.setText("Comprar Pasaje");
        comprarPasaje3.setHideActionText(true);
        comprarPasaje3.setMaximumSize(new java.awt.Dimension(135, 29));
        comprarPasaje3.setPreferredSize(new java.awt.Dimension(159, 25));
        comprarPasaje3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comprarPasaje3ActionPerformed(evt);
            }
        });

        jScrollPane2.setViewportView(jListaServicios);

        cerrarSesion3.setText("Cerrar Sesion");
        cerrarSesion3.setMaximumSize(new java.awt.Dimension(135, 29));
        cerrarSesion3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cerrarSesion3ActionPerformed(evt);
            }
        });

        jLabel51.setFont(new java.awt.Font("Lucida Grande", 0, 10)); // NOI18N
        jLabel51.setText("Origen|Destino|Salida|Llegada|Frecuencia|Precio|Transporte|Disponibles");

        jButton7.setText("Informacion Pasajeros");
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelModificarServicioLayout = new javax.swing.GroupLayout(panelModificarServicio);
        panelModificarServicio.setLayout(panelModificarServicioLayout);
        panelModificarServicioLayout.setHorizontalGroup(
            panelModificarServicioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelModificarServicioLayout.createSequentialGroup()
                .addGroup(panelModificarServicioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelModificarServicioLayout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(procesarArchivos4, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(44, 44, 44)
                        .addComponent(jLabel18))
                    .addGroup(panelModificarServicioLayout.createSequentialGroup()
                        .addGroup(panelModificarServicioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelModificarServicioLayout.createSequentialGroup()
                                .addGap(6, 6, 6)
                                .addGroup(panelModificarServicioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(registrarServicio4, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(crearFuncionario4, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(modificarServicio3, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(cambiarDatosPasajero3, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(consultarServicio3, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(comprarPasaje3, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(panelModificarServicioLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jButton7, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(panelModificarServicioLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(cerrarSesion3, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGroup(panelModificarServicioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelModificarServicioLayout.createSequentialGroup()
                                .addGap(34, 34, 34)
                                .addGroup(panelModificarServicioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel51, javax.swing.GroupLayout.PREFERRED_SIZE, 360, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(panelModificarServicioLayout.createSequentialGroup()
                                        .addGap(10, 10, 10)
                                        .addGroup(panelModificarServicioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(panelModificarServicioLayout.createSequentialGroup()
                                                .addGroup(panelModificarServicioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addComponent(jLabel22, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addComponent(jLabel20))
                                                .addGap(21, 21, 21)
                                                .addGroup(panelModificarServicioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addComponent(nuevaHoraInicio, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addComponent(nuevaHoraFin, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                            .addGroup(panelModificarServicioLayout.createSequentialGroup()
                                                .addGap(6, 6, 6)
                                                .addComponent(resultadoModificarServicio, javax.swing.GroupLayout.PREFERRED_SIZE, 370, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 370, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(panelModificarServicioLayout.createSequentialGroup()
                                .addGap(162, 162, 162)
                                .addComponent(guardarTransporte, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap(94, Short.MAX_VALUE))
        );
        panelModificarServicioLayout.setVerticalGroup(
            panelModificarServicioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelModificarServicioLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addGroup(panelModificarServicioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(procesarArchivos4, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(panelModificarServicioLayout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addComponent(jLabel18)))
                .addGap(6, 6, 6)
                .addGroup(panelModificarServicioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelModificarServicioLayout.createSequentialGroup()
                        .addComponent(registrarServicio4, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(crearFuncionario4, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(modificarServicio3, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(cambiarDatosPasajero3, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(consultarServicio3, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(comprarPasaje3, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton7, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cerrarSesion3, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelModificarServicioLayout.createSequentialGroup()
                        .addGap(16, 16, 16)
                        .addComponent(jLabel51)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 208, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(13, 13, 13)
                        .addGroup(panelModificarServicioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelModificarServicioLayout.createSequentialGroup()
                                .addComponent(jLabel22, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(9, 9, 9)
                                .addComponent(jLabel20))
                            .addGroup(panelModificarServicioLayout.createSequentialGroup()
                                .addComponent(nuevaHoraInicio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(2, 2, 2)
                                .addComponent(nuevaHoraFin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(12, 12, 12)
                        .addComponent(resultadoModificarServicio, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(guardarTransporte, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(67, 67, 67))
        );

        panelConsultarServicios.setPreferredSize(new java.awt.Dimension(690, 521));

        procesarArchivos5.setText("Procesar Archivos");
        procesarArchivos5.setPreferredSize(new java.awt.Dimension(159, 25));
        procesarArchivos5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                procesarArchivos5ActionPerformed(evt);
            }
        });

        registrarServicio5.setText("Registrar Servicio");
        registrarServicio5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                registrarServicio5ActionPerformed(evt);
            }
        });

        crearFuncionario5.setText("Crear Funcionario");
        crearFuncionario5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                crearFuncionario5ActionPerformed(evt);
            }
        });

        cambiarDatosPasajero5.setText("Cambiar Datos Pasajero");
        cambiarDatosPasajero5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cambiarDatosPasajero5ActionPerformed(evt);
            }
        });

        modificarServicio5.setText("Modificar Servicio");
        modificarServicio5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modificarServicio5ActionPerformed(evt);
            }
        });

        consultarServicio5.setText("Consultar Servicios");
        consultarServicio5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                consultarServicio5ActionPerformed(evt);
            }
        });

        comprarPasaje5.setText("Comprar Pasaje");
        comprarPasaje5.setPreferredSize(new java.awt.Dimension(159, 25));
        comprarPasaje5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comprarPasaje5ActionPerformed(evt);
            }
        });

        jLabel19.setText("Ciudad Origen:");

        jLabel21.setText("Ciudad Destino:");

        ciudadOrigen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ciudadOrigenActionPerformed(evt);
            }
        });

        jScrollPane1.setViewportView(jListaServiciosEncontrados);

        jLabel23.setText("Ordenar por:");

        ordenarPrecio.setBackground(new java.awt.Color(255, 255, 255));
        ordenarPrecio.setText("Precio");
        ordenarPrecio.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        ordenarPrecio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ordenarPrecioActionPerformed(evt);
            }
        });

        horaSalida.setBackground(new java.awt.Color(255, 255, 255));
        horaSalida.setText("Hora Salida");
        horaSalida.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        horaSalida.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                horaSalidaActionPerformed(evt);
            }
        });

        horaLlegada.setBackground(new java.awt.Color(255, 255, 255));
        horaLlegada.setText("Hora Llegada");
        horaLlegada.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        horaLlegada.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                horaLlegadaActionPerformed(evt);
            }
        });

        transporte.setBackground(new java.awt.Color(255, 255, 255));
        transporte.setText("Transporte");
        transporte.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        transporte.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                transporteActionPerformed(evt);
            }
        });

        cargarServicios.setText("Cargar Servicios");
        cargarServicios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cargarServiciosActionPerformed(evt);
            }
        });

        jTextField3.setFont(new java.awt.Font("Lucida Grande", 0, 10)); // NOI18N
        jTextField3.setText("Origen|Destino|Salida|Llegada|Frecuencia|Precio|Transporte|Disponibles");
        jTextField3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField3ActionPerformed(evt);
            }
        });

        cerrarSesion2.setText("Cerrar Sesion");
        cerrarSesion2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cerrarSesion2ActionPerformed(evt);
            }
        });

        jButton6.setText("Informacion Pasajeros");
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelConsultarServiciosLayout = new javax.swing.GroupLayout(panelConsultarServicios);
        panelConsultarServicios.setLayout(panelConsultarServiciosLayout);
        panelConsultarServiciosLayout.setHorizontalGroup(
            panelConsultarServiciosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelConsultarServiciosLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelConsultarServiciosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(procesarArchivos5, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(registrarServicio5, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(crearFuncionario5, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(modificarServicio5, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cambiarDatosPasajero5, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(consultarServicio5, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(comprarPasaje5, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton6, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cerrarSesion2, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(41, 41, 41)
                .addGroup(panelConsultarServiciosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelConsultarServiciosLayout.createSequentialGroup()
                        .addComponent(jLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(ciudadOrigen, javax.swing.GroupLayout.PREFERRED_SIZE, 204, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelConsultarServiciosLayout.createSequentialGroup()
                        .addComponent(jLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(ciudadDestino, javax.swing.GroupLayout.PREFERRED_SIZE, 204, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelConsultarServiciosLayout.createSequentialGroup()
                        .addComponent(cargarServicios, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(resultadoCargarServicios, javax.swing.GroupLayout.PREFERRED_SIZE, 314, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(panelConsultarServiciosLayout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addGroup(panelConsultarServiciosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(horaSalida, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel23, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(horaLlegada)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(ordenarPrecio, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(transporte, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 370, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panelConsultarServiciosLayout.setVerticalGroup(
            panelConsultarServiciosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelConsultarServiciosLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addGroup(panelConsultarServiciosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelConsultarServiciosLayout.createSequentialGroup()
                        .addComponent(procesarArchivos5, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(registrarServicio5, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(crearFuncionario5, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(modificarServicio5, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(cambiarDatosPasajero5, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(consultarServicio5, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(comprarPasaje5, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton6, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cerrarSesion2, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelConsultarServiciosLayout.createSequentialGroup()
                        .addGroup(panelConsultarServiciosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(ciudadOrigen, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(6, 6, 6)
                        .addGroup(panelConsultarServiciosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelConsultarServiciosLayout.createSequentialGroup()
                                .addGap(2, 2, 2)
                                .addComponent(jLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(ciudadDestino, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(6, 6, 6)
                        .addGroup(panelConsultarServiciosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cargarServicios, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(resultadoCargarServicios, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(7, 7, 7)
                        .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 231, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel23)
                        .addGap(19, 19, 19)
                        .addGroup(panelConsultarServiciosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(transporte, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(ordenarPrecio, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(horaLlegada, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(horaSalida, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(64, Short.MAX_VALUE))
        );

        procesarArchivos6.setText("Procesar Archivos");
        procesarArchivos6.setPreferredSize(new java.awt.Dimension(159, 25));
        procesarArchivos6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                procesarArchivos6ActionPerformed(evt);
            }
        });

        registrarServicio6.setText("Registrar Servicio");
        registrarServicio6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                registrarServicio6ActionPerformed(evt);
            }
        });

        crearFuncionario6.setText("Crear Funcionario");
        crearFuncionario6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                crearFuncionario6ActionPerformed(evt);
            }
        });

        modificarServicio6.setText("Modificar Servicio");
        modificarServicio6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modificarServicio6ActionPerformed(evt);
            }
        });

        cambiarDatosPasajero6.setText("Cambiar Datos Pasajero");
        cambiarDatosPasajero6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cambiarDatosPasajero6ActionPerformed(evt);
            }
        });

        consultarServicio6.setText("Consultar Servicios");
        consultarServicio6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                consultarServicio6ActionPerformed(evt);
            }
        });

        comprarPasaje6.setText("Comprar Pasaje");
        comprarPasaje6.setPreferredSize(new java.awt.Dimension(159, 25));
        comprarPasaje6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comprarPasaje6ActionPerformed(evt);
            }
        });

        jLabel24.setText("Nuevo Nombre:");

        jLabel25.setText("Nueva Cedula:");

        guardarPasajero.setText("Modificar");
        guardarPasajero.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                guardarPasajeroActionPerformed(evt);
            }
        });

        jLabel26.setText("Cedula Pasajero:");

        jLabel27.setFont(new java.awt.Font("Constantia", 0, 14)); // NOI18N
        jLabel27.setText("Modificar informacion de pasajero:");

        confirmarBaja.setText("Confirmar");

        confirmarCedula.setText("Confirmar");

        confirmarNombre.setText("Confirmar");
        confirmarNombre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                confirmarNombreActionPerformed(evt);
            }
        });

        jLabel28.setText("Dar de baja al pasajero seleccionado?");

        cerrarSesion.setText("Cerrar Sesion");
        cerrarSesion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cerrarSesionActionPerformed(evt);
            }
        });

        jButton5.setText("Informacion Pasajeros");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelModificarUsuarioLayout = new javax.swing.GroupLayout(panelModificarUsuario);
        panelModificarUsuario.setLayout(panelModificarUsuarioLayout);
        panelModificarUsuarioLayout.setHorizontalGroup(
            panelModificarUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelModificarUsuarioLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addGroup(panelModificarUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelModificarUsuarioLayout.createSequentialGroup()
                        .addComponent(procesarArchivos6, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(94, 94, 94)
                        .addComponent(jLabel27, javax.swing.GroupLayout.PREFERRED_SIZE, 320, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelModificarUsuarioLayout.createSequentialGroup()
                        .addGroup(panelModificarUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(registrarServicio6, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(crearFuncionario6, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(modificarServicio6, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(44, 44, 44)
                        .addGroup(panelModificarUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelModificarUsuarioLayout.createSequentialGroup()
                                .addComponent(jLabel26)
                                .addGap(17, 17, 17)
                                .addComponent(cedulaActual, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(panelModificarUsuarioLayout.createSequentialGroup()
                                .addComponent(jLabel25, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(16, 16, 16)
                                .addComponent(cedulaNueva, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(panelModificarUsuarioLayout.createSequentialGroup()
                                .addComponent(jLabel24)
                                .addGap(22, 22, 22)
                                .addComponent(nombreNuevo, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel28, javax.swing.GroupLayout.PREFERRED_SIZE, 243, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(12, 12, 12)
                        .addGroup(panelModificarUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(confirmarCedula)
                            .addComponent(confirmarNombre)
                            .addComponent(confirmarBaja)))
                    .addGroup(panelModificarUsuarioLayout.createSequentialGroup()
                        .addGroup(panelModificarUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cambiarDatosPasajero6, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(consultarServicio6, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(46, 46, 46)
                        .addComponent(guardarPasajero, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(panelModificarUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(resultadoUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(errorPasajeroNoEsta, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(comprarPasaje6, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(panelModificarUsuarioLayout.createSequentialGroup()
                        .addGroup(panelModificarUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cerrarSesion, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton5, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pasajeroBorrado, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(10, Short.MAX_VALUE))
        );
        panelModificarUsuarioLayout.setVerticalGroup(
            panelModificarUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelModificarUsuarioLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addGroup(panelModificarUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(procesarArchivos6, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(panelModificarUsuarioLayout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addComponent(jLabel27, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(2, 2, 2)
                .addGroup(panelModificarUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelModificarUsuarioLayout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addComponent(registrarServicio6, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(crearFuncionario6, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(modificarServicio6, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelModificarUsuarioLayout.createSequentialGroup()
                        .addGroup(panelModificarUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel26, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cedulaActual, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(12, 12, 12)
                        .addGroup(panelModificarUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel25)
                            .addComponent(cedulaNueva, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(12, 12, 12)
                        .addGroup(panelModificarUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel24)
                            .addComponent(nombreNuevo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(12, 12, 12)
                        .addComponent(jLabel28))
                    .addGroup(panelModificarUsuarioLayout.createSequentialGroup()
                        .addGap(40, 40, 40)
                        .addComponent(confirmarCedula)
                        .addGap(17, 17, 17)
                        .addComponent(confirmarNombre)
                        .addGap(17, 17, 17)
                        .addComponent(confirmarBaja)))
                .addGap(4, 4, 4)
                .addGroup(panelModificarUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelModificarUsuarioLayout.createSequentialGroup()
                        .addComponent(cambiarDatosPasajero6, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(consultarServicio6, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(comprarPasaje6, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(panelModificarUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelModificarUsuarioLayout.createSequentialGroup()
                                .addGap(45, 45, 45)
                                .addComponent(pasajeroBorrado, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(panelModificarUsuarioLayout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton5, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cerrarSesion, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(panelModificarUsuarioLayout.createSequentialGroup()
                        .addGap(13, 13, 13)
                        .addGroup(panelModificarUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(panelModificarUsuarioLayout.createSequentialGroup()
                                .addComponent(resultadoUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(errorPasajeroNoEsta, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(guardarPasajero, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(119, Short.MAX_VALUE))
        );

        panelCompraPasajeF.setPreferredSize(new java.awt.Dimension(690, 521));

        procesarArchivos7.setText("Procesar Archivos");
        procesarArchivos7.setPreferredSize(new java.awt.Dimension(159, 25));
        procesarArchivos7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                procesarArchivos7ActionPerformed(evt);
            }
        });

        registrarServicio7.setText("Registrar Servicio");
        registrarServicio7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                registrarServicio7ActionPerformed(evt);
            }
        });

        crearFuncionario7.setText("Crear Funcionario");
        crearFuncionario7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                crearFuncionario7ActionPerformed(evt);
            }
        });

        modificarServicio7.setText("Modificar Servicio");
        modificarServicio7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modificarServicio7ActionPerformed(evt);
            }
        });

        cambiarDatosPasajero7.setText("Cambiar Datos Pasajero");
        cambiarDatosPasajero7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cambiarDatosPasajero7ActionPerformed(evt);
            }
        });

        consultarServicio7.setText("Consultar Servicios");
        consultarServicio7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                consultarServicio7ActionPerformed(evt);
            }
        });

        comprarPasaje7.setText("Comprar Pasaje");
        comprarPasaje7.setPreferredSize(new java.awt.Dimension(159, 25));
        comprarPasaje7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comprarPasaje7ActionPerformed(evt);
            }
        });

        cerrarSesion1.setText("Cerrar Sesion");
        cerrarSesion1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cerrarSesion1ActionPerformed(evt);
            }
        });

        informacionPasajero7.setText("Informacion Pasajeros");
        informacionPasajero7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                informacionPasajero7ActionPerformed(evt);
            }
        });

        jLabel30.setFont(new java.awt.Font("Constantia", 0, 16)); // NOI18N
        jLabel30.setText("Seleccione servicio:");

        cedulaPasajero.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cedulaPasajeroActionPerformed(evt);
            }
        });

        jLabel31.setText("Cedula Pasajero:");

        comprarPasajeAPasajero.setText("Comprar");
        comprarPasajeAPasajero.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comprarPasajeAPasajeroActionPerformed(evt);
            }
        });

        jTextField1.setFont(new java.awt.Font("Lucida Grande", 0, 10)); // NOI18N
        jTextField1.setText("Origen|Destino|Salida|Llegada|Frecuencia|Precio|Transporte|Disponibles");
        jTextField1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField1ActionPerformed(evt);
            }
        });

        jLabel32.setText("Cantidad Asientos:");

        cantidadAsientos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cantidadAsientosActionPerformed(evt);
            }
        });

        jLabel41.setText("Dia:");

        jComboBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox1ActionPerformed(evt);
            }
        });

        jListaServiciosCompra.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jListaServiciosCompraMouseClicked(evt);
            }
        });
        jScrollPane10.setViewportView(jListaServiciosCompra);

        javax.swing.GroupLayout panelCompraPasajeFLayout = new javax.swing.GroupLayout(panelCompraPasajeF);
        panelCompraPasajeF.setLayout(panelCompraPasajeFLayout);
        panelCompraPasajeFLayout.setHorizontalGroup(
            panelCompraPasajeFLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelCompraPasajeFLayout.createSequentialGroup()
                .addGroup(panelCompraPasajeFLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelCompraPasajeFLayout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(procesarArchivos7, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(132, 132, 132)
                        .addComponent(jLabel30, javax.swing.GroupLayout.PREFERRED_SIZE, 232, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelCompraPasajeFLayout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addGroup(panelCompraPasajeFLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(registrarServicio7, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(crearFuncionario7, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(modificarServicio7, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cambiarDatosPasajero7, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(consultarServicio7, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(29, 29, 29)
                        .addGroup(panelCompraPasajeFLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(panelCompraPasajeFLayout.createSequentialGroup()
                                .addGap(6, 6, 6)
                                .addComponent(jScrollPane10))
                            .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 378, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(panelCompraPasajeFLayout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(comprarPasaje7, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(72, 72, 72)
                        .addComponent(jLabel31, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(cedulaPasajero, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelCompraPasajeFLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(panelCompraPasajeFLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(informacionPasajero7, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cerrarSesion1, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(26, 26, 26)
                        .addGroup(panelCompraPasajeFLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(mensajeCedula, javax.swing.GroupLayout.PREFERRED_SIZE, 327, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(panelCompraPasajeFLayout.createSequentialGroup()
                                .addGap(46, 46, 46)
                                .addGroup(panelCompraPasajeFLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(panelCompraPasajeFLayout.createSequentialGroup()
                                        .addComponent(jLabel32)
                                        .addGap(12, 12, 12)
                                        .addComponent(cantidadAsientos, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(panelCompraPasajeFLayout.createSequentialGroup()
                                        .addComponent(jLabel41)
                                        .addGap(42, 42, 42)
                                        .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                    .addGroup(panelCompraPasajeFLayout.createSequentialGroup()
                        .addGap(202, 202, 202)
                        .addComponent(mensajeServicio, javax.swing.GroupLayout.PREFERRED_SIZE, 339, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelCompraPasajeFLayout.createSequentialGroup()
                        .addGap(266, 266, 266)
                        .addComponent(comprarPasajeAPasajero, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        panelCompraPasajeFLayout.setVerticalGroup(
            panelCompraPasajeFLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelCompraPasajeFLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addGroup(panelCompraPasajeFLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(procesarArchivos7, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(panelCompraPasajeFLayout.createSequentialGroup()
                        .addGap(9, 9, 9)
                        .addComponent(jLabel30)))
                .addGap(6, 6, 6)
                .addGroup(panelCompraPasajeFLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelCompraPasajeFLayout.createSequentialGroup()
                        .addComponent(registrarServicio7, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(crearFuncionario7, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(modificarServicio7, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(cambiarDatosPasajero7, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(consultarServicio7, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelCompraPasajeFLayout.createSequentialGroup()
                        .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane10, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(6, 6, 6)
                .addGroup(panelCompraPasajeFLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(comprarPasaje7, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(panelCompraPasajeFLayout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(jLabel31))
                    .addGroup(panelCompraPasajeFLayout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(cedulaPasajero, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(6, 6, 6)
                .addGroup(panelCompraPasajeFLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelCompraPasajeFLayout.createSequentialGroup()
                        .addGroup(panelCompraPasajeFLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelCompraPasajeFLayout.createSequentialGroup()
                                .addGap(6, 6, 6)
                                .addComponent(jLabel32))
                            .addComponent(cantidadAsientos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(panelCompraPasajeFLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelCompraPasajeFLayout.createSequentialGroup()
                                .addGap(4, 4, 4)
                                .addComponent(jLabel41))
                            .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(6, 6, 6)
                        .addComponent(mensajeCedula, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelCompraPasajeFLayout.createSequentialGroup()
                        .addComponent(informacionPasajero7, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cerrarSesion1, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(12, 12, 12)
                .addComponent(mensajeServicio, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(comprarPasajeAPasajero, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jLabel49.setFont(new java.awt.Font("Lucida Grande", 0, 14)); // NOI18N
        jLabel49.setText("Historial de compra de pasajes:");

        jLabel45.setBackground(javax.swing.UIManager.getDefaults().getColor("Button.light"));
        jLabel45.setFont(new java.awt.Font("Lucida Grande", 0, 11)); // NOI18N
        jLabel45.setText("Cedula|Origen|Destino|Salida|Llegada|Dia|Precio|Transporte|Cantidad");

        jScrollPane8.setViewportView(lstMostrarHistorialP);

        jLabel52.setText("Lista pasajeros en sistema:");

        procesarArchivos8.setText("Procesar Archivos");
        procesarArchivos8.setPreferredSize(new java.awt.Dimension(159, 25));
        procesarArchivos8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                procesarArchivos8ActionPerformed(evt);
            }
        });

        registrarServicio8.setText("Registrar Servicio");
        registrarServicio8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                registrarServicio8ActionPerformed(evt);
            }
        });

        crearFuncionario8.setText("Crear Funcionario");
        crearFuncionario8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                crearFuncionario8ActionPerformed(evt);
            }
        });

        modificarServicio8.setText("Modificar Servicio");
        modificarServicio8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modificarServicio8ActionPerformed(evt);
            }
        });

        cambiarDatosPasajero8.setText("Cambiar Datos Pasajero");
        cambiarDatosPasajero8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cambiarDatosPasajero8ActionPerformed(evt);
            }
        });

        consultarServicio8.setText("Consultar Servicios");
        consultarServicio8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                consultarServicio8ActionPerformed(evt);
            }
        });

        comprarPasaje8.setText("Comprar Pasaje");
        comprarPasaje8.setPreferredSize(new java.awt.Dimension(159, 25));
        comprarPasaje8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comprarPasaje8ActionPerformed(evt);
            }
        });

        cerrarSesion8.setText("Cerrar Sesion");
        cerrarSesion8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cerrarSesion8ActionPerformed(evt);
            }
        });

        jButton3.setText("Informacion Pasajeros");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jScrollPane9.setViewportView(lstMostrarPasajerosP);

        jLabel53.setFont(new java.awt.Font("Lucida Grande", 0, 11)); // NOI18N
        jLabel53.setText("Cedula|Nombre");

        javax.swing.GroupLayout panelConsultarInfoPasajerosLayout = new javax.swing.GroupLayout(panelConsultarInfoPasajeros);
        panelConsultarInfoPasajeros.setLayout(panelConsultarInfoPasajerosLayout);
        panelConsultarInfoPasajerosLayout.setHorizontalGroup(
            panelConsultarInfoPasajerosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelConsultarInfoPasajerosLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(panelConsultarInfoPasajerosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(procesarArchivos8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(registrarServicio8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(crearFuncionario8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(modificarServicio8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cambiarDatosPasajero8, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(consultarServicio8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(comprarPasaje8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cerrarSesion8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGroup(panelConsultarInfoPasajerosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelConsultarInfoPasajerosLayout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addGroup(panelConsultarInfoPasajerosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane8, javax.swing.GroupLayout.PREFERRED_SIZE, 374, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel45, javax.swing.GroupLayout.PREFERRED_SIZE, 380, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel53)
                            .addComponent(jScrollPane9, javax.swing.GroupLayout.PREFERRED_SIZE, 237, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(panelConsultarInfoPasajerosLayout.createSequentialGroup()
                        .addGap(91, 91, 91)
                        .addComponent(jLabel49))
                    .addGroup(panelConsultarInfoPasajerosLayout.createSequentialGroup()
                        .addGap(103, 103, 103)
                        .addComponent(jLabel52)))
                .addGap(51, 51, 51))
        );
        panelConsultarInfoPasajerosLayout.setVerticalGroup(
            panelConsultarInfoPasajerosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelConsultarInfoPasajerosLayout.createSequentialGroup()
                .addGroup(panelConsultarInfoPasajerosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelConsultarInfoPasajerosLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(panelConsultarInfoPasajerosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(procesarArchivos8, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel49))
                        .addGap(6, 6, 6)
                        .addComponent(registrarServicio8, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(crearFuncionario8, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(modificarServicio8, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(cambiarDatosPasajero8, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addGroup(panelConsultarInfoPasajerosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(consultarServicio8, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel53)))
                    .addGroup(panelConsultarInfoPasajerosLayout.createSequentialGroup()
                        .addGap(43, 43, 43)
                        .addComponent(jLabel45)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane8, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel52)))
                .addGap(3, 3, 3)
                .addGroup(panelConsultarInfoPasajerosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelConsultarInfoPasajerosLayout.createSequentialGroup()
                        .addComponent(comprarPasaje8, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cerrarSesion8, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(129, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout panelMenuFuncionarioLayout = new javax.swing.GroupLayout(panelMenuFuncionario);
        panelMenuFuncionario.setLayout(panelMenuFuncionarioLayout);
        panelMenuFuncionarioLayout.setHorizontalGroup(
            panelMenuFuncionarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelRegistrarFuncionario, javax.swing.GroupLayout.DEFAULT_SIZE, 625, Short.MAX_VALUE)
            .addGroup(panelMenuFuncionarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(panelRegistroServicio, javax.swing.GroupLayout.DEFAULT_SIZE, 625, Short.MAX_VALUE))
            .addGroup(panelMenuFuncionarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(panelProcesarArchivos, javax.swing.GroupLayout.DEFAULT_SIZE, 625, Short.MAX_VALUE))
            .addGroup(panelMenuFuncionarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(panelMenuFuncionarioLayout.createSequentialGroup()
                    .addComponent(panelInicial, javax.swing.GroupLayout.PREFERRED_SIZE, 603, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 8, Short.MAX_VALUE)))
            .addGroup(panelMenuFuncionarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(panelMenuFuncionarioLayout.createSequentialGroup()
                    .addComponent(panelModificarServicio, javax.swing.GroupLayout.PREFERRED_SIZE, 609, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
            .addGroup(panelMenuFuncionarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(panelMenuFuncionarioLayout.createSequentialGroup()
                    .addComponent(panelConsultarServicios, javax.swing.GroupLayout.PREFERRED_SIZE, 609, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
            .addGroup(panelMenuFuncionarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(panelMenuFuncionarioLayout.createSequentialGroup()
                    .addComponent(panelModificarUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
            .addGroup(panelMenuFuncionarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(panelCompraPasajeF, javax.swing.GroupLayout.DEFAULT_SIZE, 625, Short.MAX_VALUE))
            .addGroup(panelMenuFuncionarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(panelConsultarInfoPasajeros, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panelMenuFuncionarioLayout.setVerticalGroup(
            panelMenuFuncionarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelMenuFuncionarioLayout.createSequentialGroup()
                .addComponent(panelRegistrarFuncionario, javax.swing.GroupLayout.PREFERRED_SIZE, 595, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 2, Short.MAX_VALUE))
            .addGroup(panelMenuFuncionarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(panelMenuFuncionarioLayout.createSequentialGroup()
                    .addComponent(panelRegistroServicio, javax.swing.GroupLayout.PREFERRED_SIZE, 554, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 43, Short.MAX_VALUE)))
            .addGroup(panelMenuFuncionarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(panelMenuFuncionarioLayout.createSequentialGroup()
                    .addComponent(panelProcesarArchivos, javax.swing.GroupLayout.PREFERRED_SIZE, 597, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
            .addGroup(panelMenuFuncionarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(panelMenuFuncionarioLayout.createSequentialGroup()
                    .addComponent(panelInicial, javax.swing.GroupLayout.PREFERRED_SIZE, 586, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 11, Short.MAX_VALUE)))
            .addGroup(panelMenuFuncionarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(panelMenuFuncionarioLayout.createSequentialGroup()
                    .addComponent(panelModificarServicio, javax.swing.GroupLayout.PREFERRED_SIZE, 555, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 42, Short.MAX_VALUE)))
            .addGroup(panelMenuFuncionarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(panelMenuFuncionarioLayout.createSequentialGroup()
                    .addComponent(panelConsultarServicios, javax.swing.GroupLayout.PREFERRED_SIZE, 555, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 42, Short.MAX_VALUE)))
            .addGroup(panelMenuFuncionarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(panelMenuFuncionarioLayout.createSequentialGroup()
                    .addComponent(panelModificarUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 48, Short.MAX_VALUE)))
            .addGroup(panelMenuFuncionarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(panelMenuFuncionarioLayout.createSequentialGroup()
                    .addComponent(panelCompraPasajeF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 76, Short.MAX_VALUE)))
            .addGroup(panelMenuFuncionarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(panelMenuFuncionarioLayout.createSequentialGroup()
                    .addComponent(panelConsultarInfoPasajeros, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 5, Short.MAX_VALUE)))
        );

        javax.swing.GroupLayout jLayeredPaneLayout = new javax.swing.GroupLayout(jLayeredPane);
        jLayeredPane.setLayout(jLayeredPaneLayout);
        jLayeredPaneLayout.setHorizontalGroup(
            jLayeredPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jLayeredPaneLayout.createSequentialGroup()
                .addComponent(panelIngreso, javax.swing.GroupLayout.PREFERRED_SIZE, 599, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(jLayeredPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jLayeredPaneLayout.createSequentialGroup()
                    .addComponent(panelMenuPasajero, javax.swing.GroupLayout.DEFAULT_SIZE, 603, Short.MAX_VALUE)
                    .addContainerGap()))
            .addGroup(jLayeredPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(panelCreacionUsuario, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(jLayeredPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jLayeredPaneLayout.createSequentialGroup()
                    .addComponent(panelMenuFuncionario, javax.swing.GroupLayout.PREFERRED_SIZE, 603, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        jLayeredPaneLayout.setVerticalGroup(
            jLayeredPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelIngreso, javax.swing.GroupLayout.DEFAULT_SIZE, 645, Short.MAX_VALUE)
            .addGroup(jLayeredPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(panelMenuPasajero, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 645, Short.MAX_VALUE))
            .addGroup(jLayeredPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(panelCreacionUsuario, javax.swing.GroupLayout.DEFAULT_SIZE, 645, Short.MAX_VALUE))
            .addGroup(jLayeredPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jLayeredPaneLayout.createSequentialGroup()
                    .addComponent(panelMenuFuncionario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 65, Short.MAX_VALUE)))
        );
        jLayeredPane.setLayer(panelIngreso, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane.setLayer(panelCreacionUsuario, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane.setLayer(panelMenuPasajero, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane.setLayer(panelMenuFuncionario, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLayeredPane, javax.swing.GroupLayout.PREFERRED_SIZE, 604, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLayeredPane, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 645, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtUsuarioActionPerformed

    }//GEN-LAST:event_txtUsuarioActionPerformed

    private void ingresoSesionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ingresoSesionActionPerformed
        boolean[] pasajYUs = new boolean[2]; //pasajYUs[0] = true (es pasajero), pasajYUs[1] = true (es Usuario) 
        try {
            boolean esValida = wt.validacionCedulaLogin(txtUsuario.getText());
            boolean login = wt.iniciarSesion(txtUsuario.getText(), pasajYUs);
            if (esValida && login) {
                if (pasajYUs[0]) {
                    mostrarMenuPasajero();
                    cedulaUsuario = txtUsuario.getText();
                } else {
                    mostrarMenuFuncionario();
                }
                cedulaUsuario = txtUsuario.getText();
            }
        } catch (MiExcepcion e) {
            lblMensajeLogin.setText(e.getMessage());
            lblMensajeLogin.setForeground(Color.red);
        }
    }//GEN-LAST:event_ingresoSesionActionPerformed

    public void mostrarMenuFuncionario() {
        jLayeredPane.moveToFront(panelMenuFuncionario);
        jLayeredPane.moveToBack(panelMenuPasajero);
        jLayeredPane.moveToBack(panelIngreso);
        jLayeredPane.moveToBack(panelCreacionUsuario);
        panelProcesarArchivos.setVisible(false);
        panelRegistrarFuncionario.setVisible(false);
        panelRegistroServicio.setVisible(false);
        panelModificarServicio.setVisible(false);
        panelConsultarServicios.setVisible(false);
        panelCompraPasajeF.setVisible(false);
        panelModificarUsuario.setVisible(false);
        panelConsultarInfoPasajeros.setVisible(false);
        panelInicial.setVisible(true);
    }

    public void mostrarMenuPasajero() {
        jLayeredPane.moveToFront(panelMenuPasajero);
        jLayeredPane.moveToBack(panelMenuFuncionario);
        jLayeredPane.moveToBack(panelIngreso);
        jLayeredPane.moveToBack(panelCreacionUsuario);
        panelCompraViajeP.setVisible(false);
        panelModificarP.setVisible(false);
        panelConsultarServiciosP.setVisible(false);
        panelInicialP.setVisible(true);
    }

    private void crearCuentaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_crearCuentaActionPerformed
        try {
            boolean esCedulaValida = wt.validacionCedulaCreacion(txtCrear.getText());
            if (esCedulaValida) {
                cedulaUsuario = txtCrear.getText();
                if (txtNomCrear.getText().isEmpty()) {
                    lblMensajeCreacion.setText("El nombre no puede ser vacio");
                    lblMensajeCreacion.setForeground(Color.red);
                } else {
                    wt.crearCuenta(cedulaUsuario, txtNomCrear.getText());
                     mostrarMenuPasajero();
                }
            }
        } catch (MiExcepcion e) {
            lblMensajeCreacion.setText(e.getMessage());
            lblMensajeCreacion.setForeground(Color.red);
        }
    }//GEN-LAST:event_crearCuentaActionPerformed

    private void crearNuevaCuentaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_crearNuevaCuentaActionPerformed
        jLayeredPane.moveToFront(panelCreacionUsuario);
        txtCrear.setText("");
        txtNomCrear.setText("");
        lblMensajeCreacion.setText("");
    }//GEN-LAST:event_crearNuevaCuentaActionPerformed

    public void procesarArchivos() {
        crearNuevaCuenta.setEnabled(false);
        panelProcesarArchivos.setVisible(true);
        panelConsultarInfoPasajeros.setVisible(false);
        panelRegistroServicio.setVisible(false);
        panelInicial.setVisible(false);
        panelCompraPasajeF.setVisible(false);
        panelRegistrarFuncionario.setVisible(false);
        panelConsultarServicios.setVisible(false);
        panelModificarServicio.setVisible(false);
        panelModificarUsuario.setVisible(false);
        int[] contadorPyS = new int[2]; //posicion 0 es el contador de pasajeros, y posicion 1 contador de servicios
        ArrayList<String> idsEmpresas = new ArrayList<>();

        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setMultiSelectionEnabled(true);
        fileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
        int result = fileChooser.showOpenDialog(this);
        if (result == JFileChooser.APPROVE_OPTION) {
            File[] selectedFiles = fileChooser.getSelectedFiles();
            for (File f : selectedFiles) {
                wt.procesamientoDeArchivos(f.getAbsolutePath(), f.getName(), contadorPyS, idsEmpresas);
            }
        }
        txtInfoArchivos.setForeground(Color.green);
        txtInfoArchivos.setText("Cantidad de pasajeros procesados: " + contadorPyS[0]
                + "\nCantidad de servicios procesados: " + contadorPyS[1]
                + "\nCantidad de empresas procesadas: " + idsEmpresas.size());
    }

    private void txtNombreFuncActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNombreFuncActionPerformed

    }//GEN-LAST:event_txtNombreFuncActionPerformed

    private void cancelarCreacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelarCreacionActionPerformed
        jLayeredPane.moveToFront(panelIngreso);
        txtUsuario.setText("");
        lblMensajeLogin.setText("");
    }//GEN-LAST:event_cancelarCreacionActionPerformed

    private void registrarNuevoFuncActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_registrarNuevoFuncActionPerformed
        try {
            boolean esValida = wt.validacionCedulaCreacion(txtCedulaFunc.getText());
            if (esValida) {
                wt.crearFuncionario(txtNombreFunc.getText(), txtCedulaFunc.getText());
                lblMensajeCreacionFunc.setText("Funcionario agregado correctamente al sistema");
                lblMensajeCreacionFunc.setForeground(Color.green);
            }
        } catch (MiExcepcion e) {
            lblMensajeCreacionFunc.setText(e.getMessage());
            lblMensajeCreacionFunc.setForeground(Color.red);
        }
    }//GEN-LAST:event_registrarNuevoFuncActionPerformed

    private void rLunesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rLunesActionPerformed

    }//GEN-LAST:event_rLunesActionPerformed

    private void txtDestinoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDestinoActionPerformed

    }//GEN-LAST:event_txtDestinoActionPerformed

    private void txtHoraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtHoraActionPerformed

    }//GEN-LAST:event_txtHoraActionPerformed

    private void tiposTransporteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tiposTransporteActionPerformed

    }//GEN-LAST:event_tiposTransporteActionPerformed

    private void registroServicioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_registroServicioActionPerformed
        String identificador = txtIdentificador.getText();
        String origen = txtOrigen.getText();
        String destino = txtDestino.getText();
        String salidaI = txtHora.getText();
        String llegadaI = txtDuracion.getText();
        String asientosStr = txtAsientos.getText();
        String precioStr = txtPrecio.getText();
        String tipo = tiposTransporte.getSelectedItem().toString();
        Transporte transporteI = new Transporte(tipo);
        if (identificador.isEmpty() || origen.isEmpty() || destino.isEmpty() || salidaI.isEmpty()
                || llegadaI.isEmpty() || asientosStr.isEmpty() || precioStr.isEmpty()) {
            lblMensajeServicio.setText("Uno de los campos es vacio");
            lblMensajeServicio.setForeground(Color.red);
        } else {//Campos no vacios
            if (wt.validarId(txtIdentificador.getText())) { //identificador forma correcta
                if (wt.idUnico(tipo)) {//identificador unico
                    if (wt.esStringNumerico(asientosStr) && wt.esStringNumerico(precioStr)) {//asientos y precios numericos
                        if (wt.validarHora(salidaI) && wt.validarHora(llegadaI)) {//horas validas
                            String[] diasDisponibles = wt.setearDiasDisponibles(rLunes.isSelected(), rMartes.isSelected(), rMiercoles.isSelected(), rJueves.isSelected(), rViernes.isSelected(), rSabado.isSelected(), rDomingo.isSelected());
                            String frecuencia = wt.calcularFrecuencia(diasDisponibles);
                            int precio = Integer.parseInt(precioStr);
                            int asientos = Integer.parseInt(asientosStr);
                            if (!frecuencia.isEmpty()) { //Si se seleccionario algun dia disponible
                                ServicioDeTransporte servicioTransporte = new ServicioDeTransporte(identificador, origen, destino, salidaI, llegadaI, frecuencia, precio, transporteI, asientos);
                                wt.agregarServicioDeTransporte(servicioTransporte);
                                lblMensajeServicio.setText("Servicio ingresado correctamente");
                                lblMensajeServicio.setForeground(Color.green);
                            } else {
                                lblMensajeServicio.setText("Debe seleccionar los dias disponibles");
                                lblMensajeServicio.setForeground(Color.red);
                            }
                        } else {
                            lblMensajeServicio.setText("La hora debe tener la forma (00-23):(00--59)");
                            lblMensajeServicio.setForeground(Color.red);
                        }

                    } else {
                        lblMensajeServicio.setText("El precio y los asientos deben ser numericos");
                        lblMensajeServicio.setForeground(Color.red);
                    }
                } else {
                    lblMensajeServicio.setText("Ya existe el identificador, ingrese otro");
                    lblMensajeServicio.setForeground(Color.red);
                }
            } else {
                lblMensajeServicio.setText("Identificador debe contener 3 letras mayusculas seguidas de 4 numeros");
                lblMensajeServicio.setForeground(Color.red);
            }
        }
    }//GEN-LAST:event_registroServicioActionPerformed

    private void procesarArchivos1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_procesarArchivos1ActionPerformed
        procesarArchivos();
    }//GEN-LAST:event_procesarArchivos1ActionPerformed

    public void crearFunc() {
        crearNuevaCuenta.setEnabled(false);
        panelProcesarArchivos.setVisible(false);
        panelConsultarInfoPasajeros.setVisible(false);
        panelRegistroServicio.setVisible(false);
        panelInicial.setVisible(false);
        panelCompraPasajeF.setVisible(false);
        panelModificarServicio.setVisible(false);
        panelConsultarServicios.setVisible(false);
        panelModificarUsuario.setVisible(false);
        panelRegistrarFuncionario.setVisible(true);
        txtNombreFunc.setText("");
        txtCedulaFunc.setText("");
        lblMensajeCreacionFunc.setText("");
    }

    private void crearFuncionario1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_crearFuncionario1ActionPerformed
        crearFunc();
    }//GEN-LAST:event_crearFuncionario1ActionPerformed

    public void registrarServ() {
        crearNuevaCuenta.setEnabled(false);
        panelProcesarArchivos.setVisible(false);
        panelConsultarInfoPasajeros.setVisible(false);
        panelRegistrarFuncionario.setVisible(false);
        panelModificarServicio.setVisible(false);
        panelInicial.setVisible(false);
        panelCompraPasajeF.setVisible(false);
        panelConsultarServicios.setVisible(false);
        panelModificarUsuario.setVisible(false);
        panelRegistroServicio.setVisible(true);
        txtIdentificador.setText("");
        txtOrigen.setText("");
        txtDestino.setText("");
        txtHora.setText("");
        txtDuracion.setText("");
        txtPrecio.setText("");
        txtAsientos.setText("");
        rLunes.setSelected(false);
        rMartes.setSelected(false);
        rMiercoles.setSelected(false);
        rJueves.setSelected(false);
        rViernes.setSelected(false);
        rSabado.setSelected(false);
        rDomingo.setSelected(false);
        lblMensajeServicio.setText("");
    }

    public void modificarServ() {
        crearNuevaCuenta.setEnabled(false);
        panelConsultarInfoPasajeros.setVisible(false);
        panelProcesarArchivos.setVisible(false);
        panelRegistrarFuncionario.setVisible(false);
        panelRegistroServicio.setVisible(false);
        panelInicial.setVisible(false);
        panelCompraPasajeF.setVisible(false);
        panelConsultarServicios.setVisible(false);
        panelModificarUsuario.setVisible(false);
        jListaServicios.setListData(wt.getListaServiciosDeTransporte().toArray());
        panelModificarServicio.setVisible(true);
        resultadoModificarServicio.setText("");
        nuevaHoraInicio.setText("");
        nuevaHoraFin.setText("");
    }

    public void consultarServ() {
        crearNuevaCuenta.setEnabled(false);
        panelProcesarArchivos.setVisible(false);
        panelConsultarInfoPasajeros.setVisible(false);
        panelRegistrarFuncionario.setVisible(false);
        panelRegistroServicio.setVisible(false);
        panelModificarServicio.setVisible(false);
        panelInicial.setVisible(false);
        panelCompraPasajeF.setVisible(false);
        panelModificarUsuario.setVisible(false);
        panelConsultarServicios.setVisible(true);
        ciudadOrigen.setText("");
        ciudadDestino.setText("");
        resultadoCargarServicios.setText("");
        jListaServiciosEncontrados.removeAll();

    }

    public void cambiarDatosPas() {
        crearNuevaCuenta.setEnabled(false);
        panelProcesarArchivos.setVisible(false);
        panelConsultarInfoPasajeros.setVisible(false);
        panelRegistrarFuncionario.setVisible(false);
        panelRegistroServicio.setVisible(false);
        panelModificarServicio.setVisible(false);
        panelInicial.setVisible(false);
        panelConsultarServicios.setVisible(false);
        panelCompraPasajeF.setVisible(false);
        panelModificarUsuario.setVisible(true);
        cedulaActual.setText("");
        cedulaNueva.setText("");
        nombreNuevo.setText("");
        confirmarBaja.setSelected(false);
        confirmarCedula.setSelected(false);
        confirmarNombre.setSelected(false);
        resultadoUsuario.setText("");
        errorPasajeroNoEsta.setText("");
    }

    public void comprarPas() {
        crearNuevaCuenta.setEnabled(false);
        panelProcesarArchivos.setVisible(false);
        panelConsultarInfoPasajeros.setVisible(false);
        panelRegistrarFuncionario.setVisible(false);
        panelRegistroServicio.setVisible(false);
        panelModificarServicio.setVisible(false);
        panelModificarUsuario.setVisible(false);
        panelInicial.setVisible(false);
        panelConsultarServicios.setVisible(false);
        wt.obtenerServiciosAJList(jListaServiciosCompra);
        panelCompraPasajeF.setVisible(true);
        cedulaPasajero.setText("");
        cantidadAsientos.setText("");
        mensajeCedula.setText("");
        mensajeServicio.setText("");
    }

    public void cerrarSesionF() {
        crearNuevaCuenta.setEnabled(true);
        txtUsuario.setText("");
        lblMensajeLogin.setText("");
        panelProcesarArchivos.setVisible(false);
        panelConsultarInfoPasajeros.setVisible(false);
        panelRegistrarFuncionario.setVisible(false);
        panelRegistroServicio.setVisible(false);
        panelModificarServicio.setVisible(false);
        panelModificarUsuario.setVisible(false);
        panelInicial.setVisible(false);
        panelConsultarServicios.setVisible(false);
        panelCompraPasajeF.setVisible(false);
        jLayeredPane.moveToFront(panelIngreso);
        panelIngreso.setVisible(true);
    }

    private void registrarServicio1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_registrarServicio1ActionPerformed
        registrarServ();
    }//GEN-LAST:event_registrarServicio1ActionPerformed

    private void procesarArchivos2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_procesarArchivos2ActionPerformed
        procesarArchivos();
    }//GEN-LAST:event_procesarArchivos2ActionPerformed

    private void crearFuncionario2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_crearFuncionario2ActionPerformed
        crearFunc();
    }//GEN-LAST:event_crearFuncionario2ActionPerformed

    private void registrarServicio2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_registrarServicio2ActionPerformed
        registrarServ();
    }//GEN-LAST:event_registrarServicio2ActionPerformed

    private void procesarArchivos3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_procesarArchivos3ActionPerformed
        procesarArchivos();
    }//GEN-LAST:event_procesarArchivos3ActionPerformed

    private void crearFuncionario3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_crearFuncionario3ActionPerformed
        crearFunc();
    }//GEN-LAST:event_crearFuncionario3ActionPerformed

    private void registrarServicio3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_registrarServicio3ActionPerformed
        registrarServ();
    }//GEN-LAST:event_registrarServicio3ActionPerformed

    private void registrarServicioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_registrarServicioActionPerformed
        registrarServ();
    }//GEN-LAST:event_registrarServicioActionPerformed

    private void crearFuncionarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_crearFuncionarioActionPerformed
        crearFunc();
    }//GEN-LAST:event_crearFuncionarioActionPerformed

    private void procesarArchivosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_procesarArchivosActionPerformed
        procesarArchivos();
    }//GEN-LAST:event_procesarArchivosActionPerformed

    private void modificarServicio1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_modificarServicio1ActionPerformed
        modificarServ();
    }//GEN-LAST:event_modificarServicio1ActionPerformed

    private void modificarServicioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_modificarServicioActionPerformed
        modificarServ();
    }//GEN-LAST:event_modificarServicioActionPerformed

    private void modificarServicio2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_modificarServicio2ActionPerformed
        modificarServ();
    }//GEN-LAST:event_modificarServicio2ActionPerformed

    private void procesarArchivos4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_procesarArchivos4ActionPerformed
        procesarArchivos();
    }//GEN-LAST:event_procesarArchivos4ActionPerformed

    private void registrarServicio4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_registrarServicio4ActionPerformed
        registrarServ();
    }//GEN-LAST:event_registrarServicio4ActionPerformed

    private void crearFuncionario4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_crearFuncionario4ActionPerformed
        crearFunc();
    }//GEN-LAST:event_crearFuncionario4ActionPerformed

    private void modificarServicio3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_modificarServicio3ActionPerformed
        modificarServ();
    }//GEN-LAST:event_modificarServicio3ActionPerformed

    private void nuevaHoraFinActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nuevaHoraFinActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_nuevaHoraFinActionPerformed

    private void modificarServicio4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_modificarServicio4ActionPerformed
        modificarServ();
    }//GEN-LAST:event_modificarServicio4ActionPerformed

    private void guardarTransporteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_guardarTransporteActionPerformed
        if (!wt.getListaServiciosDeTransporte().isEmpty()) {
            if (jListaServicios.getSelectedValue() != null) {
                ServicioDeTransporte s = (ServicioDeTransporte) jListaServicios.getSelectedValue();
                if (wt.validarHora(nuevaHoraFin.getText()) && wt.validarHora(nuevaHoraInicio.getText())) {
                    wt.modificarServicioDeTransporte(s, nuevaHoraInicio.getText(), nuevaHoraFin.getText());
                    resultadoModificarServicio.setText("Datos modificados correctamente");
                    resultadoModificarServicio.setForeground(Color.green);
                } else {
                    resultadoModificarServicio.setText("Las horas deben ser (hh:mm) " + "\ningrese datos nuevamente");
                    resultadoModificarServicio.setForeground(Color.red);
                }
            } else {
                resultadoModificarServicio.setText("Seleccione un servicio");
                resultadoModificarServicio.setForeground(Color.blue);
            }
        } else {
            resultadoModificarServicio.setText("No hay servicios en el sistema");
            resultadoModificarServicio.setForeground(Color.red);
        }
    }//GEN-LAST:event_guardarTransporteActionPerformed

    private void procesarArchivos5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_procesarArchivos5ActionPerformed
        procesarArchivos();
    }//GEN-LAST:event_procesarArchivos5ActionPerformed

    private void registrarServicio5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_registrarServicio5ActionPerformed
        registrarServ();
    }//GEN-LAST:event_registrarServicio5ActionPerformed

    private void crearFuncionario5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_crearFuncionario5ActionPerformed
        crearFunc();
    }//GEN-LAST:event_crearFuncionario5ActionPerformed

    private void modificarServicio5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_modificarServicio5ActionPerformed
        modificarServ();
    }//GEN-LAST:event_modificarServicio5ActionPerformed

    private void cambiarDatosPasajero5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cambiarDatosPasajero5ActionPerformed
        cambiarDatosPas();
    }//GEN-LAST:event_cambiarDatosPasajero5ActionPerformed

    private void ciudadOrigenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ciudadOrigenActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ciudadOrigenActionPerformed

    private void cargarServiciosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cargarServiciosActionPerformed
        if (!ciudadOrigen.getText().isEmpty() && !ciudadDestino.getText().isEmpty()) {
            String cOrigen = ciudadOrigen.getText();
            String cDestino = ciudadDestino.getText();
            ArrayList<ServicioDeTransporte> servicios = wt.seleccionarServicios(cOrigen, cDestino);
            if (servicios.isEmpty()) {
                resultadoCargarServicios.setText("No se encontraron resultados");
                resultadoCargarServicios.setForeground(Color.blue);
            } else {
                jListaServiciosEncontrados.setListData(servicios.toArray());
                resultadoCargarServicios.setText("Carga correcta");
                resultadoCargarServicios.setForeground(Color.GREEN);
            }
        } else {
            resultadoCargarServicios.setText("Ingrese datos correctos");
            resultadoCargarServicios.setForeground(Color.red);
        }
    }//GEN-LAST:event_cargarServiciosActionPerformed

    private void consultarServicio3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_consultarServicio3ActionPerformed
        consultarServ();
    }//GEN-LAST:event_consultarServicio3ActionPerformed

    private void consultarServicioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_consultarServicioActionPerformed
        consultarServ();
    }//GEN-LAST:event_consultarServicioActionPerformed

    private void consultarServicio1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_consultarServicio1ActionPerformed
        consultarServ();
    }//GEN-LAST:event_consultarServicio1ActionPerformed

    private void consultarServicio2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_consultarServicio2ActionPerformed
        consultarServ();
    }//GEN-LAST:event_consultarServicio2ActionPerformed

    private void consultarServicio4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_consultarServicio4ActionPerformed
        consultarServ();
    }//GEN-LAST:event_consultarServicio4ActionPerformed

    private void consultarServicio5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_consultarServicio5ActionPerformed
        consultarServ();
    }//GEN-LAST:event_consultarServicio5ActionPerformed

    private void procesarArchivos6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_procesarArchivos6ActionPerformed
        procesarArchivos();
    }//GEN-LAST:event_procesarArchivos6ActionPerformed

    private void registrarServicio6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_registrarServicio6ActionPerformed
        registrarServ();
    }//GEN-LAST:event_registrarServicio6ActionPerformed

    private void crearFuncionario6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_crearFuncionario6ActionPerformed
        crearFunc();
    }//GEN-LAST:event_crearFuncionario6ActionPerformed

    private void modificarServicio6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_modificarServicio6ActionPerformed
        modificarServ();
    }//GEN-LAST:event_modificarServicio6ActionPerformed

    private void cambiarDatosPasajero6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cambiarDatosPasajero6ActionPerformed
        cambiarDatosPas();
    }//GEN-LAST:event_cambiarDatosPasajero6ActionPerformed

    private void consultarServicio6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_consultarServicio6ActionPerformed
        consultarServ();
    }//GEN-LAST:event_consultarServicio6ActionPerformed

    private void cambiarDatosPasajero1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cambiarDatosPasajero1ActionPerformed
        cambiarDatosPas();
    }//GEN-LAST:event_cambiarDatosPasajero1ActionPerformed

    private void cambiarDatosPasajero2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cambiarDatosPasajero2ActionPerformed
        cambiarDatosPas();
    }//GEN-LAST:event_cambiarDatosPasajero2ActionPerformed

    private void cambiarDatosPasajero4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cambiarDatosPasajero4ActionPerformed
        cambiarDatosPas();
    }//GEN-LAST:event_cambiarDatosPasajero4ActionPerformed

    private void cambiarDatosPasajero3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cambiarDatosPasajero3ActionPerformed
        cambiarDatosPas();
    }//GEN-LAST:event_cambiarDatosPasajero3ActionPerformed

    private void cambiarDatosPasajeroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cambiarDatosPasajeroActionPerformed
        cambiarDatosPas();
    }//GEN-LAST:event_cambiarDatosPasajeroActionPerformed

    private void guardarPasajeroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_guardarPasajeroActionPerformed
        try {
            resultadoUsuario.setText("");
            pasajeroBorrado.setText("");
            errorPasajeroNoEsta.setText("");
            boolean esValida = wt.validacionCedulaLogin(cedulaActual.getText());

            if (esValida && wt.esUsPas(cedulaActual.getText())) {
                //La cedula ingresada es valida y le pertence a un pasajero
                String nuevaCedula = cedulaNueva.getText();
                String nuevoNombre = nombreNuevo.getText();
                Pasajero pModif = wt.esUsuarioPasajero(cedulaActual.getText());
                if (!confirmarBaja.isSelected()) {//Caso que no se lo quiere dar de baja
                    if (confirmarCedula.isSelected()) { //Selecciona modificar cedula 
                        if (wt.validacionCedulaCreacion(nuevaCedula)) { //La cedula a modificar es valida
                            if (confirmarNombre.isSelected()) { //Selecciona modificar Nombre
                                pModif.setNombre(nuevoNombre);
                                pModif.setCedula(nuevaCedula);
                                resultadoUsuario.setText("Datos modificados correctamente");
                            } else { //Modifica unicamente la cedula
                                pModif.setCedula(nuevaCedula);
                                resultadoUsuario.setText("Cedula modificada correctamente");
                            }
                            resultadoUsuario.setForeground(Color.green);
                        } else { //La cedula a modificar es invalida
                            resultadoUsuario.setText("No se pueden modificar los datos, cedula incorrecta");
                            resultadoUsuario.setForeground(Color.red);
                        }

                    } else if (confirmarNombre.isSelected()) { //Selecciona unicamente modificar nombre
                        pModif.setNombre(nuevoNombre);
                        resultadoUsuario.setText("Nombre modificado correctamente");
                        resultadoUsuario.setForeground(Color.green);
                    } else {//No se selecciona ninguna confirmacion
                        resultadoUsuario.setText("Confirme antes de modificar");
                        resultadoUsuario.setForeground(Color.red);
                    }
                } else { //Selecciona la confirmacion de baja de usuario
                    wt.borrarPasajero(pModif);
                    pasajeroBorrado.setText("Pasajero borrado correctamente");
                    pasajeroBorrado.setForeground(Color.blue);
                }
            } else {//La cedula es de un funcionario
                errorPasajeroNoEsta.setText("No existe el pasajero ingresado");
                errorPasajeroNoEsta.setForeground(Color.red);
            }
        } catch (MiExcepcion e) {
            errorPasajeroNoEsta.setText(e.getMessage());
            errorPasajeroNoEsta.setForeground(Color.red);
        }
    }//GEN-LAST:event_guardarPasajeroActionPerformed

    private void comprarPasaje2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comprarPasaje2ActionPerformed
        comprarPas();
    }//GEN-LAST:event_comprarPasaje2ActionPerformed

    private void horaSalidaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_horaSalidaActionPerformed
        wt.ordenarPorX(jListaServiciosEncontrados, "porHoraSalida");
    }//GEN-LAST:event_horaSalidaActionPerformed


    private void ordenarPrecioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ordenarPrecioActionPerformed
        wt.ordenarPorX(jListaServiciosEncontrados, "porPrecio");
    }//GEN-LAST:event_ordenarPrecioActionPerformed

    private void procesarArchivos7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_procesarArchivos7ActionPerformed
        procesarArchivos();
    }//GEN-LAST:event_procesarArchivos7ActionPerformed

    private void registrarServicio7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_registrarServicio7ActionPerformed
        registrarServ();
    }//GEN-LAST:event_registrarServicio7ActionPerformed

    private void crearFuncionario7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_crearFuncionario7ActionPerformed
        crearFunc();
    }//GEN-LAST:event_crearFuncionario7ActionPerformed

    private void modificarServicio7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_modificarServicio7ActionPerformed
        modificarServ();
    }//GEN-LAST:event_modificarServicio7ActionPerformed

    private void cambiarDatosPasajero7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cambiarDatosPasajero7ActionPerformed
        cambiarDatosPas();
    }//GEN-LAST:event_cambiarDatosPasajero7ActionPerformed

    private void consultarServicio7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_consultarServicio7ActionPerformed
        consultarServ();
    }//GEN-LAST:event_consultarServicio7ActionPerformed

    private void comprarPasajeAPasajeroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comprarPasajeAPasajeroActionPerformed
        if (wt.getListaServiciosDeTransporte().isEmpty()) {
            mensajeServicio.setText("No hay servicios para comprar");
            mensajeServicio.setForeground(Color.red);
        } else {
            try {
                boolean esValida = wt.validacionCedulaLogin(cedulaPasajero.getText());
                if (esValida && wt.esUsPas(cedulaPasajero.getText())) {
                    if (wt.getListaServiciosDeTransporte().isEmpty()) {//No tengo servicios para mostrar
                        mensajeServicio.setText("No hay servicios disponibles");
                        mensajeServicio.setForeground(Color.red);
                    } else {//Hay servicios
                        if (jListaServiciosCompra.getSelectedValue() != null) { //Selecciono uno
                            Pasajero pSeleccionado = wt.esUsuarioPasajero(cedulaPasajero.getText());
                            ServicioDeTransporte s = (ServicioDeTransporte) jListaServiciosCompra.getSelectedValue();
                            if (wt.esStringNumerico(cantidadAsientos.getText()) && Integer.parseInt(cantidadAsientos.getText()) != 0) {
                                int asientosAComprar = Integer.parseInt(cantidadAsientos.getText());
                                if ((s.getAsientosDisponibles() - asientosAComprar >= 0)) { //Hay asientos disponibles
                                    wt.registrarCompraPasaje(s, pSeleccionado, asientosAComprar, (String) jComboBox1.getSelectedItem(), "Por Funcionario");
                                    mensajeServicio.setText("Pasaje comprado correctamente");
                                    mensajeServicio.setForeground(Color.green);
                                    jListaServiciosCompra.setListData(wt.getListaServiciosDeTransporte().toArray());
                                } else {//No quedan asientos para vender
                                    mensajeServicio.setText("No hay suficientes asientos disponibles");
                                    mensajeServicio.setForeground(Color.red);
                                }
                            } else { //El campo de cantidad de asientos no es numerico
                                mensajeServicio.setText("La cantidad de asientos es invalida");
                                mensajeServicio.setForeground(Color.red);
                            }
                        } else { //No selecciona ningun servicio
                            mensajeServicio.setText("Seleccione un servicio");
                            mensajeServicio.setForeground(Color.blue);
                        }
                    }
                } else {//Solo puede pasar que no sea pasajero, esValida retorna true o sino tira MiExcepcion
                    mensajeCedula.setText("No existe el pasajero seleccionado");
                    mensajeCedula.setForeground(Color.red);
                }
            } catch (MiExcepcion e) {
                mensajeCedula.setText(e.getMessage());
                mensajeCedula.setForeground(Color.red);
            }
        }
    }//GEN-LAST:event_comprarPasajeAPasajeroActionPerformed

    private void comprarPasaje7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comprarPasaje7ActionPerformed
        comprarPas();
    }//GEN-LAST:event_comprarPasaje7ActionPerformed

    private void comprarPasajeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comprarPasajeActionPerformed
        comprarPas();
    }//GEN-LAST:event_comprarPasajeActionPerformed

    private void comprarPasaje1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comprarPasaje1ActionPerformed
        comprarPas();
    }//GEN-LAST:event_comprarPasaje1ActionPerformed

    private void comprarPasaje4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comprarPasaje4ActionPerformed
        comprarPas();
    }//GEN-LAST:event_comprarPasaje4ActionPerformed

    private void comprarPasaje3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comprarPasaje3ActionPerformed
        comprarPas();
    }//GEN-LAST:event_comprarPasaje3ActionPerformed

    private void comprarPasaje5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comprarPasaje5ActionPerformed
        comprarPas();
    }//GEN-LAST:event_comprarPasaje5ActionPerformed

    private void comprarPasaje6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comprarPasaje6ActionPerformed
        comprarPas();
    }//GEN-LAST:event_comprarPasaje6ActionPerformed

    private void cedulaPasajeroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cedulaPasajeroActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cedulaPasajeroActionPerformed

    private void jTextField1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField1ActionPerformed


    private void transporteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_transporteActionPerformed
        wt.ordenarPorX(jListaServiciosEncontrados, "porTransporte");
    }//GEN-LAST:event_transporteActionPerformed

    private void jTextField3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField3ActionPerformed

    private void cantidadAsientosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cantidadAsientosActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cantidadAsientosActionPerformed

    public void mostrarInfoPersonal() {
        crearNuevaCuenta.setEnabled(false);
        panelModificarP.setVisible(true);
        panelConsultarServiciosP.setVisible(false);
        panelInicialP.setVisible(false);
        panelCompraViajeP.setVisible(false);
        wt.mostrarEnJListHistorialP(mostrarHistorialP, wt.esUsuarioPasajero(cedulaUsuario));
        nuevaCedula.setText("");
        nuevoNombre.setText("");
        nuevat1.setText("");
        nuevat2.setText("");
        nuevat3.setText("");
        nuevat4.setText("");
        confirmarNuevaCedula.setSelected(false);
        confirmarNuevoNombre.setSelected(false);
        confirmarNuevaTarjeta.setSelected(false);
        errorCedulaNueva.setText("");
        errorNombreNuevo.setText("");
        errorTarjetaNueva.setText("");
    }

    public void mostrarConsultarServiciosP() {
        crearNuevaCuenta.setEnabled(false);
        panelModificarP.setVisible(false);
        panelConsultarServiciosP.setVisible(true);
        panelInicialP.setVisible(false);
        panelCompraViajeP.setVisible(false);
        origenServicioP.setText("");
        destinoServicioP.setText("");
        lblMensajeServiciosP.setText("");
        lblMensajeCompraP.setText("");
        listaServiciosP.removeAll();
    }

    public void mostrarCompraPasajesP() {
        panelModificarP.setVisible(false);
        panelConsultarServiciosP.setVisible(false);
        panelInicialP.setVisible(false);
        panelCompraViajeP.setVisible(true);
        cantidadAsientosP.setText("");
        numeroTarj.setText("");
        numeroTarj1.setText("");
        numeroTarj2.setText("");
        numeroTarj3.setText("");
    }

    public void cerrarSesionP() {
        crearNuevaCuenta.setEnabled(true);
        panelIngreso.setVisible(true);
        jLayeredPane.moveToFront(panelIngreso);
        panelModificarP.setVisible(false);
        panelConsultarServiciosP.setVisible(false);
        panelInicialP.setVisible(false);
        panelCompraViajeP.setVisible(false);
        txtUsuario.setText("");
        lblMensajeLogin.setText("");
    }

    private void informacionPersonal1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_informacionPersonal1ActionPerformed
        mostrarInfoPersonal();
    }//GEN-LAST:event_informacionPersonal1ActionPerformed

    private void informacionPersonal5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_informacionPersonal5ActionPerformed
        mostrarInfoPersonal();
    }//GEN-LAST:event_informacionPersonal5ActionPerformed

    private void informacionPersonal6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_informacionPersonal6ActionPerformed
        mostrarInfoPersonal();
    }//GEN-LAST:event_informacionPersonal6ActionPerformed

    private void consultarServiciosP5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_consultarServiciosP5ActionPerformed
        mostrarConsultarServiciosP();
    }//GEN-LAST:event_consultarServiciosP5ActionPerformed

    private void consultarServiciosP1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_consultarServiciosP1ActionPerformed
        mostrarConsultarServiciosP();
    }//GEN-LAST:event_consultarServiciosP1ActionPerformed

    private void consultarServiciosP6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_consultarServiciosP6ActionPerformed
        mostrarConsultarServiciosP();
    }//GEN-LAST:event_consultarServiciosP6ActionPerformed

    private void comprarPasajesP6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comprarPasajesP6ActionPerformed
        if (listaServiciosP.getSelectedValue() != null) {
            mostrarCompraPasajesP();
            ServicioDeTransporte sSelected = (ServicioDeTransporte) listaServiciosP.getSelectedValue();
            ArrayList<ServicioDeTransporte> listaUnica = new ArrayList<>();
            listaUnica.add(sSelected);
            servicioSeleccionadoP.setListData(listaUnica.toArray());
            servicioSeleccionadoP.setSelectedIndex(0);
            servicioSeleccionadoP.setCellRenderer(new DefaultListCellRenderer());
            wt.setearFrecuenciaEnBox(comboBoxDias, sSelected.getFrecuencia());

        } else {
            lblMensajeCompraP.setText("Debe seleccionar un servicio");
            lblMensajeCompraP.setForeground(Color.red);
        }

    }//GEN-LAST:event_comprarPasajesP6ActionPerformed

    private void origenServicioPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_origenServicioPActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_origenServicioPActionPerformed

    private void buscarServiciosPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buscarServiciosPActionPerformed
        if (!origenServicioP.getText().isEmpty() && !destinoServicioP.getText().isEmpty()) {
            String cOrigen = origenServicioP.getText();
            String cDestino = destinoServicioP.getText();
            ArrayList<ServicioDeTransporte> servicios = wt.seleccionarServicios(cOrigen, cDestino);
            if (servicios.isEmpty()) {
                lblMensajeServiciosP.setText("No se encontraron resultados");
                lblMensajeServiciosP.setForeground(Color.blue);
            } else {
                listaServiciosP.setListData(servicios.toArray());
                lblMensajeServiciosP.setText("Carga correcta");
                lblMensajeServiciosP.setForeground(Color.GREEN);
            }
        } else {
            lblMensajeServiciosP.setText("Ingrese datos correctos");
            lblMensajeServiciosP.setForeground(Color.red);
        }
    }//GEN-LAST:event_buscarServiciosPActionPerformed

    private void jTextField6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField6ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField6ActionPerformed

    private void horaLlegadaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_horaLlegadaActionPerformed
        wt.ordenarPorX(jListaServiciosEncontrados, "porHoraLlegada");
    }//GEN-LAST:event_horaLlegadaActionPerformed

    private void ordPorPrecioPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ordPorPrecioPActionPerformed
        wt.ordenarPorX(listaServiciosP, "porPrecio");
    }//GEN-LAST:event_ordPorPrecioPActionPerformed

    private void ordPorHoraSalidaPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ordPorHoraSalidaPActionPerformed
        wt.ordenarPorX(listaServiciosP, "porHoraSalida");
    }//GEN-LAST:event_ordPorHoraSalidaPActionPerformed

    private void ordPorHoraLlegadaPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ordPorHoraLlegadaPActionPerformed
        wt.ordenarPorX(listaServiciosP, "porHoraLlegada");
    }//GEN-LAST:event_ordPorHoraLlegadaPActionPerformed

    private void ordPorTransporteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ordPorTransporteActionPerformed
        wt.ordenarPorX(listaServiciosP, "porTransporte");
    }//GEN-LAST:event_ordPorTransporteActionPerformed

    private void efectuarCompraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_efectuarCompraActionPerformed
        String diaCompra = (String) comboBoxDias.getSelectedItem();
        Pasajero p = wt.esUsuarioPasajero(cedulaUsuario);
        ServicioDeTransporte s = (ServicioDeTransporte) servicioSeleccionadoP.getSelectedValue();
        String metodoPago = (String) jComboBoxPagos.getSelectedItem();
        String t = numeroTarj.getText();
        String t1 = numeroTarj1.getText();
        String t2 = numeroTarj2.getText();
        String t3 = numeroTarj3.getText();
        if (wt.esStringNumerico(cantidadAsientosP.getText()) && Integer.parseInt(cantidadAsientosP.getText()) != 0) {
            int asientosAComprar = Integer.parseInt(cantidadAsientosP.getText());
            if (wt.esStringNumerico(t) && wt.esStringNumerico(t1) && wt.esStringNumerico(t2) && wt.esStringNumerico(t3)
                    && t.length() == 4 && t1.length() == 4 && t2.length() == 4 && t3.length() == 4) {///Numerico y largo valido
                if ((s.getAsientosDisponibles() - asientosAComprar >= 0)) { //Hay asientos disponibles
                    wt.registrarCompraPasaje(s, p, asientosAComprar, diaCompra, metodoPago);
                    mensajeServicioP.setText("Pasaje comprado correctamente");
                    mensajeServicioP.setForeground(Color.green);
                    ArrayList<ServicioDeTransporte> sActualizado = new ArrayList<>();
                    sActualizado.add(s);
                    servicioSeleccionadoP.setListData(sActualizado.toArray());
                } else {//No quedan asientos para vender
                    mensajeServicioP.setText("No hay suficientes asientos disponibles");
                    mensajeServicioP.setForeground(Color.red);
                }
            } else {//No numerico o largo no valido
                mensajeServicioP.setText("La tarjeta debe ser de 16 digitos");
                mensajeServicioP.setForeground(Color.red);
            }

        } else { //El campo de cantidad de asientos no es numerico
            mensajeServicioP.setText("La cantidad de asientos es invalida");
            mensajeServicioP.setForeground(Color.red);
        }
        servicioSeleccionadoP.setSelectedIndex(0); //selecciona el servicio
        servicioSeleccionadoP.setCellRenderer(new DefaultListCellRenderer()); //blockea por prevencion, para que siempre este seleccionado
    }//GEN-LAST:event_efectuarCompraActionPerformed

    private void cantidadAsientosPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cantidadAsientosPActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cantidadAsientosPActionPerformed

    private void consultarServiciosP7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_consultarServiciosP7ActionPerformed
        mostrarConsultarServiciosP();
    }//GEN-LAST:event_consultarServiciosP7ActionPerformed

    private void informacionPersonal7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_informacionPersonal7ActionPerformed
        mostrarInfoPersonal();
    }//GEN-LAST:event_informacionPersonal7ActionPerformed

    private void comboBoxDiasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboBoxDiasActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_comboBoxDiasActionPerformed

    private void jComboBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox1ActionPerformed

    }//GEN-LAST:event_jComboBox1ActionPerformed

    private void confirmarNombreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_confirmarNombreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_confirmarNombreActionPerformed

    private void guardarPasActualizadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_guardarPasActualizadoActionPerformed
        errorCedulaNueva.setText("");
        errorNombreNuevo.setText("");
        errorTarjetaNueva.setText("");
        boolean cC = confirmarNuevaCedula.isSelected();
        boolean cN = confirmarNuevoNombre.isSelected();
        boolean cT = confirmarNuevaTarjeta.isSelected();
        String nC = nuevaCedula.getText();
        String nN = nuevoNombre.getText();
        String t = nuevat1.getText();
        String t1 = nuevat2.getText();
        String t2 = nuevat3.getText();
        String t3 = nuevat4.getText();
        String tarj = t.concat(t1).concat(t2).concat(t3);
        Pasajero pActual = wt.esUsuarioPasajero(cedulaUsuario);
        if (cC) { //Si selecciona confirmar cedula
            try {
                if (wt.validacionCedulaLogin(nC)) {//cedula valida
                    pActual.setCedula(nC);
                    cedulaUsuario = nC;
                    errorCedulaNueva.setText("Cedula modificada correctamente");
                    errorCedulaNueva.setForeground(Color.green);
                }
            } catch (MiExcepcion e) {
                errorCedulaNueva.setText(e.getMessage());
                errorCedulaNueva.setForeground(Color.red);
            }
        }
        if (cN) {//confirma nombre
            if (!nN.isEmpty()) {//nombre correcto
                pActual.setNombre(nN);
                errorNombreNuevo.setText("Nombre modificado correctamente");
                errorNombreNuevo.setForeground(Color.green);
            } else {//nombre vacio
                errorNombreNuevo.setText("Nombre vacio, ingrese nuevamente");
                errorNombreNuevo.setForeground(Color.red);
            }
        }
        if (cT) {//confirma tarjeta
            if (wt.esStringNumerico(t) && wt.esStringNumerico(t1) && wt.esStringNumerico(t2) && wt.esStringNumerico(t3) && tarj.length() == 16) {
                pActual.setTarjetaCredito(tarj);
                errorTarjetaNueva.setText("Tarjeta modificada correctamente");
                errorTarjetaNueva.setForeground(Color.green);
            } else {//tarjeta invalida
                errorTarjetaNueva.setText("La tarjeta debe contener 16 digitos");
                errorTarjetaNueva.setForeground(Color.red);
            }
        }
        confirmarNuevaCedula.setSelected(false);
        confirmarNuevoNombre.setSelected(false);
        confirmarNuevaTarjeta.setSelected(false);
    }//GEN-LAST:event_guardarPasActualizadoActionPerformed

    private void cerrarSesion7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cerrarSesion7ActionPerformed
        cerrarSesionF();
    }//GEN-LAST:event_cerrarSesion7ActionPerformed

    private void cerrarSesion6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cerrarSesion6ActionPerformed
        cerrarSesionF();
    }//GEN-LAST:event_cerrarSesion6ActionPerformed

    private void cerrarSesion5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cerrarSesion5ActionPerformed
        cerrarSesionF();
    }//GEN-LAST:event_cerrarSesion5ActionPerformed

    private void cerrarSesion4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cerrarSesion4ActionPerformed
        cerrarSesionF();
    }//GEN-LAST:event_cerrarSesion4ActionPerformed

    private void cerrarSesion3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cerrarSesion3ActionPerformed
        cerrarSesionF();
    }//GEN-LAST:event_cerrarSesion3ActionPerformed

    private void cerrarSesion2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cerrarSesion2ActionPerformed
        cerrarSesionF();
    }//GEN-LAST:event_cerrarSesion2ActionPerformed

    private void cerrarSesionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cerrarSesionActionPerformed
        cerrarSesionF();
    }//GEN-LAST:event_cerrarSesionActionPerformed

    private void cerrarSesion1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cerrarSesion1ActionPerformed
        cerrarSesionF();
    }//GEN-LAST:event_cerrarSesion1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        cerrarSesionP();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        cerrarSesionP();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void cerrarSeson9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cerrarSeson9ActionPerformed
        cerrarSesionP();
    }//GEN-LAST:event_cerrarSeson9ActionPerformed

    private void cerrarSesion10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cerrarSesion10ActionPerformed
        cerrarSesionP();
    }//GEN-LAST:event_cerrarSesion10ActionPerformed

    private void procesarArchivos8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_procesarArchivos8ActionPerformed
        procesarArchivos();
    }//GEN-LAST:event_procesarArchivos8ActionPerformed

    private void registrarServicio8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_registrarServicio8ActionPerformed
        registrarServ();
    }//GEN-LAST:event_registrarServicio8ActionPerformed

    private void crearFuncionario8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_crearFuncionario8ActionPerformed
        crearFunc();
    }//GEN-LAST:event_crearFuncionario8ActionPerformed

    private void modificarServicio8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_modificarServicio8ActionPerformed
        modificarServ();
    }//GEN-LAST:event_modificarServicio8ActionPerformed

    private void cambiarDatosPasajero8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cambiarDatosPasajero8ActionPerformed
        cambiarDatosPas();
    }//GEN-LAST:event_cambiarDatosPasajero8ActionPerformed

    private void consultarServicio8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_consultarServicio8ActionPerformed
        consultarServ();
    }//GEN-LAST:event_consultarServicio8ActionPerformed

    private void comprarPasaje8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comprarPasaje8ActionPerformed
        comprarPas();
    }//GEN-LAST:event_comprarPasaje8ActionPerformed

    private void cerrarSesion8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cerrarSesion8ActionPerformed
        cerrarSesionF();
    }//GEN-LAST:event_cerrarSesion8ActionPerformed
    public void consultarInfoPs() {
        crearNuevaCuenta.setEnabled(false);
        panelProcesarArchivos.setVisible(false);
        panelConsultarInfoPasajeros.setVisible(true);
        panelRegistrarFuncionario.setVisible(false);
        panelRegistroServicio.setVisible(false);
        panelModificarServicio.setVisible(false);
        panelModificarUsuario.setVisible(false);
        panelInicial.setVisible(false);
        panelConsultarServicios.setVisible(false);
        panelCompraPasajeF.setVisible(false);
        wt.obtenerHistorialesAJList(lstMostrarHistorialP);
        wt.obtenerPasajerosAJlist(lstMostrarPasajerosP);

    }
    private void jButton11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton11ActionPerformed
        consultarInfoPs();
    }//GEN-LAST:event_jButton11ActionPerformed

    private void jButton10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton10ActionPerformed
        consultarInfoPs();
    }//GEN-LAST:event_jButton10ActionPerformed

    private void jButton9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton9ActionPerformed
        consultarInfoPs();
    }//GEN-LAST:event_jButton9ActionPerformed

    private void jButton8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton8ActionPerformed
        consultarInfoPs();
    }//GEN-LAST:event_jButton8ActionPerformed

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        consultarInfoPs();
    }//GEN-LAST:event_jButton7ActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        consultarInfoPs();
    }//GEN-LAST:event_jButton6ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        consultarInfoPs();
    }//GEN-LAST:event_jButton5ActionPerformed

    private void informacionPasajero7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_informacionPasajero7ActionPerformed
        consultarInfoPs();
    }//GEN-LAST:event_informacionPasajero7ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        consultarInfoPs();
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jListaServiciosCompraMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jListaServiciosCompraMouseClicked
        if (wt.getListaServiciosDeTransporte().isEmpty()) {
            
        } else {
            ServicioDeTransporte s = (ServicioDeTransporte) jListaServiciosCompra.getSelectedValue();
            wt.setearFrecuenciaEnBox(jComboBox1, s.getFrecuencia());
        }
    }//GEN-LAST:event_jListaServiciosCompraMouseClicked

// <editor-fold defaultstate="collapsed" desc="Declaracion de componentes de interfaz">
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton buscarServiciosP;
    private javax.swing.JButton cambiarDatosPasajero;
    private javax.swing.JButton cambiarDatosPasajero1;
    private javax.swing.JButton cambiarDatosPasajero2;
    private javax.swing.JButton cambiarDatosPasajero3;
    private javax.swing.JButton cambiarDatosPasajero4;
    private javax.swing.JButton cambiarDatosPasajero5;
    private javax.swing.JButton cambiarDatosPasajero6;
    private javax.swing.JButton cambiarDatosPasajero7;
    private javax.swing.JButton cambiarDatosPasajero8;
    private javax.swing.JButton cancelarCreacion;
    private javax.swing.JTextField cantidadAsientos;
    private javax.swing.JTextField cantidadAsientosP;
    private javax.swing.JButton cargarServicios;
    private javax.swing.JTextField cedulaActual;
    private javax.swing.JTextField cedulaNueva;
    private javax.swing.JTextField cedulaPasajero;
    private javax.swing.JButton cerrarSesion;
    private javax.swing.JButton cerrarSesion1;
    private javax.swing.JButton cerrarSesion10;
    private javax.swing.JButton cerrarSesion2;
    private javax.swing.JButton cerrarSesion3;
    private javax.swing.JButton cerrarSesion4;
    private javax.swing.JButton cerrarSesion5;
    private javax.swing.JButton cerrarSesion6;
    private javax.swing.JButton cerrarSesion7;
    private javax.swing.JButton cerrarSesion8;
    private javax.swing.JButton cerrarSeson9;
    private javax.swing.JTextField ciudadDestino;
    private javax.swing.JTextField ciudadOrigen;
    private javax.swing.JComboBox comboBoxDias;
    private javax.swing.JButton comprarPasaje;
    private javax.swing.JButton comprarPasaje1;
    private javax.swing.JButton comprarPasaje2;
    private javax.swing.JButton comprarPasaje3;
    private javax.swing.JButton comprarPasaje4;
    private javax.swing.JButton comprarPasaje5;
    private javax.swing.JButton comprarPasaje6;
    private javax.swing.JButton comprarPasaje7;
    private javax.swing.JButton comprarPasaje8;
    private javax.swing.JButton comprarPasajeAPasajero;
    private javax.swing.JButton comprarPasajesP6;
    private javax.swing.JRadioButton confirmarBaja;
    private javax.swing.JRadioButton confirmarCedula;
    private javax.swing.JRadioButton confirmarNombre;
    private javax.swing.JRadioButton confirmarNuevaCedula;
    private javax.swing.JRadioButton confirmarNuevaTarjeta;
    private javax.swing.JRadioButton confirmarNuevoNombre;
    private javax.swing.JButton consultarServicio;
    private javax.swing.JButton consultarServicio1;
    private javax.swing.JButton consultarServicio2;
    private javax.swing.JButton consultarServicio3;
    private javax.swing.JButton consultarServicio4;
    private javax.swing.JButton consultarServicio5;
    private javax.swing.JButton consultarServicio6;
    private javax.swing.JButton consultarServicio7;
    private javax.swing.JButton consultarServicio8;
    private javax.swing.JButton consultarServiciosP1;
    private javax.swing.JButton consultarServiciosP5;
    private javax.swing.JButton consultarServiciosP6;
    private javax.swing.JButton consultarServiciosP7;
    private javax.swing.JButton crearCuenta;
    private javax.swing.JButton crearFuncionario;
    private javax.swing.JButton crearFuncionario1;
    private javax.swing.JButton crearFuncionario2;
    private javax.swing.JButton crearFuncionario3;
    private javax.swing.JButton crearFuncionario4;
    private javax.swing.JButton crearFuncionario5;
    private javax.swing.JButton crearFuncionario6;
    private javax.swing.JButton crearFuncionario7;
    private javax.swing.JButton crearFuncionario8;
    private javax.swing.JButton crearNuevaCuenta;
    private javax.swing.JTextField destinoServicioP;
    private javax.swing.JButton efectuarCompra;
    private javax.swing.JLabel errorCedulaNueva;
    private javax.swing.JLabel errorNombreNuevo;
    private javax.swing.JLabel errorPasajeroNoEsta;
    private javax.swing.JLabel errorTarjetaNueva;
    private javax.swing.JButton guardarPasActualizado;
    private javax.swing.JButton guardarPasajero;
    private javax.swing.JButton guardarTransporte;
    private javax.swing.JButton horaLlegada;
    private javax.swing.JButton horaSalida;
    private javax.swing.JButton informacionPasajero7;
    private javax.swing.JButton informacionPersonal1;
    private javax.swing.JButton informacionPersonal5;
    private javax.swing.JButton informacionPersonal6;
    private javax.swing.JButton informacionPersonal7;
    private javax.swing.JButton ingresoSesion;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton10;
    private javax.swing.JButton jButton11;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JButton jButton8;
    private javax.swing.JButton jButton9;
    private javax.swing.JComboBox jComboBox1;
    private javax.swing.JComboBox jComboBoxPagos;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel44;
    private javax.swing.JLabel jLabel45;
    private javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel47;
    private javax.swing.JLabel jLabel48;
    private javax.swing.JLabel jLabel49;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel50;
    private javax.swing.JLabel jLabel51;
    private javax.swing.JLabel jLabel52;
    private javax.swing.JLabel jLabel53;
    private javax.swing.JLabel jLabel54;
    private javax.swing.JLabel jLabel55;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLayeredPane jLayeredPane;
    private javax.swing.JList jListaServicios;
    private javax.swing.JList jListaServiciosCompra;
    private javax.swing.JList jListaServiciosEncontrados;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane10;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JScrollPane jScrollPane9;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField3;
    private javax.swing.JTextField jTextField6;
    private javax.swing.JLabel lblAvisoCompraP;
    private javax.swing.JLabel lblMensajeCompraP;
    private javax.swing.JLabel lblMensajeCreacion;
    private javax.swing.JLabel lblMensajeCreacionFunc;
    private javax.swing.JLabel lblMensajeLogin;
    private javax.swing.JLabel lblMensajeServicio;
    private javax.swing.JLabel lblMensajeServiciosP;
    private javax.swing.JList listaServiciosP;
    private javax.swing.JList lstMostrarHistorialP;
    private javax.swing.JList lstMostrarPasajerosP;
    private javax.swing.JLabel mensajeCedula;
    private javax.swing.JLabel mensajeServicio;
    private javax.swing.JLabel mensajeServicioP;
    private javax.swing.JButton modificarServicio;
    private javax.swing.JButton modificarServicio1;
    private javax.swing.JButton modificarServicio2;
    private javax.swing.JButton modificarServicio3;
    private javax.swing.JButton modificarServicio4;
    private javax.swing.JButton modificarServicio5;
    private javax.swing.JButton modificarServicio6;
    private javax.swing.JButton modificarServicio7;
    private javax.swing.JButton modificarServicio8;
    private javax.swing.JList mostrarHistorialP;
    private javax.swing.JTextField nombreNuevo;
    private javax.swing.JTextField nuevaCedula;
    private javax.swing.JTextField nuevaHoraFin;
    private javax.swing.JTextField nuevaHoraInicio;
    private javax.swing.JTextField nuevat1;
    private javax.swing.JTextField nuevat2;
    private javax.swing.JTextField nuevat3;
    private javax.swing.JTextField nuevat4;
    private javax.swing.JTextField nuevoNombre;
    private javax.swing.JTextField numeroTarj;
    private javax.swing.JTextField numeroTarj1;
    private javax.swing.JTextField numeroTarj2;
    private javax.swing.JTextField numeroTarj3;
    private javax.swing.JButton ordPorHoraLlegadaP;
    private javax.swing.JButton ordPorHoraSalidaP;
    private javax.swing.JButton ordPorPrecioP;
    private javax.swing.JButton ordPorTransporte;
    private javax.swing.JButton ordenarPrecio;
    private javax.swing.JTextField origenServicioP;
    private javax.swing.JPanel panelCompraPasajeF;
    private javax.swing.JPanel panelCompraViajeP;
    private javax.swing.JPanel panelConsultarInfoPasajeros;
    private javax.swing.JPanel panelConsultarServicios;
    private javax.swing.JPanel panelConsultarServiciosP;
    private javax.swing.JPanel panelCreacionUsuario;
    private javax.swing.JPanel panelIngreso;
    private javax.swing.JPanel panelInicial;
    private javax.swing.JPanel panelInicialP;
    private javax.swing.JPanel panelMenuFuncionario;
    private javax.swing.JPanel panelMenuPasajero;
    private javax.swing.JPanel panelModificarP;
    private javax.swing.JPanel panelModificarServicio;
    private javax.swing.JPanel panelModificarUsuario;
    private javax.swing.JPanel panelProcesarArchivos;
    private javax.swing.JPanel panelRegistrarFuncionario;
    private javax.swing.JPanel panelRegistroServicio;
    private javax.swing.JLabel pasajeroBorrado;
    private javax.swing.JButton procesarArchivos;
    private javax.swing.JButton procesarArchivos1;
    private javax.swing.JButton procesarArchivos2;
    private javax.swing.JButton procesarArchivos3;
    private javax.swing.JButton procesarArchivos4;
    private javax.swing.JButton procesarArchivos5;
    private javax.swing.JButton procesarArchivos6;
    private javax.swing.JButton procesarArchivos7;
    private javax.swing.JButton procesarArchivos8;
    private javax.swing.JRadioButton rDomingo;
    private javax.swing.JRadioButton rJueves;
    private javax.swing.JRadioButton rLunes;
    private javax.swing.JRadioButton rMartes;
    private javax.swing.JRadioButton rMiercoles;
    private javax.swing.JRadioButton rSabado;
    private javax.swing.JRadioButton rViernes;
    private javax.swing.JButton registrarNuevoFunc;
    private javax.swing.JButton registrarServicio;
    private javax.swing.JButton registrarServicio1;
    private javax.swing.JButton registrarServicio2;
    private javax.swing.JButton registrarServicio3;
    private javax.swing.JButton registrarServicio4;
    private javax.swing.JButton registrarServicio5;
    private javax.swing.JButton registrarServicio6;
    private javax.swing.JButton registrarServicio7;
    private javax.swing.JButton registrarServicio8;
    private javax.swing.JButton registroServicio;
    private javax.swing.JLabel resultadoCargarServicios;
    private javax.swing.JLabel resultadoModificarServicio;
    private javax.swing.JLabel resultadoUsuario;
    private javax.swing.JList servicioSeleccionadoP;
    private javax.swing.JComboBox tiposTransporte;
    private javax.swing.JButton transporte;
    private javax.swing.JTextField txtAsientos;
    private javax.swing.JTextField txtCedulaFunc;
    private javax.swing.JTextField txtCrear;
    private javax.swing.JTextField txtDestino;
    private javax.swing.JTextField txtDuracion;
    private javax.swing.JTextField txtHora;
    private javax.swing.JTextField txtIdentificador;
    private javax.swing.JTextArea txtInfoArchivos;
    private javax.swing.JTextField txtNomCrear;
    private javax.swing.JTextField txtNombreFunc;
    private javax.swing.JTextField txtOrigen;
    private javax.swing.JTextField txtPrecio;
    private javax.swing.JTextField txtUsuario;
    // End of variables declaration//GEN-END:variables
//</editor-fold>

}
