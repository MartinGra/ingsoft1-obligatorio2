package dominio;

public class Funcionario {
    private String nombre;
    private String cedula;
    
    public Funcionario(){
        this.nombre = "";
        this.cedula = "";
    }
    public Funcionario(String unNombre, String unaCedula){
        this.nombre = unNombre;
        this.cedula = unaCedula;
    }
    public String getCedula() {
        return cedula;
    }
    public void setCedula(String cedula) {
        this.cedula = cedula;
    }
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
   
}
