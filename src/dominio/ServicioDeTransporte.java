package dominio;
 
import java.util.Objects;

public class ServicioDeTransporte implements Comparable <ServicioDeTransporte>{
    private String identificador;
    private String origen;
    private String destino;
    private String horaSalida;
    private String horaLlegada;
    private String frecuencia;
    private int precio;
    private Transporte transporte;
    private int asientosDisponibles;
    
    public ServicioDeTransporte (String unIdentificador,String unOrigen, String unDestino, String unaHoraSalida, String unaHoraLlegada, String unaFrecuencia, int unPrecio, Transporte unTransporte, int asientosDisponibles) {
        this.identificador = unIdentificador;
        this.origen = unOrigen;
        this.destino = unDestino;
        this.horaSalida = unaHoraSalida;
        this.horaLlegada = unaHoraLlegada;
        this.frecuencia = unaFrecuencia;
        this.precio = unPrecio;
        this.transporte = unTransporte;
        this.asientosDisponibles = asientosDisponibles;
    }
    
    public String getHoraSalida() {
        return horaSalida;
    }

    public void setHoraSalida(String horaSalida) {
        this.horaSalida = horaSalida;
    }

    public String getHoraLlegada() {
        return horaLlegada;
    }

    public void setHoraLlegada(String horaLlegada) {
        this.horaLlegada = horaLlegada;
    }
    
    public int getAsientosDisponibles() {
        return asientosDisponibles;
    }

    public void setAsientosDisponibles(int asientosDisponibles) {
        this.asientosDisponibles = asientosDisponibles;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }
    
    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    public Transporte getTransporte() {
        return transporte;
    }

    public void setTransporte(Transporte transporte) {
        this.transporte = transporte;
    }
    
    public String getIdentificador() {
        return identificador;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    public String getFrecuencia() {
        return frecuencia;
    }

    public void setFrecuencia(String frecuencia) {
        this.frecuencia = frecuencia;
    }
    
    @Override
    public String toString() {
        return origen + "|" + destino + "|" + horaSalida + "|" + horaLlegada + "|" + frecuencia + "|" + precio + "|" + transporte +"|"+ asientosDisponibles;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ServicioDeTransporte other = (ServicioDeTransporte) obj;
        return Objects.equals(this.identificador, other.identificador);
    }
    
    @Override
    public int compareTo (ServicioDeTransporte o) {
        return o.getPrecio() - this.getPrecio();
    }
}
