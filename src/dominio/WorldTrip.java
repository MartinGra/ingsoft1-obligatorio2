package dominio;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import javax.swing.JComboBox;
import javax.swing.JList;
import javax.swing.ListModel;

public class WorldTrip {

    private final ArrayList<Funcionario> listaFuncionarios;
    private final ArrayList<Pasajero> listaPasajeros;
    private final ArrayList<ServicioDeTransporte> listaServiciosDeTransporte;
    private final ArrayList<Viaje> listaViajes;
    private static volatile WorldTrip instance;
    private static final WorldTrip myLock = new WorldTrip();

    private WorldTrip() {
        this.listaFuncionarios = new ArrayList<>();
        this.listaPasajeros = new ArrayList<>();
        this.listaServiciosDeTransporte = new ArrayList<>();
        this.listaViajes = new ArrayList<>();
    }

    public static WorldTrip getInstance() {
        if (instance == null) {
            synchronized (WorldTrip.myLock) {
                if (instance == null) {
                    instance = new WorldTrip();
                }
            }
        }
        return instance;
    }

    public ArrayList<Funcionario> getListaFuncionarios() {
        return listaFuncionarios;
    }

    public ArrayList<ServicioDeTransporte> getListaServiciosDeTransporte() {
        return listaServiciosDeTransporte;
    }

    public ArrayList<Viaje> getListaViajes() {
        return listaViajes;
    }

    public ArrayList<Pasajero> getListaPasajeros() {
        return listaPasajeros;
    }

    public void agregarPasajero(Pasajero p) {
        listaPasajeros.add(p);
    }

    public void agregarFuncionario(Funcionario f) {
        listaFuncionarios.add(f);
    }

    public void agregarViaje(Viaje v) {
        listaViajes.add(v);
    }

    public void agregarServicioDeTransporte(ServicioDeTransporte unServicioDeTransporte) {
        listaServiciosDeTransporte.add(unServicioDeTransporte);
    }

    //Metodo para poder iniciar la sesion en el sistema como un usuario pasajero o funcionario
    public boolean iniciarSesion(String cedula, boolean[] pasajYUs) throws MiExcepcion {
        boolean ret;
        if (esUsuarioFuncionario(cedula) != null) {//Es usuario funcionario
            ret = true;
            pasajYUs[0] = false;
            pasajYUs[1] = true;
        } else if (esUsuarioPasajero(cedula) != null) {//Es usuario pasajero
            ret = true;
            pasajYUs[0] = true;
            pasajYUs[1] = true;
        } else {
            ret = false;
            pasajYUs[1] = false;
            MiExcepcion failedLogin = new MiExcepcion("El usuario seleccionado no pertenece al sistema");
            throw failedLogin;
        }
        return ret;
    }

    /*Metodo de creacion de un nuevo pasajero, setea la tarjeta del mismo vacia por defecto
     y lo agrega a la lista de pasajeros*/
    public void crearCuenta(String cedula, String nombre) {
        Pasajero nuevoPasajero = new Pasajero(nombre, cedula, "");
        agregarPasajero(nuevoPasajero);
    }
    /*Metodo para la creacion de un nuevo funcionario, al crearlo lo agrega a la lista del sistema*/

    public void crearFuncionario(String nombre, String cedula) {
        Funcionario func = new Funcionario(nombre, cedula);
        agregarFuncionario(func);
    }
    /*Recibe una cedula y busca en la lista de funcionarios si alguno coincide con la cedula ingresada
     y retorna al Funcionario,de no encontrar ninguno retorna null*/

    public Funcionario esUsuarioFuncionario(String cedula) {
        Funcionario fRet = null;
        for (int i = 0; i < getListaFuncionarios().size(); i++) {
            if (getListaFuncionarios().get(i).getCedula().equals(cedula)) {
                fRet = getListaFuncionarios().get(i);
            }
        }
        return fRet;
    }
    /*Recibe una cedula y retorna true si la misma le pertenece a algun pasajero registrado en el sistema*/

    public boolean esUsPas(String cedula) {
        boolean ret = false;
        for (int i = 0; i < getListaPasajeros().size(); i++) {
            if (getListaPasajeros().get(i).getCedula().equals(cedula)) {
                ret = true;
            }
        }
        return ret;
    }
    /*Recibe un cedula y retorna el Pasajero que esta registrado con dicha cedula, de no encontrar
     uno, retorna null*/

    public Pasajero esUsuarioPasajero(String cedula) {
        Pasajero pRet = null;
        for (int i = 0; i < getListaPasajeros().size(); i++) {
            if (getListaPasajeros().get(i).getCedula().equals(cedula)) {
                pRet = getListaPasajeros().get(i);
            }
        }
        return pRet;
    }
    /*Recibe un Pasajero y lo borra del sistema */

    public void borrarPasajero(Pasajero p) {
        this.getListaPasajeros().remove(p);
    }

    /*Metodo para validar la cedula al momento de hacer login, retorna true si no es vacia, 
     si es nummerica y si su largo es exactamente 8, de lo contrario retorna una excepcion*/
    public boolean validacionCedulaLogin(String cedula) throws MiExcepcion {
        boolean ret = true;
        MiExcepcion BlankField = new MiExcepcion("La cedula ingresada es vacia");
        MiExcepcion IncorrectLength = new MiExcepcion("El largo de la cedula debe ser 8 caracteres");
        MiExcepcion NonNumeric = new MiExcepcion("Debe ingresar unicamente caracteres numericos");
        if (cedula.length() == 0) {
            throw BlankField;
        }
        if (!esStringNumerico(cedula)) {
            throw NonNumeric;
        }
        if (cedula.length() != 8) {
            throw IncorrectLength;
        }
        return ret;
    }
    /*Metodo para validar la cedula al momento de crear un nuevo usuario, si la cedula no le pertenece
     a un Pasajero o Funcionario retorna el metodo que valida que la forma de la cedula sea correcta*/

    public boolean validacionCedulaCreacion(String cedula) throws MiExcepcion {
        MiExcepcion AlreadyMember = new MiExcepcion("El usuario ingresado ya pertenece al sistema");
        if (esUsuarioFuncionario(cedula) != null || esUsuarioPasajero(cedula) != null) {
            throw AlreadyMember;
        }
        return validacionCedulaLogin(cedula);
    }
    /*Valida que la hora ingresada, tenga la forma correcta (hh:mm) donde la hora puede estar entre 00-23
     y los minutos entre 00-59*/

    public boolean validarHora(String unaHora) {
        boolean ret = false;
        if (unaHora.length() == 5) {
            String horas = unaHora.substring(0, 2);
            char separador = unaHora.charAt(2);
            String minutos = unaHora.substring(3, 5);
            if (esStringNumerico(horas) && esStringNumerico(minutos) && separador == ':') { //Tengo que validar antes de entrar, porque sino parseInt tira una excepcion
                int horasInt = Integer.parseInt(horas);
                int minutosInt = Integer.parseInt(minutos);
                if ((horasInt >= 0 && horasInt <= 23) && (minutosInt >= 0 && minutosInt <= 59)) {
                    ret = true;
                }
            }
        }
        return ret;
    }
    /*Metodo para validar si un string es numerico, retorna true si y solo si todos los caracteres son digitos*/

    public boolean esStringNumerico(String s) {
        boolean ret = true;
        if (s.length() == 0) {
            ret = false;
        }
        for (int i = 0; i < s.length(); i++) {
            Character charTemp = s.charAt(i);
            if (!Character.isDigit(charTemp)) {
                ret = false;
            }
        }
        return ret;
    }
    /*Metodo para validar si un identificador es correcto, retorna true si y solo si 
     los primeros 3 caracteres son letras mayusculas y los 4 ultimos digitos*/

    public boolean validarId(String unId) {
        String regex = "^[A-Z]{3}\\d{4}";
        return unId.matches(regex);
    }
    /*Valida que el identificador sea unico, retorna true si al buscar en la lista de servicios
     ningun otro tiene dicho identificador*/

    public boolean idUnico(String unId) {
        boolean esUnico = true;
        for (ServicioDeTransporte s : listaServiciosDeTransporte) {
            if (s.getIdentificador().equals(unId)) {
                esUnico = false;
            }
        }
        return esUnico;
    }

    /*Metodo para el procesamiento de archivos, recibe un path y genera un ArchivoLectura del cual
     va a llamar al metodo cargarArchivo por cada una de sus lineas*/
    public void procesamientoDeArchivos(String pathArchivo, String nombreArchivo, int[] cantPyS, ArrayList<String> idsEmpr) {
        ArchivoLectura aL = new ArchivoLectura(pathArchivo);
        while (aL.hayMasLineas()) {
            String linea = aL.linea();
            String[] tokens = linea.split("\\|"); // AMIRA!, caracter especial del regex de split
            cargarArchivo(tokens, nombreArchivo, cantPyS, idsEmpr);
        }
        aL.cerrar();
    }
    /*Recibe un array con atributos, y el nombre de un archivo y diferencia 3 tipos de carga a partir del nombre
     y carga exitosamente si segun el nombre del archivo los atributos son correctos*/

    public void cargarArchivo(String[] arrAtributos, String nombreArchivo, int[] contPyS, ArrayList<String> idsE) {
        String identificadorEmpresa = nombreArchivo.substring(0, 3);
        boolean cargo = false;
        String tipoArchivo = nombreArchivo.substring(3);
        if (tipoArchivo.equals("pasajeros.txt") && arrAtributos.length == 3) { //Cargo Pasajero
            String nombre= arrAtributos[0];
            String cedula = arrAtributos[1];
            String tarjeta = arrAtributos[2];
            try {
                boolean ok = validacionCedulaCreacion(cedula);
                if (ok && (tarjeta.equals(" ")|| (!tarjeta.isEmpty() && esStringNumerico(tarjeta) && tarjeta.length() ==16)) && !nombre.isEmpty()) {
                    Pasajero pArchivo = new Pasajero(nombre, cedula, tarjeta);
                    agregarPasajero(pArchivo);
                    contPyS[0]++;
                    cargo = true;
                    System.out.println("nuevo pasajero añadido:" + pArchivo);
                }
            } catch (MiExcepcion e) {
                //No cargo el archivo
            }
        }
        if (tipoArchivo.equals("historialVentas.txt") && arrAtributos.length == 12) { //Cargo Viaje
            String orig = arrAtributos[0];
            String dest = arrAtributos[1];
            String horaSalida = arrAtributos[2];
            String horaLlegada = arrAtributos[3];
            String precioStr = arrAtributos[4];
            Transporte trans = new Transporte(arrAtributos[5]);
            String nombreP = arrAtributos[6];
            String cedulaP = arrAtributos[7];
            String tarjetaP = arrAtributos[8]; //Puede ser vacia o si no es vacia debe ser numerico
            String dia = arrAtributos[9];
            String cantPasajesStr = arrAtributos[10];
            String metodoPago = arrAtributos[11];
            if (esStringNumerico(precioStr) && esStringNumerico(cantPasajesStr) && (tarjetaP.equals(" ")
                    || (!tarjetaP.isEmpty() && esStringNumerico(tarjetaP)) && tarjetaP.length() == 16)) {//Valido que el precio, cantidad pasajes sean numericos
                int precio = Integer.parseInt(precioStr);
                int cantidadPasajes = Integer.parseInt(precioStr);
                if (!orig.isEmpty() && !dest.isEmpty() && !trans.getTipo().isEmpty() && !nombreP.isEmpty()
                        && !metodoPago.isEmpty() && validarHora(horaLlegada) && validarHora(horaSalida)) {
                    if (esUsuarioPasajero(cedulaP) == null) {//Si no existe el pasajero lo creo    
                        Pasajero pasaj = new Pasajero(nombreP, cedulaP, tarjetaP);
                        Viaje vArchivo = new Viaje(orig, dest, horaSalida, horaLlegada, precio, trans, pasaj, dia, cantidadPasajes, metodoPago);
                        agregarViaje(vArchivo);
                        agregarPasajero(pasaj);
                        contPyS[0]++;
                        System.out.println("Nuevo viaje agregado: " +vArchivo);
                        System.out.println("Nuevo pasajero agregado: "+ pasaj);
                    } else {//Si existe el usuario pasajero
                        Viaje vArchivo = new Viaje(orig, dest, horaSalida, horaLlegada, precio, trans, esUsuarioPasajero(cedulaP), dia, cantidadPasajes, metodoPago);
                        agregarViaje(vArchivo);
                        System.out.println("Nuevo viaje agregado: "+ vArchivo);
                    }
                    cargo = true;
                }
            }
        }
        if (tipoArchivo.equals("serviciosTransporte.txt") && arrAtributos.length == 9) { //Cargo Servicio de Transporte
            String ident = arrAtributos[0];
            String orig = arrAtributos[1];
            String dest = arrAtributos[2];
            String salida = arrAtributos[3];
            String llegada = arrAtributos[4];
            String frecuencia = arrAtributos[5];//Suponemos que es un string  no vacio correcto
            String precioStr = arrAtributos[6];
            Transporte t = new Transporte(arrAtributos[7]);
            String asientosStr = arrAtributos[8];   
            if (idUnico(ident) && validarId(ident) && esStringNumerico(asientosStr) && esStringNumerico(precioStr) 
                && !orig.isEmpty() && !dest.isEmpty() && validarHora(salida) && validarHora(llegada)) {
                int asientosD = Integer.parseInt(asientosStr);
                int precio = Integer.parseInt(precioStr);
                ServicioDeTransporte sArchivo = new ServicioDeTransporte(ident, orig, dest, salida, llegada, frecuencia, precio, t, asientosD);
                agregarServicioDeTransporte(sArchivo);
                contPyS[1]++;
                cargo = true;
                System.out.println("Nuevo servicio: " + sArchivo);
            }
        }
        if (cargo) { // si cargue algun Pasajero, Servicio o Viaje, agrego al contador de empresas la empresa si esta es unica
            if (!idsE.contains(identificadorEmpresa)) {
                idsE.add(identificadorEmpresa);
            }
        }
    }

    /*Recibe booleans representando si se selecciono un dia de la semana, retorna un string con los dias 
     seleccionados*/
    public String[] setearDiasDisponibles(boolean l, boolean ma, boolean mi, boolean j, boolean v, boolean s, boolean d) {
        String[] diasDisponibles = new String[7];
        Arrays.fill(diasDisponibles, "");
        if (l) {
            diasDisponibles[0] = "L";
        }
        if (ma) {
            diasDisponibles[1] = "Ma";
        }
        if (mi) {
            diasDisponibles[2] = "Mi";
        }
        if (j) {
            diasDisponibles[3] = "J";
        }
        if (v) {
            diasDisponibles[4] = "V";
        }
        if (s) {
            diasDisponibles[5] = "S";
        }
        if (d) {
            diasDisponibles[6] = "D";
        }
        return diasDisponibles;
    }
    /*Recibe un array de Strings y retorna un String unico, el cual contiene separado por comas los dias
     si se encuentran las 7 posiciones con Strings distintos al vacio, retorna una frecuencia Diaria*/

    public String calcularFrecuencia(String[] diasD) {
        String ret = "";
        int count = 0;
        if (esDiario(diasD)) {
            ret = "Diario";
        } else {
            for (String diasD1 : diasD) {
                if (!diasD1.isEmpty()) {
                    if (count > 0) {
                        ret = ret.concat("," + diasD1);
                    } else {
                        ret = ret.concat(diasD1);
                    }
                    count++;
                }
            }
        }
        return ret;
    }
    /*Recibe un string de dias y retorna true si todas las 7 posiciones son distintas del String vacio*/

    public boolean esDiario(String[] dias) {
        boolean ret = true;
        for (String dia : dias) {
            if (dia.isEmpty()) {
                ret = false;
            }
        }
        return ret;
    }
    /*Crea un nuevo servicio de transporte y lo agrega al sistema*/

    public void crearServicioTransporte(String unId, String unOrigen, String unDestino, String unaHoraS, String unaHoraL, String unaFrecuencia, int unPrecio, Transporte unTransporte, int asientosDisponibles) {
        ServicioDeTransporte nuevoS = new ServicioDeTransporte(unId, unOrigen, unDestino, unaHoraS, unaHoraL, unaFrecuencia, unPrecio, unTransporte, asientosDisponibles);
        agregarServicioDeTransporte(nuevoS);
    }
    /*Recibe un servicio y una nueva hora de salida y llegada, actualiza el servicio con los nuevos datos */

    public void modificarServicioDeTransporte(ServicioDeTransporte unServicio, String unaHoraSalida, String unaHoraLlegada) {
        unServicio.setHoraSalida(unaHoraSalida);
        unServicio.setHoraLlegada(unaHoraLlegada);
    }
    /*A partir de un parametro origen y destino, retorna una lista con los servicios que tienen los mismos atributos*/

    public ArrayList<ServicioDeTransporte> seleccionarServicios(String cOrigen, String cDestino) {
        ArrayList<ServicioDeTransporte> ret = new ArrayList<>();
        for (ServicioDeTransporte s : listaServiciosDeTransporte) {
            if (cOrigen.equalsIgnoreCase(s.getOrigen()) && cDestino.equalsIgnoreCase(s.getDestino())) {
                ServicioDeTransporte nuevo = s;
                ret.add(nuevo);
            }
        }
        return ret;
    }
    /*Recibe un JList y obtiene cada uno de sus elementos que son objetos de tipo ServicioDeTransporte
     y los retorna en una lista*/

    public ArrayList<ServicioDeTransporte> obtenerServiciosDeJList(JList j) {
        ArrayList<ServicioDeTransporte> serviciosEncontrados = new ArrayList<>();
        ListModel listaElementos = j.getModel();
        for (int i = 0; i < listaElementos.getSize(); i++) {
            ServicioDeTransporte sTemp = (ServicioDeTransporte) listaElementos.getElementAt(i);
            serviciosEncontrados.add(sTemp);
        }
        return serviciosEncontrados;
    }
    /*Recibe un jList, obtiene sus elementos en una lista,los ordena de acuerdo al tipo de sorting elegido
     y setea la lista ordenada al jList*/

    public void ordenarPorX(JList j, String sorting) {
        ArrayList<ServicioDeTransporte> servsEncontrados = obtenerServiciosDeJList(j);
        if (sorting.equals("porPrecio")) {
            Collections.sort(servsEncontrados);
        }
        if (sorting.equals("porTransporte")) {
            servsEncontrados.sort(new CompararPorTransporte());
        }
        if (sorting.equals("porHoraSalida")) {
            servsEncontrados.sort(new CompararPorHora(false));
        }
        if (sorting.equals("porHoraLlegada")) {
            servsEncontrados.sort(new CompararPorHora(true));
        }
        j.setListData(servsEncontrados.toArray());
    }
    /*Recibe un comboBox y una frecuencia, carga dias de la semana en el combo box, segun como
     sea la frecuencia*/

    public void setearFrecuenciaEnBox(JComboBox cBox, String frec) {
        cBox.removeAllItems();
        if (frec.equals("Diario")) {
            cBox.addItem("Lunes");
            cBox.addItem("Martes");
            cBox.addItem("Miercoles");
            cBox.addItem("Jueves");
            cBox.addItem("Viernes");
            cBox.addItem("Sabado");
            cBox.addItem("Domingo");
        } else {
            String[] dias = frec.split(",");
            for (String s : dias) {
                if (s.equals("L")) {
                    cBox.addItem("Lunes");
                }
                if (s.equals("Ma")) {
                    cBox.addItem("Martes");
                }
                if (s.equals("Mi")) {
                    cBox.addItem("Miercoles");
                }
                if (s.equals("J")) {
                    cBox.addItem("Jueves");
                }
                if (s.equals("V")) {
                    cBox.addItem("Viernes");
                }
                if (s.equals("S")) {
                    cBox.addItem("Sabado");
                }
                if (s.equals("D")) {
                    cBox.addItem("Domingo");
                }
            }
        }
    }
    /*Registra un nuevo Viaje con una cantidad de pasajes definida y lo agrega a la lista de viajes. Tambien 
     actualiza del servicio sus asientos disponibles*/

    public void registrarCompraPasaje(ServicioDeTransporte s, Pasajero p, int cantPasajes, String dia, String metodoPago) {
        Viaje compraPasaje = new Viaje(s.getOrigen(), s.getDestino(), s.getHoraSalida(), s.getHoraLlegada(), s.getPrecio(), s.getTransporte(), p, dia, cantPasajes, metodoPago);
        s.setAsientosDisponibles(s.getAsientosDisponibles() - cantPasajes);
        agregarViaje(compraPasaje);
    }
    /*Recibe un pasajero y un JList, carga los viajes registrados en nombre del pasajero, de no tener ninguno
     carga un aviso*/

    public void mostrarEnJListHistorialP(JList lista, Pasajero p) {
        lista.removeAll();//Revisar si va o no
        ArrayList<String> historial = new ArrayList<>();
        for (Viaje v : getListaViajes()) {
            if (v.getPasajero().equals(p)) {
                String sTemp = v.getOrigen() + "|" + v.getDestino() + "|" + v.getHoraSalida() + "|" + v.getHoraLlegada() + "|" + v.getDia() + "|" + v.getTransporte() + "|" + v.getPrecio() + "|" + v.getCantidadPasajes() + "|" + v.getMetodoPago();
                historial.add(sTemp);
            }
        }
        if (historial.isEmpty()) {
            historial.add("No tiene compras realizadas en su cuenta");
        }
        lista.setListData(historial.toArray());
    }
    /*Retorna true si la tarjeta recibida contiene unicamente digitos y su largo es 16*/

    public boolean validarTarjeta(String tarjeta) {
        boolean ret = false;
        if (tarjeta.length() == 16 && esStringNumerico(tarjeta)) {
            ret = true;
        }
        return ret;
    }
    /*Carga en un JList los historiales(Viajes) registrados en el sistema, de no haber niguno agrega un mensaje*/

    public void obtenerHistorialesAJList(JList j) {
        j.removeAll();
        if (listaViajes.isEmpty()) {
            String[] arr = new String[1];
            arr[0] = "No hay compras registradas en el sistema";
            j.setListData(arr);
        } else {
            j.setListData(listaViajes.toArray());
        }
    }
    /*Carga en un JList la lista de pasajeros registrados en el sistema, de no haber ninguno agrega un mensaje*/

    public void obtenerPasajerosAJlist(JList j) {
        j.removeAll();
        if (listaPasajeros.isEmpty()) {
            String[] arr = new String[1];
            arr[0] = "No hay pasajeros registrados en el sistema";
            j.setListData(arr);
        } else {
            j.setListData(listaPasajeros.toArray());
        }
    }
    public void obtenerServiciosAJList(JList j){
        j.removeAll();
        if(listaServiciosDeTransporte.isEmpty()){
            String[] arr = new String[1];
            arr[0] = "No hay servicios registrados en el sistema";
            j.setListData(arr);
        } else {
            j.setListData(listaServiciosDeTransporte.toArray());
        }
       
    }
}
