package dominio;

import java.io.Serializable;
import java.util.Comparator;

public class CompararPorTransporte implements Comparator<ServicioDeTransporte>, Serializable{
    public static final long serialVersionUID = 1L;
    
    @Override
    public int compare(ServicioDeTransporte o1, ServicioDeTransporte o2) {
        return o1.getTransporte().getTipo().compareTo(o2.getTransporte().getTipo());
    }
}
