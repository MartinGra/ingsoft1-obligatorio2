package dominio;

import java.util.Objects;

public class Transporte {
    private String tipo;

    public Transporte(String unTipo){
        this.tipo = unTipo;
    }
    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
    
    @Override
    public String toString() {
        return tipo; 
    }

    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Transporte other = (Transporte) obj;
        return Objects.equals(this.tipo, other.tipo);
    }
    
    
    public int compareTo (Transporte o) {
        return this.getTipo().compareTo(tipo);
    }
    
}
