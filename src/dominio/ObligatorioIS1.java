package dominio;

import interfaz.VentanaPrincipal;

public class ObligatorioIS1 {

    public static void main(String[] args) {
        WorldTrip wt = WorldTrip.getInstance();
        VentanaPrincipal v = new VentanaPrincipal();
        v.setVisible(true);
        Funcionario f = new Funcionario("admin", "00000000");
        wt.agregarFuncionario(f);
    }

}
