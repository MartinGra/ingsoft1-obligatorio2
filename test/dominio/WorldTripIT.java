/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import java.util.ArrayList;
import javax.swing.JComboBox;
import javax.swing.JList;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Martin
 */
public class WorldTripIT {
    
    public WorldTripIT() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Test
    public void testGetInstance() {
        System.out.println("getInstance");
        WorldTrip expResult = WorldTrip.getInstance();
        WorldTrip result = WorldTrip.getInstance();
        assertEquals(expResult, result);
    }
    @Test
    public void testAgregarPasajero() {
        System.out.println("agregarPasajero");
        Pasajero p = new Pasajero("laura", "12321232", "");
        WorldTrip instance = WorldTrip.getInstance();
        int largo = instance.getListaPasajeros().size();
        instance.agregarPasajero(p);
        int expected = instance.getListaPasajeros().size();
        assertEquals(expected, largo + 1);
    }
    
    @Test
    public void testAgregarFuncionario() {
        System.out.println("agregarFuncionario");
        Funcionario f = new Funcionario("funcionario1", "18181818");
        WorldTrip instance = WorldTrip.getInstance();
        int largo = instance.getListaFuncionarios().size();
        instance.agregarFuncionario(f);
        int expected = instance.getListaFuncionarios().size();
        assertEquals(expected, largo + 1);
    }
    
    @Test
    public void testAgregarViaje() {
        System.out.println("agregarViaje");
        Viaje v = new Viaje("orig", "dest", "12:12", "10:10", 123, new Transporte("avion"), new Pasajero("paul", "33333333", ""), "Lunes", 42, "Oca");
        WorldTrip instance = WorldTrip.getInstance();
        int largo = instance.getListaViajes().size();
        instance.agregarViaje(v);
        int resultado = instance.getListaViajes().size();
        assertEquals(resultado, largo + 1);
    }
    
    @Test(expected = MiExcepcion.class)
    public void testIniciarSesion() throws Exception {
        System.out.println("Prueba iniciarSesion1");
        String cedula = "1234";
        boolean[] pasajYUs = {true, true};
        WorldTrip instance = WorldTrip.getInstance();
        instance.iniciarSesion(cedula, pasajYUs);
    }
    
    @Test(expected = MiExcepcion.class)
    public void testIniciarSesion2() throws Exception {
        System.out.println("Prueba iniciarSesion2");
        WorldTrip instance = WorldTrip.getInstance();
        String cedula = "";
        boolean[] pasajYUs = {true, true};
        instance.iniciarSesion(cedula, pasajYUs);
    }
    
    @Test(expected = MiExcepcion.class)
    public void testIniciarSesion3() throws Exception {
        System.out.println("Prueba iniciarSesion3");
        String cedula = "waefeaw";
        boolean[] pasajYUs = {true, true};
        WorldTrip instance = WorldTrip.getInstance();
        instance.iniciarSesion(cedula, pasajYUs);
    }
    
    @Test(expected = MiExcepcion.class)
    public void testIniciarSesion4() throws Exception {
        WorldTrip instance = WorldTrip.getInstance();
        System.out.println("Prueba iniciarSesion4");
        String cedula = "11111111";
        boolean[] pasajYUs = {true, true};
        instance.iniciarSesion(cedula, pasajYUs);
    }
    
    @Test
    public void testAgregarServicioDeTransporte() {
        System.out.println("agregarServicioDeTransporte");
        WorldTrip instance = WorldTrip.getInstance();
        ServicioDeTransporte unServicioDeTransporte = new ServicioDeTransporte("ARD1234", "orig", "dest", "12:12", "10:10", "Lunes", 13224, new Transporte("train"), 42);
        
        int largo = instance.getListaServiciosDeTransporte().size();
        instance.agregarServicioDeTransporte(unServicioDeTransporte);
        int expected = instance.getListaServiciosDeTransporte().size();
        assertEquals(expected, largo + 1);
    }
    
    @Test
    public void testCrearCuenta() {
        System.out.println("Prueba crearCuenta");
        String cedula = "21345444";
        String nombre = "jorge";
        WorldTrip instance = WorldTrip.getInstance();
        int largo = instance.getListaPasajeros().size();
        instance.crearCuenta(cedula, nombre);
        int expected = instance.getListaPasajeros().size();
        assertEquals(expected, largo + 1);
    }
    
    @Test
    public void testCrearFuncionario() {
        System.out.println("crearFuncionario");
        String nombre = "nombreF";
        String cedula = "77777777";
        WorldTrip instance = WorldTrip.getInstance();
        int largo = instance.getListaFuncionarios().size();
        instance.crearFuncionario(nombre, cedula);
        int expected = instance.getListaFuncionarios().size();
        assertEquals(expected, largo + 1);
    }
    
    @Test
    public void testEsUsuarioFuncionario() {
        System.out.println("esUsuarioFuncionario");
        String cedula = "76576534";
        WorldTrip instance = WorldTrip.getInstance();
        Funcionario expResult = null;
        Funcionario result = instance.esUsuarioFuncionario(cedula);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testEsUsuarioFuncionario1() {
        WorldTrip instance = WorldTrip.getInstance();
        System.out.println("esUsuarioFuncionario");
        Funcionario f = new Funcionario("fun1", "12345678");
        instance.agregarFuncionario(f);
        String cedula = "12345678";
        assertEquals(f, instance.esUsuarioFuncionario(cedula));
    }
    
    @Test
    public void testEsUsuarioFuncionario2() {
        WorldTrip instance = WorldTrip.getInstance();
        System.out.println("esUsuarioFuncionario");
        Pasajero p = new Pasajero("pas1", "12355678", "");
        instance.agregarPasajero(p);
        String cedula = "12355678";
        Funcionario result = instance.esUsuarioFuncionario(cedula);
        assertEquals(null, result);
    }
    
    @Test
    public void testEsUsuarioFuncionario3() {
        WorldTrip instance = WorldTrip.getInstance();
        System.out.println("esUsuarioFuncionario");
        String cedula = "dawawfa";
        Funcionario result = instance.esUsuarioFuncionario(cedula);
        assertEquals(null, result);
    }
    
    @Test
    public void testEsUsuarioFuncionario4() {
        WorldTrip instance = WorldTrip.getInstance();
        System.out.println("esUsuarioFuncionario");
        String cedula = "123456789";
        Funcionario result = instance.esUsuarioFuncionario(cedula);
        assertEquals(null, result);
    }
    
    @Test
    public void testEsUsuarioPasajero() {
        System.out.println("esUsuarioPasajero");
        String cedula = "123456789";
        WorldTrip instance = WorldTrip.getInstance();
        Pasajero expResult = null;
        Pasajero result = instance.esUsuarioPasajero(cedula);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testEsUsuarioPasajero1() {
        System.out.println("esUsuarioPasajero");
        String cedula = "01010101";
        WorldTrip instance = WorldTrip.getInstance();
        Pasajero expResult = new Pasajero("pasaj1", cedula, "");
        instance.agregarPasajero(expResult);
        Pasajero result = instance.esUsuarioPasajero(cedula);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testEsUsuarioPasajero2() {
        System.out.println("esUsuarioPasajero");
        String cedula = "asfasda3";
        WorldTrip instance = WorldTrip.getInstance();
        Pasajero expResult = null;
        Pasajero result = instance.esUsuarioPasajero(cedula);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testBorrarPasajero() {
        System.out.println("borrarPasajero");
        Pasajero p = new Pasajero();
        WorldTrip instance = WorldTrip.getInstance();
        instance.agregarPasajero(p);
        int largo = instance.getListaPasajeros().size();
        instance.borrarPasajero(p);
        int expected = instance.getListaPasajeros().size();
        assertEquals(expected, largo - 1);
    }
    
    @Test
    public void testValidacionCedulaLogin() throws Exception {
        System.out.println("validacionCedulaLogin");
        WorldTrip instance = WorldTrip.getInstance();
        String cedula = "12345678";
        boolean expResult = true;
        boolean result = instance.validacionCedulaLogin(cedula);
        assertEquals(expResult, result);
    }
    
    @Test(expected = MiExcepcion.class)
    public void testValidacionCedulaLogin1() throws Exception {
        System.out.println("validacionCedulaLogin");
        WorldTrip instance = WorldTrip.getInstance();
        String cedula = "";
        instance.validacionCedulaLogin(cedula);
    }
    
    @Test(expected = MiExcepcion.class)
    public void testValidacionCedulaLogin2() throws Exception {
        System.out.println("validacionCedulaLogin");
        WorldTrip instance = WorldTrip.getInstance();
        String cedula = "aadsabda";
        instance.validacionCedulaLogin(cedula);
    }
    
    @Test(expected = MiExcepcion.class)
    public void testValidacionCedulaLogin3() throws Exception {
        System.out.println("validacionCedulaLogin");
        WorldTrip instance = WorldTrip.getInstance();
        String cedula = "123456789";
        instance.validacionCedulaLogin(cedula);
    }
    
    @Test
    public void testValidacionCedulaCreacion() throws Exception {
        System.out.println("validacionCedulaCreacion");
        String cedula = "66655544";
        WorldTrip instance = WorldTrip.getInstance();
        boolean expResult = true;
        boolean result = instance.validacionCedulaCreacion(cedula);
        assertEquals(expResult, result);
    }
    
    @Test(expected = MiExcepcion.class)
    public void testValidacionCedulaCreacion1() throws Exception {
        System.out.println("validacionCedulaCreacion");
        String cedula = "";
        WorldTrip instance = WorldTrip.getInstance();
        instance.validacionCedulaCreacion(cedula);
    }
    
    @Test
    public void testValidarHora() {
        System.out.println("validarHora");
        String unaHora = "";
        WorldTrip instance = WorldTrip.getInstance();
        boolean expResult = false;
        boolean result = instance.validarHora(unaHora);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testValidarHora1() {
        System.out.println("validarHora");
        String unaHora = "awdawf";
        WorldTrip instance = WorldTrip.getInstance();
        boolean expResult = false;
        boolean result = instance.validarHora(unaHora);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testValidarHora2() {
        System.out.println("validarHora");
        String unaHora = "12311";
        WorldTrip instance = WorldTrip.getInstance();
        boolean expResult = false;
        boolean result = instance.validarHora(unaHora);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testValidarHora3() {
        System.out.println("validarHora");
        String unaHora = "1:23";
        WorldTrip instance = WorldTrip.getInstance();
        boolean expResult = false;
        boolean result = instance.validarHora(unaHora);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testValidarHora4() {
        System.out.println("validarHora");
        String unaHora = "01:3";
        WorldTrip instance = WorldTrip.getInstance();
        boolean expResult = false;
        boolean result = instance.validarHora(unaHora);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testValidarHora5() {
        System.out.println("validarHora");
        String unaHora = "05/12";
        WorldTrip instance = WorldTrip.getInstance();
        boolean expResult = false;
        boolean result = instance.validarHora(unaHora);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testValidarHora6() {
        System.out.println("validarHora");
        String unaHora = "25:59";
        WorldTrip instance = WorldTrip.getInstance();
        boolean expResult = false;
        boolean result = instance.validarHora(unaHora);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testValidarHora7() {
        System.out.println("validarHora");
        String unaHora = "24:60";
        WorldTrip instance = WorldTrip.getInstance();
        boolean expResult = false;
        boolean result = instance.validarHora(unaHora);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testValidarHora8() {
        System.out.println("validarHora");
        String unaHora = "00:00";
        WorldTrip instance = WorldTrip.getInstance();
        boolean expResult = true;
        boolean result = instance.validarHora(unaHora);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testValidarHora9() {
        System.out.println("validarHora");
        String unaHora = "23:59";
        WorldTrip instance = WorldTrip.getInstance();
        boolean expResult = true;
        boolean result = instance.validarHora(unaHora);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testEsStringNumerico() {
        System.out.println("esStringNumerico");
        String s = "";
        WorldTrip instance = WorldTrip.getInstance();
        boolean expResult = false;
        boolean result = instance.esStringNumerico(s);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testEsStringNumerico1() {
        System.out.println("esStringNumerico");
        String s = "adsada123";
        WorldTrip instance = WorldTrip.getInstance();
        boolean expResult = false;
        boolean result = instance.esStringNumerico(s);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testEsStringNumerico2() {
        System.out.println("esStringNumerico");
        String s = "12 14";
        WorldTrip instance = WorldTrip.getInstance();
        boolean expResult = false;
        boolean result = instance.esStringNumerico(s);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testEsStringNumerico3() {
        System.out.println("esStringNumerico");
        String s = "14321";
        WorldTrip instance = WorldTrip.getInstance();
        boolean expResult = true;
        boolean result = instance.esStringNumerico(s);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testValidarId() {
        System.out.println("validarId");
        String unId = "";
        WorldTrip instance = WorldTrip.getInstance();
        boolean expResult = false;
        boolean result = instance.validarId(unId);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testValidarId1() {
        System.out.println("validarId");
        String unId = "124431";
        WorldTrip instance = WorldTrip.getInstance();
        boolean expResult = false;
        boolean result = instance.validarId(unId);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testValidarId2() {
        System.out.println("validarId");
        String unId = "adawaw";
        WorldTrip instance = WorldTrip.getInstance();
        boolean expResult = false;
        boolean result = instance.validarId(unId);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testValidarId3() {
        System.out.println("validarId");
        String unId = "adr4213";
        WorldTrip instance = WorldTrip.getInstance();
        boolean expResult = false;
        boolean result = instance.validarId(unId);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testValidarId4() {
        System.out.println("validarId");
        String unId = "ADr4213";
        WorldTrip instance = WorldTrip.getInstance();
        boolean expResult = false;
        boolean result = instance.validarId(unId);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testValidarId5() {
        System.out.println("validarId");
        String unId = "ZZZ413";
        WorldTrip instance = WorldTrip.getInstance();
        boolean expResult = false;
        boolean result = instance.validarId(unId);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testValidarId6() {
        System.out.println("validarId");
        String unId = "ABC4132";
        WorldTrip instance = WorldTrip.getInstance();
        boolean expResult = true;
        boolean result = instance.validarId(unId);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testIdUnico() {
        System.out.println("idUnico");
        String unId = "AZR2134";
        WorldTrip instance = WorldTrip.getInstance();
        boolean expResult = true;
        boolean result = instance.idUnico(unId);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testIdUnico1() {
        System.out.println("idUnico1");
        ServicioDeTransporte s = new ServicioDeTransporte("ABB1212", "orig", "dest", "12:12", "10:10", "Lunes", 13224, new Transporte("avion"), 42);
        String unId = "ABB1212";
        WorldTrip instance = WorldTrip.getInstance();
        instance.agregarServicioDeTransporte(s);
        boolean expResult = false;
        boolean result = instance.idUnico(unId);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testSetearDiasDisponibles() {
        System.out.println("setearDiasDisponibles");
        boolean l = false;
        boolean ma = false;
        boolean mi = false;
        boolean j = false;
        boolean v = false;
        boolean s = false;
        boolean d = false;
        WorldTrip instance = WorldTrip.getInstance();
        String[] expResult = {"", "", "", "", "", "", ""};
        String[] result = instance.setearDiasDisponibles(l, ma, mi, j, v, s, d);
        assertArrayEquals(expResult, result);
    }
    
    @Test
    public void testSetearDiasDisponibles1() {
        System.out.println("setearDiasDisponibles1");
        boolean l = false;
        boolean ma = true;
        boolean mi = false;
        boolean j = true;
        boolean v = false;
        boolean s = true;
        boolean d = false;
        WorldTrip instance = WorldTrip.getInstance();
        String[] expResult = {"", "Ma", "", "J", "", "S", ""};
        String[] result = instance.setearDiasDisponibles(l, ma, mi, j, v, s, d);
        assertArrayEquals(expResult, result);
    }
    
    @Test
    public void testSetearDiasDisponibles2() {
        System.out.println("setearDiasDisponibles2");
        boolean l = true;
        boolean ma = true;
        boolean mi = true;
        boolean j = true;
        boolean v = true;
        boolean s = true;
        boolean d = true;
        WorldTrip instance = WorldTrip.getInstance();
        String[] expResult = {"L", "Ma", "Mi", "J", "V", "S", "D"};
        String[] result = instance.setearDiasDisponibles(l, ma, mi, j, v, s, d);
        assertArrayEquals(expResult, result);
    }
    
    @Test
    public void testCalcularFrecuencia() {
        System.out.println("calcularFrecuencia");
        String[] diasD = {"L", "", "", "J", "", "S", "D"};
        WorldTrip instance = WorldTrip.getInstance();
        String expResult = "L,J,S,D";
        String result = instance.calcularFrecuencia(diasD);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testCalcularFrecuencia1() {
        System.out.println("calcularFrecuencia1");
        String[] diasD = {"L", "", "Mi", "", "V", "S", ""};
        WorldTrip instance = WorldTrip.getInstance();
        String expResult = "L,Mi,V,S";
        String result = instance.calcularFrecuencia(diasD);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testCalcularFrecuencia2() {
        System.out.println("calcularFrecuencia2");
        String[] diasD = {"L", "Ma", "Mi", "J", "V", "S", "D"};
        WorldTrip instance = WorldTrip.getInstance();
        String expResult = "Diario";
        String result = instance.calcularFrecuencia(diasD);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testEsDiario() {
        System.out.println("esDiario");
        String[] dias = {"L", "Ma", "Mi", "J", "V", "S", "D"};
        WorldTrip instance = WorldTrip.getInstance();
        boolean expResult = true;
        boolean result = instance.esDiario(dias);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testEsDiario1() {
        System.out.println("esDiario1");
        String[] dias = {"", "", "", "", "", "", ""};
        WorldTrip instance = WorldTrip.getInstance();
        boolean expResult = false;
        boolean result = instance.esDiario(dias);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testEsDiario2() {
        System.out.println("esDiario2");
        String[] dias = {"L", "", "", "", "", "", ""};
        WorldTrip instance = WorldTrip.getInstance();
        boolean expResult = false;
        boolean result = instance.esDiario(dias);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testEsDiario3() {
        System.out.println("esDiario3");
        String[] dias = {"", "", "", "", "", "", "D"};
        WorldTrip instance = WorldTrip.getInstance();
        boolean expResult = false;
        boolean result = instance.esDiario(dias);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testEsDiario4() {
        System.out.println("esDiario4");
        String[] dias = {"L", "", "", "J", "", "", "D"};
        WorldTrip instance = WorldTrip.getInstance();
        boolean expResult = false;
        boolean result = instance.esDiario(dias);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testEsDiario5() {
        System.out.println("esDiario5");
        String[] dias = {"", "Ma", "Mi", "", "V", "", "D"};
        WorldTrip instance = WorldTrip.getInstance();
        boolean expResult = false;
        boolean result = instance.esDiario(dias);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testEsDiario6() {
        System.out.println("esDiario5");
        String[] dias = {"", "Ma", "", "", "", "S", "D"};
        WorldTrip instance = WorldTrip.getInstance();
        boolean expResult = false;
        boolean result = instance.esDiario(dias);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testCrearServicioTransporte() {
        System.out.println("crearServicioTransporte");
        String unId = "";
        String unOrigen = "";
        String unDestino = "";
        String unaHora = "";
        String unaDuracion = "";
        String unaFrecuencia = "";
        int unPrecio = 0;
        Transporte unTransporte = null;
        int asientosDisponibles = 0;
        WorldTrip instance = WorldTrip.getInstance();
        instance.crearServicioTransporte(unId, unOrigen, unDestino, unaHora, unaDuracion, unaFrecuencia, unPrecio, unTransporte, asientosDisponibles);
    }
    
    @Test
    public void testCrearServicioTransporte2() {
        System.out.println("crearServicioTransporte");
        String unId = "ADR2134";
        String unOrigen = "Madrid";
        String unDestino = "Montevideo";
        String unaHora = "12:52";
        String unaDuracion = "01:43";
        String unaFrecuencia = "M,J,D";
        int unPrecio = 2143;
        Transporte unTransporte = new Transporte("Barco");
        int asientosDisponibles = 350;
        WorldTrip instance = WorldTrip.getInstance();
        instance.crearServicioTransporte(unId, unOrigen, unDestino, unaHora, unaDuracion, unaFrecuencia, unPrecio, unTransporte, asientosDisponibles);
    }
    
    @Test
    public void testModificarServicioDeTransporte() {
        System.out.println("modificarServicioDeTransporte1");
        ServicioDeTransporte unServicio = new ServicioDeTransporte("APP2534", "BsAs", "Berna", "20:42", "23:23", "Diario", 23142, new Transporte("Avion"), 190);
        String unaHoraSalida = "19:08";
        String unaHoraLlegada = "21:27";
        WorldTrip instance = WorldTrip.getInstance();
        instance.modificarServicioDeTransporte(unServicio, unaHoraSalida, unaHoraLlegada);
        assertEquals(unaHoraSalida, unServicio.getHoraSalida());
        assertEquals(unaHoraLlegada, unServicio.getHoraLlegada());       
    }
    
    @Test
    public void testSeleccionarServicios() {
        System.out.println("seleccionarServicios");
        String cOrigen = "Paraguay";
        String cDestino = "Hungria";
        WorldTrip instance = WorldTrip.getInstance();
        ArrayList<ServicioDeTransporte> expResult = new ArrayList<>();
        ServicioDeTransporte serv = new ServicioDeTransporte("ZLP2324", "Paraguay", "Hungria", "20:42", "23:23", "L,Ma,J", 17242, new Transporte("Avion"), 250);
        instance.agregarServicioDeTransporte(serv);
        expResult.add(serv);
        ArrayList<ServicioDeTransporte> result = instance.seleccionarServicios(cOrigen, cDestino);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testSeleccionarServicios1() {
        System.out.println("seleccionarServicios1");
        String cOrigen = "HongKong";
        String cDestino = "Lima";
        WorldTrip instance = WorldTrip.getInstance();
        ArrayList<ServicioDeTransporte> expResult = new ArrayList<>();
        ServicioDeTransporte serv = new ServicioDeTransporte("ZLP2324", "Paraguay", "Hungria", "20:42", "23:23", "L,Ma,J", 17242, new Transporte("Avion"), 250);
        instance.agregarServicioDeTransporte(serv);
        ArrayList<ServicioDeTransporte> result = instance.seleccionarServicios(cOrigen, cDestino);
        assertEquals(expResult, result);
    }
    
    public void testSeleccionarServicios2() {
        System.out.println("seleccionarServicios2");
        String cOrigen = "";
        String cDestino = "";
        WorldTrip instance = WorldTrip.getInstance();
        ArrayList<ServicioDeTransporte> expResult = new ArrayList<>();
        ArrayList<ServicioDeTransporte> result = instance.seleccionarServicios(cOrigen, cDestino);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testObtenerServiciosDeJList() {
        System.out.println("obtenerServiciosDeJList");
        JList j = new JList();
        WorldTrip instance = WorldTrip.getInstance();
        ArrayList<ServicioDeTransporte> expResult = new ArrayList<>();
        ArrayList<ServicioDeTransporte> result = instance.obtenerServiciosDeJList(j);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testObtenerServiciosDeJList1() {
        System.out.println("obtenerServiciosDeJList1");
        ArrayList<ServicioDeTransporte> expected = new ArrayList<>();
        ServicioDeTransporte serv = new ServicioDeTransporte("ZLP2324", "Paraguay", "Hungria", "20:42", "23:23", "L,Ma,J", 17242, new Transporte("Avion"), 250);
        expected.add(serv);
        JList j = new JList(expected.toArray());
        WorldTrip instance = WorldTrip.getInstance();
        ArrayList<ServicioDeTransporte> result = instance.obtenerServiciosDeJList(j);
        assertEquals(expected, result);
    }
    
    @Test
    public void testOrdenarPorX() {
        System.out.println("ordenarPorX");
        JList j = new JList();
        String sorting = "";
        WorldTrip instance = WorldTrip.getInstance();
        instance.ordenarPorX(j, sorting);
    }
    
    @Test
    public void testOrdenarPorX1() {
        System.out.println("ordenarPorX1");
        JList j = new JList();
        String sorting = "porTransporte";
        WorldTrip instance = WorldTrip.getInstance();
        instance.ordenarPorX(j, sorting);
    }

    @Test
    public void testSetearFrecuenciaEnBox() {
        System.out.println("setearFrecuenciaEnBox");
        JComboBox cBox = new JComboBox();
        String frec = "";
        WorldTrip instance = WorldTrip.getInstance();
        instance.setearFrecuenciaEnBox(cBox, frec);
    }

    @Test
    public void testSetearFrecuenciaEnBox1() {
        System.out.println("setearFrecuenciaEnBox1");
        JComboBox cBox = new JComboBox();
        String frec = "Diario";
        WorldTrip instance = WorldTrip.getInstance();
        instance.setearFrecuenciaEnBox(cBox, frec);
        assertEquals("Lunes", cBox.getItemAt(0));
        assertEquals("Martes", cBox.getItemAt(1));
        assertEquals("Miercoles", cBox.getItemAt(2));
        assertEquals("Jueves", cBox.getItemAt(3));
        assertEquals("Viernes", cBox.getItemAt(4));
        assertEquals("Sabado", cBox.getItemAt(5));
        assertEquals("Domingo", cBox.getItemAt(6));
    }
    @Test
    public void testSetearFrecuenciaEnBox2() {
        System.out.println("setearFrecuenciaEnBox2");
        JComboBox cBox = new JComboBox();
        String frec = "L,Mi,V";
        WorldTrip instance = WorldTrip.getInstance();
        instance.setearFrecuenciaEnBox(cBox, frec);
        assertEquals("Lunes", cBox.getItemAt(0));
        assertEquals("Miercoles", cBox.getItemAt(1));
        assertEquals("Viernes", cBox.getItemAt(2));
    }
     @Test
    public void testSetearFrecuenciaEnBox3() {
        System.out.println("setearFrecuenciaEnBox3");
        JComboBox cBox = new JComboBox();
        String frec = "Ma,V,D";
        WorldTrip instance = WorldTrip.getInstance();
        instance.setearFrecuenciaEnBox(cBox, frec);
        assertEquals("Martes", cBox.getItemAt(0));
        assertEquals("Viernes", cBox.getItemAt(1));
        assertEquals("Domingo", cBox.getItemAt(2));
    }
      @Test
    public void testSetearFrecuenciaEnBox4() {
        System.out.println("setearFrecuenciaEnBox3");
        JComboBox cBox = new JComboBox();
        String frec = "D";
        WorldTrip instance = WorldTrip.getInstance();
        instance.setearFrecuenciaEnBox(cBox, frec);
        assertEquals("Domingo", cBox.getItemAt(0));
    }

    @Test
    public void testValidarTarjeta() {
        System.out.println("validarTarjeta");
        String tarjeta = "";
        WorldTrip instance = WorldTrip.getInstance();
        boolean expResult = false;
        boolean result = instance.validarTarjeta(tarjeta);
        assertEquals(expResult, result);
    }
    @Test
    public void testValidarTarjeta1() {
        System.out.println("validarTarjeta1");
        String tarjeta = "adwamdawdma";
        WorldTrip instance = WorldTrip.getInstance();
        boolean expResult = false;
        boolean result = instance.validarTarjeta(tarjeta);
        assertEquals(expResult, result);
    }
    @Test
    public void testValidarTarjeta2() {
        System.out.println("validarTarjeta2");
        String tarjeta = "123456789123456";
        WorldTrip instance = WorldTrip.getInstance();
        boolean expResult = false;
        boolean result = instance.validarTarjeta(tarjeta);
        assertEquals(expResult, result);
    }
    @Test
    public void testValidarTarjeta3() {
        System.out.println("validarTarjeta3");
        String tarjeta = "1234 21412";
        WorldTrip instance = WorldTrip.getInstance();
        boolean expResult = false;
        boolean result = instance.validarTarjeta(tarjeta);
        assertEquals(expResult, result);
    }
    @Test
    public void testValidarTarjeta4() {
        System.out.println("validarTarjeta4");
        String tarjeta = "1$3456789123456A";
        WorldTrip instance = WorldTrip.getInstance();
        boolean expResult = false;
        boolean result = instance.validarTarjeta(tarjeta);
        assertEquals(expResult, result);
    }
    @Test
    public void testValidarTarjeta5() {
        System.out.println("validarTarjeta4");
        String tarjeta = "1234567891234567";
        WorldTrip instance = WorldTrip.getInstance();
        boolean expResult = true;
        boolean result = instance.validarTarjeta(tarjeta);
        assertEquals(expResult, result);
    }
    @Test
    public void testRegistrarCompraPasaje() {
        System.out.println("registrarCompraPasaje");
        ServicioDeTransporte serv = new ServicioDeTransporte("ZLP2324", "Paraguay", "Hungria", "20:42", "23:23", "L,Ma,J", 17242, new Transporte("Avion"), 250);
        Pasajero p = null;
        int cantPasajes = 0;
        String dia = "Lunes";
        String metodoPago = "Visa";
        WorldTrip instance = WorldTrip.getInstance();
        instance.registrarCompraPasaje(serv, p, cantPasajes, dia, metodoPago);
    }
    /* @Test
     public void testOrdenarPorX2() {
     System.out.println("ordenarPorX2");
     JList j = new JList();
     ServicioDeTransporte serv1 = new ServicioDeTransporte("ZLP2324", "Paraguay", "Hungria", "12:42", "11:35", "L,Ma,J", 2123, new Transporte("Barco"), 250);
     ServicioDeTransporte serv2 = new ServicioDeTransporte("ZLP2324", "Paraguay", "Hungria", "18:15", "08:23", "L,Ma,J", 242, new Transporte("Avion"), 250);
     ServicioDeTransporte serv3 = new ServicioDeTransporte("ZLP2324", "Paraguay", "Hungria", "01:00", "09:18", "L,Ma,J", 42211, new Transporte("Remisse"), 250);
     JList expected = new JList();
     ArrayList<ServicioDeTransporte> ordTrans = new ArrayList<>();
     ordTrans.add(serv2);
     ordTrans.add(serv1);
     ordTrans.add(serv3);
     expected.setListData(ordTrans.toArray());
     String sorting = "porTransporte";
     WorldTrip instance = WorldTrip.getInstance();
     instance.ordenarPorX(j, sorting);
     assertEquals(expected, j);
     }*/
    /*

     @Test
     public void testProcesamientoDeArchivos() {
     System.out.println("procesamientoDeArchivos");
     String pathArchivo = "";
     String nombreArchivo = "";
     int[] cantPyS = null;
     ArrayList<String> idsEmpr = null;
     WorldTrip instance = null;
     instance.procesamientoDeArchivos(pathArchivo, nombreArchivo, cantPyS, idsEmpr);
     // TODO review the generated test code and remove the default call to fail.
     fail("The test case is a prototype.");
     }

     @Test
     public void testCargarArchivo() {
     System.out.println("cargarArchivo");
     String[] arrAtributos = null;
     String nombreArchivo = "";
     int[] contPyS = null;
     ArrayList<String> idsE = null;
     WorldTrip instance = null;
     instance.cargarArchivo(arrAtributos, nombreArchivo, contPyS, idsE);
     // TODO review the generated test code and remove the default call to fail.
     fail("The test case is a prototype.");
     }

     @Test
     public void testMostrarEnJListHistorialP() {
     System.out.println("mostrarEnJListHistorialP");
     JList lista = null;
     Pasajero p = null;
     WorldTrip instance = null;
     instance.mostrarEnJListHistorialP(lista, p);
     // TODO review the generated test code and remove the default call to fail.
     fail("The test case is a prototype.");
     }

     @Test
     public void testObtenerHistorialesAJList() {
     System.out.println("obtenerHistorialesAJList");
     JList j = null;
     WorldTrip instance = null;
     instance.obtenerHistorialesAJList(j);
     // TODO review the generated test code and remove the default call to fail.
     fail("The test case is a prototype.");
     }

     @Test
     public void testObtenerPasajerosAJlist() {
     System.out.println("obtenerPasajerosAJlist");
     JList j = null;
     WorldTrip instance = null;
     instance.obtenerPasajerosAJlist(j);
     // TODO review the generated test code and remove the default call to fail.
     fail("The test case is a prototype.");
     }
     */
    
}
