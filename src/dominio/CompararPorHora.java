package dominio;

import java.io.Serializable;
import java.util.Comparator;

public class CompararPorHora implements Comparator<ServicioDeTransporte>, Serializable{
    private final boolean esHoraLlegada;
    private static final long serialVersionUID = 1L;
    
    public CompararPorHora(boolean esHoraLLegada){
        this.esHoraLlegada = esHoraLLegada;
    }
    @Override
    public int compare(ServicioDeTransporte o1, ServicioDeTransporte o2) {
        if(esHoraLlegada){
            return o2.getHoraLlegada().compareTo(o1.getHoraLlegada());
        }else{
            return o2.getHoraSalida().compareTo(o1.getHoraSalida());
        }
    }
}
