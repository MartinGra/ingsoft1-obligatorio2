package dominio;

import java.util.Objects;

public class Pasajero {
    private String nombre;
    private String cedula;
    private String tarjetaCredito;
    
    public Pasajero(){
        this.nombre = "";
        this.cedula = "";
        this.tarjetaCredito = "";
    }
    public Pasajero(String nom, String ced, String tarjC){
        this.nombre = nom;
        this.cedula = ced;
        this.tarjetaCredito = tarjC;
    }
  
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getTarjetaCredito() {
        return tarjetaCredito;
    }

    public void setTarjetaCredito(String tarjetaCredito) {
        this.tarjetaCredito = tarjetaCredito;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 83 * hash + Objects.hashCode(this.cedula);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Pasajero other = (Pasajero) obj;
        return Objects.equals(this.cedula, other.cedula);
    }

    @Override
    public String toString() {
        return  cedula + "|" + nombre;
    }
   
 
}
