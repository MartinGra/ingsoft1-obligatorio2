package dominio;

public class Viaje {
    private String origen;
    private String destino;
    private String horaSalida;
    private String horaLlegada;
    private String dia;
    private int precio;
    private Transporte transporte;
    private int cantidadPasajes;
    private String metodoPago;
    private Pasajero pasajero;
    
    public String getDestino() {
        return destino;
    } 

    public Viaje(String origen, String destino, String horaSalida, String horaLlegada, int precio, Transporte transporte, Pasajero pasajero,String dia,int cantidadPasajes, String metodoPago) {
        this.origen = origen;
        this.destino = destino;
        this.horaSalida = horaSalida;
        this.horaLlegada = horaLlegada;
        this.precio = precio;
        this.transporte = transporte;
        this.pasajero = pasajero;
        this.dia = dia;
        this.cantidadPasajes = cantidadPasajes;
        this.metodoPago = metodoPago;
    }

    public String getMetodoPago() {
        return metodoPago;
    }

    public void setMetodoPago(String metodoPago) {
        this.metodoPago = metodoPago;
    }

    public int getCantidadPasajes() {
        return cantidadPasajes;
    }

    public void setCantidadPasajes(int cantidadPasajes) {
        this.cantidadPasajes = cantidadPasajes;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public String getHoraSalida() {
        return horaSalida;
    }

    public String getDia() {
        return dia;
    }

    public void setDia(String dia) {
        this.dia = dia;
    }

    public void setHoraSalida(String horaSalida) {
        this.horaSalida = horaSalida;
    }

    public String getHoraLlegada() {
        return horaLlegada;
    }

    public void setHoraLlegada(String horaLlegada) {
        this.horaLlegada = horaLlegada;
    }

    public String getOrigen() {
        return origen;
    }
    public void setOrigen(String origen){
        this.origen = origen;
    }
    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    public Transporte getTransporte() {
        return transporte;
    }

    public void setTransporte(Transporte transporte) {
        this.transporte = transporte;
    }
    

    public Pasajero getPasajero() {
        return pasajero;
    }

    public void setPasajero(Pasajero pasajero) {
        this.pasajero = pasajero;
    }

    @Override
    public String toString() {
        return pasajero.getCedula()+"|"+ origen + "|" + destino +"|"+ horaSalida+"|" + horaLlegada +"|" +dia +"|" +precio+"|"+transporte.getTipo()+"|" +cantidadPasajes;
    }
    
    
}
